2.0: First edition

2.1: Added "Multithreading" chapter and corrections.

2.2:

- Lots of small errata corrected.
- Rephrased the "Parentheses and brackets" section of the Toolchain chapter, following reader feedback.
- Added @Parcelize support and reorganized the Kotlin introduction section.
- Added discussion about companion objects in Kotlin.
- Added warning about array and dictionary initialization in Kotlin vs. Swift.
- Added a section about Kotlin coroutines in the Multithreading chapter.
- Added a section about operator overloading and infix methods.
- Indicated how to use event handlers directly in layout files.
- Added a small example showing how to use HTML in TextView instances.
- Renamed the NDK chapter as "Cross Platform" and added a chapter about embedding JavaScript with Rhino.
- Added formulae for the RSA algorithm.
- Added fast scrolling to the "UI/Fragments" code example.
- Simplified the "Room" section of the "Storage" chapter using stuff explained in the "Multithreading" chapter.
- Reduced size of images and of resulting book files.
- Removed EPUB warnings.
- Code highlighting in code snippets looks more like the default code highlighting of Android Studio.
- Nicer code highlighting for ebook readers.

