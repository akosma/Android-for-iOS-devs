[[chapter_ui]]
= User Interface

Android uses the same basic input interface as iOS; a touchscreen. Through this interface, users are able to manipulate and interact with widgets such as buttons, scrollbars, panes and menus, with a sense of physicality very much like the one offered by UIKit and its related frameworks.

In this chapter we are going to learn how to build and organize user interfaces in our applications, concentrating our attention in the major building blocks of Android apps: Activities, Intents and Fragments.

== TL;DR

For those of you in a hurry, the table below summarizes the most important pieces of information in this chapter.

[[table_ui]]
.User Interface in Android
[format="csv", options="header"]
|===
include::{datadir}/UI/tldr.csv[]
|===

== UI Design Guidelines

https://material.google.com/[((Material Design))] is the current visual language that Google has created to unify the visuals and interactions throughout their complete suite of products, on the web, on the desktop and of course on mobile devices.

This book is definitely not a book about visual design (and if you can tell through my UML diagrams, I can say that design in general is not one of my strenghts!) but it is important to understand the underlying principlies behind Material Design.

Google created Material Design with the https://material.google.com/#introduction-principles[following principles] in mind:

[quote]
.Material is the metaphor
____
A material metaphor is the unifying theory of a rationalized space and a system of motion.
____

[quote]
.Bold, graphic, intentional
____
The foundational elements of print-based design – typography, grids, space, scale, color, and use of imagery – guide visual treatments.
____

[quote]
.Motion provides meaning
____
Motion respects and reinforces the user as the prime mover.
____

Of course, this should come as no surprise to any seasoned iOS developer; Apple itself is well known for having created visual guidelines for their own operating systems (starting with https://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/index.html[macOS] and following with https://developer.apple.com/ios/human-interface-guidelines/overview/design-principles/[iOS], https://developer.apple.com/watchos/human-interface-guidelines/[watchOS] and https://developer.apple.com/tvos/human-interface-guidelines/overview/[tvOS]) for a long time.

These guidelines, as the name suggest, provide designers and developers with a common language, enabling teams to discuss and elaborate visual architectures for their applications.

I strongly suggest the reader of these lines to spend some time browsing the https://material.google.com/[Google Material Design website] in order to understand the paradigms and the ideas behind the different visual elements that make up an Android application.

== Android Support Library

(((Hardware fragmentation, Support Library)))Historically, the characteristic of Android that frightens most iOS developers is the sheer diversity of devices and versions of Android available in the wild. The technical press usually refers to this issue as the "fragmentation" of the Android world… but as with many things in the press these days, it is safe to say that these claims (and the fears generated as a consequence) are overrated.

Early in the development of Android, Google realized that application developers should be able to support lots of different devices, with different screen sizes (such as tablets and smartphones of all sizes) and resolutions. This situation led to two very important additions to the Android toolkit http://android-developers.blogspot.ch/2011/03/fragments-for-all.html[back in 2011]:

1. Fragments.
2. The Support Library.

The ((Support Library)), initially known as the Android Compatibility Package, allows applications running in older versions of Android to enjoy the UI paradigms and the features brought to the system in new versions of Android.

This library is available to Android developers through the Android SDK, and is distributed to users through the Play Store; this means that Android devices that include the standard Google Play Store will always have the Support Library installed, and this enables all applications to run seamlessly in all devices, starting in Android 2.3 (API level 9) and higher.

Throughout this book, we are going to use this library extensively. The package is named `android.support` and all of our applications will inherit from `android.support.v7.app.AppCompatActivity` instead of the standard `Activity` class provided by Android.

== Activities

By far, the most important building block of Android applications are *activities.* They fulfill a similar role to that of `UIViewController` instances in iOS, that is, to *manage the display of a screenful of data at any single time.*

Android Studio allows developers to build applications around the ((Activity)) paradigm, creating and removing activities for each of the specific tasks that are required by the application. Activities are a powerful architectural mechanism for organizing and encapsulating your code.

IMPORTANT: Activities must be self-contained and have as few dependencies from other activities as possible. This will allow you to reuse them in the same or in other applications easily.

=== The Activity Stack

In every Android application, there is a ((default activity)) that is launched and displayed by default when the application starts. This is similar to the "entry point" defined in iOS Storyboards. The same way you can move the arrow that represents the entry point of the Main storyboard in an iOS application, you can specify the default activity for any Android application as a simple entry in the AndroidManifest.xml file:

[[android_manifest]]
.AndroidManifest.xml
[source,xml,indent=0]
----
include::{codedir}/UI/Basic/app/src/main/AndroidManifest.xml[]
----

However, Android being different from iOS, it also means that the way activities are connected to each other is also different. In Android, all activities are included by default in a default ((navigation stack)) (((Activity, navigation stack))), and any activity can be launched independently of the other activities bundled in an application.

For iOS developers, it is better to think that the operating system holds a system-wide ((`UINavigationController`)) instance that spans accross all applications; every time the user launches an application, the activity is "pushed" to this global navigation stack. When the user presses the btn:[Back] button (which is usually available in most Android devices by default as a hardware button) the current activity is "popped" from the stack, and the device returns to the previous state of operation, whichever that is.

This means that if your application launches an activity from the calendar application, and this one in turn launches another activity, say for example a contact from the address book, then when the user presses the back button twice, the current activity will be again your own activity, the one that started this chain of operations.

=== A Basic Application

In the source code bundled with this book, please open the Android Studio project located in the `UI/Basic` folder. This is a very simple Android application with a single activity, that shows how to use an activity as a simple controller for the UI of your application.

To recreate the application by yourself, follow these steps:

. Create a new Android Studio application. Select the default options, including the "Empty Activity" template.
. When Android Studio is ready, you open the `activity_main.xml` layout file and delete the label that appears on top of the screen.
. Using your mouse, drag three components to the UI, in the following order: a plain text EditText, a Button, and a TextView widget. You should see them in the palette at the left side of the editor panel, a familiar sight for those used to Interface Builder.
. Select the `EditText` component in the designer, and on the right side of the editor you should see a properties panel. Change the ID to `nameEditText`, change the "hint" property to `Enter your name and touch "Greet"` and remove the value of the "text" property.
. Select the `Button` component in the designer, and on the properties panel change the properties as follows: set the ID to `greetButton`, set the text to `Greet`, and set the `onClick` event to `button_onClick`.
. Select the `TextView` component in the designer, and set the properties as follows: ID to `greetingTextView` and remove any value in the "text" property.

TIP: As you can see, the Android UI designer properties panel works in a very similar way as that of Interface Builder; it organizes the properties following the inheritance chain; the `EditText` properties appearing on top of those defined in the `TextView` class.

[start=7]
. Launch your application on the emulator or, better yet, connect your device to your Mac and use it to launch the application. You should see the widgets displayed according to the layout created in the designer, but for the moment you can not do anything on the user interface. Clicking the btn:[Greet] button yields nothing. It is time to enter some code.
. Double click on the `MainActivity.kt` file in the project browser on the left of the Android Studio window (if you have not done that already.) We are going to add a bit of code to that file, until it looks like the source code below.

[[src-basic-mainactivity]]
.MainActivity.kt
[source,kotlin,indent=0]
----
include::{codedir}/UI/Basic/app/src/main/java/training/akosma/basic/MainActivity.kt[tags=intro;oncreate;listener;last]
----

<1> Just like `UIViewController` instances, Activities have a well-defined lifecycle (((Activity, lifecycle))). They have several methods that are called at specific moments in their lifetime. This method, `onCreate()` is roughly equivalent to `viewDidLoad` in iOS, as it is also called when the instance is created in memory.

<2> In addition to using the XML layout to assign event handlers, you can also add them using simple Kotlin lambdas assigned via the `setOnClickListener` method. Moreover, the `kotlin-android-extensions` plugin allows to create automatically the connections between variables in the code and widgets in the layout.

<3> You can easily display HTML code directly in your `TextView` instances. Use the `android.text.Html.fromHtml()` method and assign the result (a `Spanned` value) to the `text` property.

WARNING: The `R` class is autogenerated, and can sometimes get out of sync with the resources of the project. If that happens, a quick solution is to use the menu:Build[Clean Project] menu item in Android Studio.

=== Activity States

(((Activity, lifecycle)))As you have just seen, Activities have a lifecycle very similar to that of the `UIViewController` class. The following diagram shows in detail the various states and methods called in each state change, as the Activity instance is created, modified and disposed.

[[diagram-activity-states]]
.Activity State Diagram
[plantuml,diagram-activity-states,png]
....
title Activity State Diagram

state Normal {
    [*] --> None
    None --> Stopped : onCreate()
    Stopped --> Paused : onStart()
    Paused --> Running : onResume()
    Running --> Paused : onPause()
    Paused --> Stopped : onStop()
    Stopped --> None : onDestroy()
    None -up-> [*]
}

Normal -right-> Saved : onSaveInstanceState()
....

=== Inspecting the UI

When inspecting the user interface using the Hierarchy Viewer bundled in the Android Device Monitor, the screen shown in the image below appears.

[[img-basic-hierarchy]]
.Inspecting the user interface of the Basic application
image::UI/Hierarchy_viewer.png[Inspecting the user interface of the Basic application]

=== Android Widgets

In the Basic application built above, we have used several different widgets for the user interface.

WARNING: Sometimes Android UI widget classes have strange sounding names for those of us coming from the iOS world. The naming conventions are different, and as such it is important to learn them.

In the "Basic" application above we have used the `TextView`, `EditText` and `Button` classes, which are all related through inheritance. (((View class inheritance)))

[[diagram-edittext-classes]]
.EditText Hierarchy Diagram
[plantuml,diagram-edittext-classes,png]
....
title EditText Hierarchy Diagram

class View
class TextView
class EditText
class Button

View <|-- TextView
TextView <|-- EditText
TextView <|-- Button
....

The `TextView` class is a subclass of `android.view.View`, the base class of all visible things in Android. Just like in iOS, a View is a representation of a rectangle on the screen, including everything that is drawn inside.

IMPORTANT: Instances of the `android.view.View` class, unlike `UIView` instances, cannot have children widgets; this is only possible at the level of the `android.view.ViewGroup` class, itself a subclass of `android.view.View`. Remember this when you will want to create your own complex view systems.

Many of the most important subclasses of `android.view.View` are shown below.

[[diagram-view-hierarchy]]
.View Hierarchy Diagram
[plantuml,diagram-view-classes,png]
....
title View Hierarchy Diagram

class View
class TextView
class TabItem
class ProgressBar
class ImageView
class ViewGroup
class LinearLayout
class FrameLayout
class RadioGroup

View <|-- TextView
View <|-- TabItem
View <|-- ProgressBar
View <|-- ImageView
View <|-- ViewGroup
ViewGroup <|-- LinearLayout
ViewGroup <|-- FrameLayout
LinearLayout <|-- RadioGroup
FrameLayout <|-- DatePicker
....

Finally, when a developer wants to set or get the text of a `TextView`, it turns out that the setter and getter do not take `String` instances, but rather reference the `CharSequence` interface. In the case of the `EditText`, the `getText()` method returns an object implementing the `Editable` interface.

TIP: As a general design decision, using interfaces in your method signatures it is always a good idea. It makes your APIs more flexible and extensible.

[[diagram-string-classes]]
.String Hierarchy Diagram
[plantuml,diagram-string-classes,png]
....
title String Hierarchy Diagram

class Editable << (I,LightGray) >>
class CharSequence << (I,LightGray) >>
class String
class Spanned

CharSequence <|-- Editable
CharSequence <|-- String
CharSequence <|-- Spanned
....

=== Optimizing the Project with Lint

(((Android Studio, lint)))Let us enhance now this "Basic" application a little bit. First of all, we are going to select the menu:Analyze[Inspect Code…] menu item. The dialog shown in the screenshot below will appear.

[[img-lint-dialog]]
.Android Studio Code Inspection Dialog
image::UI/Lint_dialog.png[Android Studio Code Inspection Dialog]

Android Lint is a tool roughly equivalent to the http://clang-analyzer.llvm.org:[Clang Static Analyzer], available in Xcode in the menu:Product[Analyze…] menu item. We will run it with the default options, and among the many improvements, there are a few "low hanging fruits" which we can fix right now.

The output of the tool appears at the bottom of the screen.

[[img-lint-results]]
.Android Studio Lint Results
image::UI/Lint_results.png[Android Studio Lint Results]

The "Inspection" pane at the bottom of the Android Studio window contains the list of problems found in the project, and when selecting any of these items, an explanation is shown on the left side.

=== String Files

Now let us fix some of these problems.

The first one is actually highlighted in the previous image, and has to do with the fact that we have hardcoded strings in the layout file when we created our "Basic" application. This is hardly a good idea; first of all, we might want to make our application available to users in other languages in the future, and this is where the `strings.xml` file comes in handy.

The ((`strings.xml`)) resource file is more or less equivalent to the ((`Localizable.strings`)) file used in Cocoa to provide international versions of all the strings included in the application. There is, however, a nice difference; the strings referenced in `strings.xml` are immediately parsed by Android Studio, and they are available through the autogenerated `R` class.

To solve the problem in the Lint Inspection pane, select it with your mouse and hit the kbd:[{commandkey} + ↓] keyboard shortcut. This will open the file where the problem resides and will scroll automatically to the required line of code. Replace the value `Enter your name and touch "Greet"` with the text `@string/edit_hint`. At first Android Studio will complain that the key is non existent, but we are going to fix that immediately.

Open now the `strings.xml` file; for that, let us use another handy keyboard shortcut: kbd:[Shift + {commandkey} + O]. This opens a "Quick Open" dialog that allows you to open any file on the project. Add the required key on the file, and then do the same with the text on the btn:[Greet] button.

Your strings file should now look like shown below.

[[src-strings-xml]]
.A simple strings.xml file
[source,xml,indent=0]
----
include::{codedir}/UI/Basic/app/src/main/res/values/strings.xml[]
----

One of the nice things of concentrating strings inside of the resources file is that Android Studio proposes to "autocomplete" most string placeholders with values taken out of it. This simplifies the handling of these values greatly, and makes it easy to translate all the strings in a project in one operation, usually before publication.

(((Android Studio, string file editor)))One final note about string files: to avoid (mis)handling the XML code in the file, Android Studio provides a nice interface that can be used to edit strings instead. This can be accessed by clicking on the "Open editor" link that appears on the top right of the `strings.xml` file tab in Android Studio.

[[img-strings-editor]]
.Android Studio Translation Editor
image::UI/Strings_form.png[Android Studio Translation Editor]

[[section-orientation-changes]]
=== Handling Orientation Changes

Whether you are using now a physical device or an emulator, try now the following: first, enter your name in the `EditText` field, hit the btn:[Greet] button and then change the orientation of the device.

See what happened? The value in the `TextView` suddenly disappeared. This is something that can be very puzzling for iOS developers. In iOS, the state of a `UIViewController` is kept between orientation changes, and the controller just receives a few callbacks to be notified of the change.

(((orientation change))) In the case of Android, the behavior is radically different; when the user rotates the device, *the activity is destroyed and disposed, and a new one is created and displayed.*

The only possible solution for an Android developer, given these constraints, is to save the current state of the application, and to reload it accordingly if required. Let us add some code to solve this problem.

[[src-basic-bundle]]
.MainActivity keeping state
[source,kotlin,indent=0]
----
include::{codedir}/UI/Basic/app/src/main/java/training/akosma/basic/MainActivity.kt[tags=key;check;save]
----

<1> This is the key used to store the current value of the `TextView` instance when the activity is about to be destroyed.

<2> When Android creates the activity, it verifies whether there was already a stored value; if this is the case, then use it to reset the UI to the previous state.

<3> This method is called right before the Activity is destroyed; we use the `KEY` string to store the current value of the `TextView` widget before being called upon oblivion.

== Intents

The basic communication mechanism between activities is the ((`android.content.Intent`)) class. Whenever an activity wants to start another activity, or to communicate with another process in the device, it will always use an instance of the ((`Intent`)) class.

This architecture has no equal in the world of iOS applications, where the communication between view controllers is usually strongly coupled; this has the advantage of a simpler programming model, relatively easier to understand for newcomers, but it also leads to tangled architectures, where it is almost impossible to reuse controllers in different contexts.

Thanks to intents, Android activities can truly be independent from each other at every time, which helps in the creation of decoupled architectures, with high degrees of testability and reuse.

[TIP]
.Follow along
====
The code of this section is located in the `UI/Age` folder.
====

There are three use cases for intents in Android:

- ((*Explicit intents*)) specify the class of the activity to launch, and are commonly used inside of a single application to navigate from screen to screen.
- ((*Implicit intents*)) are used to open system-wide services, such as asking the built-in browser to open a web page or to search for a contact in the contacts database.
- *Return values* from an activity to the "previous" one in the activity stack are also `Intent` instances, holding on to the data that must be passed back.

Let us learn now how to use `Intent` instances to open other activities.

=== Implicit Intents

((Implicit intents)) are the simplest. Just specify the action you would like to launch (in this case, `android.content.Intent.ACTION_VIEW`) and the parameter (in this case, a URL.) This implicit intent has the net result of opening the default web browser in the device.

[[src-intent-implicit]]
.Using implicit intents
[source,kotlin,indent=0]
----
include::{codedir}/UI/Age/app/src/main/java/training/akosma/age/MainActivity.kt[tag=implicit]
----

The `startActivity()` method of the `Activity` class takes an `Intent` instance as a parameter, and asks the operating system to do the rest.

=== Explicit Intents

(((Explicit intents)))The simplest use case consists in navigating from one activity to another, that is, pushing a new activity on the device navigation stack.

[[src-intent-explicit]]
.Using explicit intents
[source,kotlin,indent=0]
----
include::{codedir}/UI/Age/app/src/main/java/training/akosma/age/MainActivity.kt[tag=explicit]
----

In this case we use the `startActivityForResult()` method of the `Activity` class, because we are expecting the `AgeActivity` class to return a simple value.

=== Returning Values

The `AgeActivity` class in our example contains a `SeekBar` that the user can slide from left to right to choose a suitable age. When the user presses the "Back" button (or, alternatively hits the "Finish" button) the current activity is popped off the current stack and the `setResult()` method is called. This method takes an `Intent` as a parameter, one that contains the data to be passed to the previous activity.

[[src-intent-return]]
.Returning data with an Intent
[source,kotlin,indent=0]
----
include::{codedir}/UI/Age/app/src/main/java/training/akosma/age/AgeActivity.kt[tag=returning]
----

When an activity calls `setResult()` the one that has requested it through the `startActivityForResult()` will be notified of this, and the `onActivityResult()` callback will be activated.

[[src-intent-read-return]]
.Reading a result value
[source,kotlin,indent=0]
----
include::{codedir}/UI/Age/app/src/main/java/training/akosma/age/MainActivity.kt[tag=reading]
----

Thanks to this simple architecture, data can flow from one activity to another freely and simply. The structure of the data is, of course, part of an implicit contract that should be documented and specified – and tested, if at all possible.

Another example of requesting data from the operating system is shown below, where we ask the user to select a contact from its device and we display the name on our application.

[[src-intent-request-contact]]
.Requesting and showing a contact
[source,kotlin,indent=0]
----
include::{codedir}/UI/Age/app/src/main/java/training/akosma/age/AgeActivity.kt[tag=contact]
----

[source,kotlin,indent=0]
----
include::{codedir}/UI/Age/app/src/main/java/training/akosma/age/AgeActivity.kt[tag=result]
----

[IMPORTANT]
.Data Providers
====
The contacts example we saw in the previous section is just one example among a large collection of generic data providers. We are going to learn more about Android data providers in chapter "Storage".
====

== Fragments

Remember iOS "Universal Applications"? These are, according to Apple, iOS applications that can run both in the "form factor" of the iPhone or on that of the iPad. They are usually quite easy to create on iOS; just create different storyboards for each device, wire the scenes in your storyboards accordingly, and iOS takes care of the rest. If the application is running on an iPhone, then the iPhone storyboard will be displayed; if it is on an iPad, then the iPad storyboard will be taken into account.

Android works in pretty much the same way; the difference is that Android applications can have different layout files for different screen sizes, and even for different resolutions, orientations, and many other factors!

However, no matter how many layout files you have in your projects, Activities still take all the available screen space, every single time. Android does not allow many activities to share the current screen. Hence, a different solution was required. And this solution is called *fragments*.

((Fragments)) are one of the most fundamental visual building blocks in Android. They allow developers to create flexible user interfaces that work differently in different devices, yet they are distributed as the same application in the Play Store.

Fragments are, in a sense, like small `UIViewController` instances that are children of a bigger, "container" `UIViewController`. You can compose complex user interfaces in iOS by nesting controllers inside of other controllers. Each contains its own view logic, and talks to other controllers using well-defined interfaces (like notifications, delegate protocols or other mechanisms.)

In Android, Fragments always exist inside an Activity. Activities can have one or many fragments, each containing its own view logic.

[TIP]
.Follow along
====
The code of this section is located in the `UI/Fragments` folder.
====

We are going to create now a small application that displays the same data in different ways depending on whether it is running on a tablet or on a smartphone; we are actually going to create something similar to a `UISplitViewController`!

The first step consists in creating the individual fragments for our application. We need a fragment that displays a list of items, and another fragment that displays just one of the items; the classical "master & detail" user interface paradigm.

We are also going to need two activities; one is the `MainActivity`, the root activity of the application, whether it is running on a smartphone or a tablet. If the application is running on a tablet, it will display both fragments at the same time. If it is running on a smartphone, that means that the "detail" fragment will not be visible, and then the `DetailActivity`, itself containing the `DetailFragment` will be called using an intent.

These are the layouts we need for both activities; as you can see, there are two layouts for the MainActivity, which has two different "look & feels":

[[src-fragments-main-smartphone]]
.MainActivity layout file for smartphones
[source,xml,indent=0]
----
include::{codedir}/UI/Fragments/app/src/main/res/layout/activity_main.xml[]
----

[[src-fragments-main-tablet]]
.MainActivity layout file for tablets
[source,xml,indent=0]
----
include::{codedir}/UI/Fragments/app/src/main/res/layout-large/activity_main.xml[]
----

The `DetailActivity` class only needs one layout:

[[src-fragments-detail]]
.DetailActivity layout file
[source,xml,indent=0]
----
include::{codedir}/UI/Fragments/app/src/main/res/layout/activity_detail.xml[]
----

In terms of code, the most complex class in the project is the `ListFragment` class, which actually uses a ((`android.support.v7.widget.RecyclerView`)) instance to display a list of strings. The ((`RecyclerView`)) class is the closest thing in Android to an iOS `UICollectionView` or `UITableView`, and we are going to learn more about it in the next few chapters. For the moment you only need to know that the `Adapter` class inside of it is used as a data source, just as you would do it in iOS.

The `ListFragment` class also defines a "callback protocol" so that users of that fragment are notified of events on the `RecyclerView`. The root activity of the project is responsible for the coordination of the work between the fragments, and we can see that at work in the code below.

[[src-fragments-mainactivity-coordination]]
.The MainActivity class coordinating the fragments
[source,kotlin,indent=0]
----
include::{codedir}/UI/Fragments/app/src/main/java/training/akosma/fragments/MainActivity.kt[tag=coordination]
----

As you can see, here we check for the existence of the detail fragment in the layout of the activity; if we are running the app in a smartphone, then the layout will not include that fragment, and the check will yield a falsy value; hence, we just create an `Intent` and ask the operating system for a new activity, in order to display the value that the user selected on the list.

On the other hand, if the app is running on a tablet, then the layout file that has been loaded by the operating system already includes that fragment, and thus the only thing we need to do is to simply update its value.

=== Coordination via LocalBroadcastManager

In the previous example we used a direct communication pattern between the list fragment and its host activity. You can use the `android.support.v4.content.LocalBroadcastManager` (((LocalBroadcastManager))) class for a more disconnected system of interaction, one that is suspiciously similar to the one provided in Cocoa by the ((`NSNotificationCenter`)) and `NSNotification` classes.

Local broadcast are available through the support library, and they guarantee privacy since no app can accept local broadcasts from any other application, and no other application can listen to the local broadcast of other apps.

[TIP]
.Follow along
====
The code of this section is located in the `UI/Notifications` folder.
====

The first thing we need to do is to make the `MainActivity` class, the one that coordinates the communication between fragments, to register itself as a listener of a local broadcast.

Create an `android.content.BroadcastReceiver` object and override its `onReceive()` method, as shown in the following listing.

[[src-notifications-mainactivity-receiver]]
.A local broadcast receiver
[source,kotlin,indent=0]
----
include::{codedir}/UI/Notifications/app/src/main/java/training/akosma/notifications/MainActivity.kt[tag=receiver]
----

Then register this receiver object in `onCreate()`.

[[src-notifications-mainactivity-register]]
.Registering a local broadcast receiver
[source,kotlin,indent=0]
----
include::{codedir}/UI/Notifications/app/src/main/java/training/akosma/notifications/MainActivity.kt[tag=register]
----

Needless to say, you should deregister it on `onDestroy()`.

[[src-notifications-mainactivity-deregister]]
.Deregistering a local broadcast receiver
[source,kotlin,indent=0]
----
include::{codedir}/UI/Notifications/app/src/main/java/training/akosma/notifications/MainActivity.kt[tag=deregister]
----

Finally, let us modify the `ListFragment` class so that local broadcasts are sent every time that the user taps on an item.

[[src-notifications-listfragment-notify]]
.Sending a local broadcast
[source,kotlin,indent=0]
----
include::{codedir}/UI/Notifications/app/src/main/java/training/akosma/notifications/ListFragment.kt[tag=notify]
----

== Layouts

Android is an incredibly popular operating system, running in billions of devices all over the planet. These devices can have dramatically different sizes, ranging from small smartphones to large tablets.

When designing Android applications, it is important to never hardcode the positions of the widgets on the screen, but rather to place them in relative positions, and then to test the application in as many devices as possible.

Android applications use XML files to define the user interface shown in activities and fragments. These XML files use different "layouts" to place the widgets in similar ways across all devices. There are several kinds of layouts available, and this chapter will describe the most common ones.

[[img-layouts-available]]
.Layouts available in Android Studio
image::UI/layouts_all.png[Layouts available in Android Studio]

[NOTE]
.Follow along
====
The code of this section is located in the `UI/Layouts` folder.
====

=== What is a Layout?

Android ((layouts)) are views which have the characteristic of being able to have children. By default, Android views cannot have children, and the first view that presents this characteristic in the class hierarchy is the `ViewGroup` class. All layouts inherit from the `ViewGroup` class, as shown in the diagram below. Also, some common widgets, such as the `RadioGroup` and the `DatePicker` class inherit from layouts, helping their complex structures.

[[diagram-layout-hierarchy]]
.Layout Hierarchy Diagram
[plantuml,diagram-layout-classes,png]
....
title Layout Hierarchy Diagram

class View
class ViewGroup
class LinearLayout
class FrameLayout
class RadioGroup

View <|-- ViewGroup
ViewGroup <|-- LinearLayout
ViewGroup <|-- FrameLayout
ViewGroup <|-- GridLayout
ViewGroup <|-- RelativeLayout
ViewGroup <|-- ConstraintLayout
LinearLayout <|-- RadioGroup
FrameLayout <|-- DatePicker
....

Since layouts are `ViewGroup` subclasses, they can contain other layouts as well. This means that complex user interfaces can be built by composition, using different layouts where they make sense, one inside the other.

=== Constraint Layout

By default, new Android application projects will use the ((Constraint Layout)) for their user interface. This is the latest and the most powerful layout system available in Android, and it is very similar to Auto Layout in iOS.

[NOTE]
.Cassowary Algorithm
====
Both iOS' Auto Layout and Android Constraint Layout derive from the same mathematical foundation, the https://dl.acm.org/citation.cfm?id=504705[Cassowary linear arithmetic constraint solving algorithm].
====

The idea behind the Constraint Layout is to describe the positions of the widgets on the screen using equations that are solved at runtime; all of the widgets are positioned in such a way that these equations are solved and correct. For example, we might want a text label with a title to be centered on the screen, but always placed 42 points below the title bar; then we would want a button to have a flexible width but a fixed height, and to be placed always 30 points below the previous text label; and so on. Android will solve this equations at runtime for you.

To make your life easier, Android provides a visual layout editor.

[[img-layouts-constraint]]
.Constraint Layout Editor
image::UI/layouts_constraint.png[Constraint Layout Editor]

In the XML file you can see the results of your manipulation.

.Constraint Layout XML
[source,xml,indent=0]
----
include::{codedir}/UI/Layouts/app/src/main/res/layout/activity_main.xml[tag=layout]
----

=== Linear Layout

The ((Linear Layout)) is the simplest of all layouts available. It just places the widgets on the screen in the order in which they are defined in the XML file, either vertically or horizontally.

.Linear Layout XML
[source,xml,indent=0]
----
include::{codedir}/UI/Layouts/app/src/main/res/layout/activity_linear.xml[tag=layout]
----

[[img-layouts-linear]]
.Linear Layout Editor
image::UI/layouts_linear.png[Linear Layout Editor]

=== Frame Layout

The ((Frame Layout)) blocks a part of the screen so that only one component can be drawn. The idea of the Frame Layout is to position a single child element, although it is technically possible to use the `android:layout_gravity` parameter of child views to place an item on a corner, at the center or at the middle of the layout. The example below shows how to do this.

.Frame Layout XML
[source,xml,indent=0]
----
include::{codedir}/UI/Layouts/app/src/main/res/layout/activity_frame.xml[tag=layout]
----

[[img-layouts-frame]]
.Frame Layout Editor
image::UI/layouts_frame.png[Frame Layout Editor]

=== Grid Layout

As the name explains, the ((Grid Layout)) is used to place child views in a grid structure. This can be useful in many situations, like for example the buttons of a calculator; the example below shows how to create a simple grid layout resembling the keypad of a pocket calculator in a few steps.

.Grid Layout XML
[source,xml,indent=0]
----
include::{codedir}/UI/Layouts/app/src/main/res/layout/activity_grid.xml[tag=layout]
----

[[img-layouts-grid]]
.Grid Layout Editor
image::UI/layouts_grid.png[Grid Layout Editor]

=== Relative Layout

The ((Relative Layout)) is used to place widgets relative to one another; it is a bit simpler to understand than the Constraint Layout, but it is also less powerful.

.Relative Layout XML
[source,xml,indent=0]
----
include::{codedir}/UI/Layouts/app/src/main/res/layout/activity_relative.xml[tag=layout]
----

[[img-layouts-relative]]
.Relative Layout Editor
image::UI/layouts_relative.png[Relative Layout Editor]

== Summary

Activities are the basic building block of Android applications. Every activity manages a screenful of information, just like `UIViewController` instances would on iOS. The UI of activities can be designed visually, just like with Interface Builder in Xcode. Attach event handlers to UI widgets directly using Kotlin lambdas, instead of using Java anonymous classes, which makes code easier to write, read and maintain.

Intents are objects used to launch other activities, either in this application or in any other application in the system. They provide the glue that allow applications to talk to each other.

Applications should be built around Fragments, to ensure their adaptation to other device sizes, such as tablets. In that sense, Activities should be seen as containers for fragments, allowing them to collaborate and to share information at runtime.

Finally, the whole operating system works like a giant `UINavigationController` instance, onto which Activity instances are constantly pushed and popped, and this explains the importance of the back button in Android devices.

