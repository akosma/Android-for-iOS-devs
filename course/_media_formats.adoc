[[appendix_media_formats]]
= Supported Media Formats

This table summarizes the most important media formats supported by Android.footnote:[Source: https://developer.android.com/guide/appendix/media-formats.html]

.Image
[format="csv", options="header"]
|===
include::{datadir}/_appendix/image_formats.csv[]
|===

.Audio
[format="csv", options="header"]
|===
include::{datadir}/_appendix/audio_formats.csv[]
|===

.Video
[format="csv", options="header"]
|===
include::{datadir}/_appendix/video_formats.csv[]
|===


