[[chapter_debugging]]
= Debugging

Writing software is a difficult activity, and in Android the chances for things to go wrong are multiplied by the astronomical number of devices available in the market. This chapter will show some useful techniques to create, debug and troubleshoot apps in different environments and with different tools.

== TL;DR

For those of you in a hurry, the table below summarizes the most important pieces of information in this chapter.

[[table_debugging_apps]]
.Debugging Android Apps
[format="csv", options="header"]
|===
include::{datadir}/Debugging/tldr.csv[]
|===

== Enabling Exception Breakpoints

The first thing that I recommend you to do is to enable ((exception breakpoints)), either caught or uncaught. This will enable Android Studio to stop the execution of the application in case an Exception occurs, and given the wide range of possibilities for errors, this can be a handy measure before any debugging session starts.

To do that, just open the menu:Run[View Breakpoints…] (or hit the kbd:[Shift + {commandkey} + F8] keyboard shortcut) and check the "Java Exception Breakpoint" and the "Any Exception" checkboxes.

[[img-exception-breakpoint]]
.Breakpoints Window in Android Studio
image::Debugging/Breakpoints_window.png[Breakpoints Window in Android Studio]

== Enabling USB Debugging

If you have reached this point in the book one could imagine very well that you have been able to successfully run code in your device; but for those readers who have jumped directly to this section, here is a quick recap of the steps required to ((debug applications in your device)).

First, you must enable "((Developer mode))" in your device. Open the Settings application and scroll to the "About phone" section. In that screen you should see a "Build number" entry, which *you must tap seven times*. If you try to do it again, you should see a "toast" message just like the one shown below.

[[img-developer-toast]]
.Developer mode already active
image::Debugging/Already_developer.png[Developer mode already active]

After you have done that, the Settings application will display a new "Developer" item. Select it, scroll down and you will see a toggle switch to enable USB debugging in the device.

[[img-usb-debugging]]
.USB Debugging switch
image::Debugging/Developer_options.png[USB Debugging switch]

== Enabling WiFi Debugging

The Android debugger can also be used via a standard WiFi connection, although this can be a security problem and you should remember to disable this capability when you are done.

First connect your device with the USB cable and make sure that ADB is running in a specific port:

    $ adb tcpip 5555

To enable ((WiFi debugging)), then issue the following command, to retrieve the IP address of the device:

    $ adb -s e99b50ed shell ifconfig

The common output of the `ifconfig` command returns information for each of the interfaces available in the device; normally the one labeled `wlan0` is the one you are looking for. Write down the IP address, disconnect the USB cable and then run the next command:

    $ adb connect 192.168.1.xxx:5555

The `adb devices` command should show something similar to the following now:

    $ adb devices -l
    List of devices attached
    192.168.1.xxx:5555     device product:OnePlus3 model:ONEPLUS_A3003 device:OnePlus3

If you see the above, you can now use the device from Android Studio or any other IDE, and you will be able to debug your application just as if it were running tethered through the standard USB cable.

At the end of your debugging session, make sure to type the command to disable wifi debugging in your device.

    $ adb -s 192.168.1.xxxx:5555 usb

This is a security measure, to disable any attempts in any network to debug and inspect applications in your device. After running this command, any attempt to connect to your device should print the following output:

    $ adb connect
    unable to connect to 192.168.1.xxx:5555: Connection refused.

TIP: Some devices offer a menu entry in the Developer settings called "ADB over network" which serves the same purpose, allowing you to enable and disable the setting visually, as shown in the picture below. (Source: http://stackoverflow.com/a/10236938/133764)

[[img-adb-network]]
.ADB over network option
image::Debugging/ADB_network.png[ADB over network option (Source: http://stackoverflow.com/a/10236938/133764)]

== Working on the Command Line

I am a bit of a ((command line)) junkie, and I like being able to perform as many tasks as possible using my preferred macOS terminal tools: http://iterm2.com[iTerm 2], http://www.zsh.org[zsh] and https://tmux.github.io[tmux]. In this section we are going to see how easy it is to leave Android Studio aside for a while, and use command line tools to manage Android applications.

First, open your preferred terminal and make sure that you have all the required tools on your system:

    $ env | grep ANDROID
    ANDROID_HOME=/Users/adrian/Library/Android/sdk

    $ which adb
    /Users/adrian/Library/Android/sdk/platform-tools/adb

Then `cd` into the folder of any of the applications provided with this book and run the following command to build the application directly from the command line:

    $ ./gradlew build

Let us install the application in the device. First, connect your Android device to your Mac and run the following command to verify that ((ADB)) is connected to it:

    $ adb devices -l
	List of devices attached
	e99b50ed               device usb:337772544X product:OnePlus3 model:ONEPLUS_A3003 device:OnePlus3

TIP: If your device does not appear, just plug and unplug the USB cable of the device, and make sure that you have enabled USB debugging in the device.

Of course, in your case the output will be different; this is what I see when I connect my own OnePlus 3 device with the USB cable.

To install the debug build in my device, I just have to run the following command:

    $ ./gradlew installDebug

This will install the application in all the devices currently connected to the `adb` daemon. Alternatively, you can also install your application in just one device using the `adb` command:

    $ adb -s e99b50ed install app/build/outputs/apk/debug/app-debug.apk

[TIP]
.Taking screenshots using adb
====
As mentioned previously in this book, you can easily take ((screenshots using ADB)) with the following commands:

    $ adb -s e99b50ed shell /system/bin/screencap -p /sdcard/screenshot.png
    $ adb -s e99b50ed pull /sdcard/screenshot.png screenshot.png
====

Once the application is installed, how about debugging it? The Android toolkit allows you to debug applications entirely through the command line. The ADB process can bridge debugger commands to the Android Runtime, by the means of port forwarding. Let us see how to do that.

Launch the application in your device and then retrieve the process ID in your device:

    $ adb -s e99b50ed jdwp
    27496

((JDWP)) stands for http://docs.oracle.com/javase/1.5.0/docs/guide/jpda/jdwp-spec.html[Java Debug Wire Protocol], a standard defined for Java debuggers, and which the Android platform implements, albeit in limited form. The technique we are going to use basically uses ADB to connect a local instance of `jdb` (the standard Java debugger distributed with the Java SDK) to the process running in the device.

The last command we ran returns the number of the process in the Android Runtime of the device. We are going to use that number to connect all the pieces together.

First, let us set a debug bridge between `jdb` and the device:

    $ adb forward tcp:7777 jdwp:27496

Then, launch the Java debugger and start a debugging session:

    $ jdb -sourcepath src -attach localhost:7777

A short debugger session then looks more or less like this:

----
Set uncaught java.lang.Throwable
Set deferred uncaught java.lang.Throwable
Initializing jdb ...
> stop in training.akosma.commandline.MainActivity.OnCreate
Unable to set breakpoint training.akosma.commandline.MainActivity.OnCreate : No method OnCreate in training.akosma.commandline.MainActivity
> stop in training.akosma.commandline.MainActivity.onCreate
Set breakpoint training.akosma.commandline.MainActivity.onCreate
>
Breakpoint hit: "thread=main", training.akosma.commandline.MainActivity.onCreate(), line=12 bci=0
12            super.onCreate(savedInstanceState);

main[1] list
8        /** Called when the activity is first created. */
9        @Override
10        public void onCreate(Bundle savedInstanceState)
11        {
12 =>         super.onCreate(savedInstanceState);
13            setContentView(R.layout.main);
14        }
15    }
main[1] next
>
Step completed: "thread=main", training.akosma.commandline.MainActivity.onCreate(), line=13 bci=3
13            setContentView(R.layout.main);

main[1] list
9        @Override
10        public void onCreate(Bundle savedInstanceState)
11        {
12            super.onCreate(savedInstanceState);
13 =>         setContentView(R.layout.main);
14        }
15    }
main[1] help
** command list **
<SNIP>
main[1] cont
> quit
----

The previous listing shows several debugger commands:

- `stop in` to create a breakpoint.
- `list` to show the current status of the instruction pointer upon hitting a breakpoint.
- `next` to "step over" to the next instruction.
- `cont` to continue the execution.
- `help` to learn more about other commands.

[WARNING]
.Quit Android Studio
====
If you are not able to perform the command-line debugger steps delineated above, make sure to quit Android Studio, as it has it uses its own `adb` daemon and this might interfere with the operations.
====

== Logcat and `pidcat`

If you use the command line frequently, you will start missing the ((logcat)) output displayed by Android Studio at the bottom of the IDE. If that is the case, and you are using (as you should) logcat commands in your application, you should install then https://github.com/JakeWharton/pidcat[((pidcat))], a logging tool that provides color output in the terminal, and which can be restrained to only display the logs for the application you are interested in.

pidcat fulfills the same role as http://www.libimobiledevice.org[libimobiledevice] or https://github.com/rpetrich/deviceconsole[deviceconsole] for iOS.

You can install it very easily using Homebrew: `brew install pidcat`. Once installed, just call it using this command:

    $ pidcat training.akosma.pidcatexample

The console should display something similar to the following:

[[src-pidcat-output]]
.Example output with pidcat
image::Debugging/PIDCat_example.png[Example output with pidcat]

The pidcat help text shows that the tool allows to filter entries by verbosity level or by device or emulator, among other options.

    $ pidcat --help

----
usage: pidcat [-h] [-w N] [-l {V,D,I,W,E,F,v,d,i,w,e,f}] [--color-gc]
              [--always-display-tags] [--current] [-s DEVICE_SERIAL] [-d] [-e]
              [-c] [-t TAG] [-i IGNORED_TAG] [-v] [-a]
              [package [package ...]]

Filter logcat by package name

positional arguments:
  package               Application package name(s)

optional arguments:
  -h, --help            show this help message and exit
  -w N, --tag-width N   Width of log tag
  -l {V,D,I,W,E,F,v,d,i,w,e,f}, --min-level {V,D,I,W,E,F,v,d,i,w,e,f}
                        Minimum level to be displayed
  --color-gc            Color garbage collection
  --always-display-tags
                        Always display the tag name
  --current             Filter logcat by current running app
  -s DEVICE_SERIAL, --serial DEVICE_SERIAL
                        Device serial number (adb -s option)
  -d, --device          Use first device for log input (adb -d option)
  -e, --emulator        Use first emulator for log input (adb -e option)
  -c, --clear           Clear the entire log before running
  -t TAG, --tag TAG     Filter output by specified tag(s)
  -i IGNORED_TAG, --ignore-tag IGNORED_TAG
                        Filter output by ignoring specified tag(s)
  -v, --version         Print the version number and exit
  -a, --all             Print all log messages
----

This is an invaluable tool to be able to quickly scan for specific values in the logger output of your applications. A similar tool, but this time with a graphical user interface for macOS is https://github.com/yepher/LogCat[LogCat].

== NSLogger

Although PID Cat is useful, sometimes it is not possible or desirable to debug application instances individually using the USB cable. For more complex debugging scenarios, https://github.com/fpillet/NSLogger/[NSLogger] is a useful option.

((NSLogger)) allows developers to gather logging information from running applications in the local network. To do that, developers must include the NSLogger https://github.com/fpillet/NSLogger/tree/master/Client%20Logger/Android[NSLogger Android client] in their applications. When the application runs, the code automatically tries to send all of its logging information to an application running on a Mac on the same local network as the devices. Developers can then watch live the updates of their applications and thus to troubleshoot problems in specific devices.

To install NSLogger in your own app, at the time of this writing there is no simple mechanism (like, for example, using Gradle.) The code of NSLogger must be included in the application, but since the linkage is done through static methods and properties, the Kotlin compiler can strip the code out of the application if not used (for example, in Release builds.)

Download the zip from the https://github.com/fpillet/NSLogger[main Github repository of NSLogger] and copy the folder inside of the `Client Logger/Android/client-code/src` into the `app/src/main/java` folder of your own application. Android Studio should detect the new code automatically.

Copy the files in the `Client Logger/Android/example/com/example/` folder using Finder and paste them in Android Studio as part of your application.

Add the following ((permissions)) to your application in the AndroidManifest.xml file:

[[src-nslogger-android-manifest]]
.Permissions for NSLogger
[source,xml]
----
include::{codedir}/Debugging/NSLogger/app/src/main/AndroidManifest.xml[lines="5..8"]
----

Include your logging code, either in your custom subclass of the `Application` class in your project, or in your activity, to activate logging.

[[src-nslogger-activity]]
.Sending NSLogger calls
[source,kotlin]
----
include::{codedir}/Debugging/NSLogger/app/src/main/java/training/akosma/nslogger/MainActivity.kt[tags=nslogger]
----

IMPORTANT: Change the IP address to match the one where the client NSLogger application is running.

Before launching your application in Android Studio, make sure to download and launch the https://github.com/fpillet/NSLogger/releases[((NSLogger desktop viewer)) application] in your Mac. You should configure so that the port used by the client code is the same as the one used in the desktop viewer.

[[img-nslogger-conf]]
.NSLogger desktop viewer configuration
image::Debugging/NSLogger_settings.png[NSLogger desktop viewer configuration]

Having done this, and after adding some logging calls in your code, launching and using the application should automatically open a new NSLogger window in your Mac, displaying something similar to the contents of the following screenshot.

[[img-nslogger-app]]
.NSLogger application running
image::Debugging/NSLogger_session.png[NSLogger application running]

TIP: The contents of the NSLogger macOS application window can be saved into a file with the `.nsloggerdata` extension. The source code of this book includes a sample NSLogger file in the Debugging/NSLogger folder.

== Stetho

http://facebook.github.io/stetho/[((Stetho))] is a tool created by Facebook to debug Android applications using the https://www.google.com/chrome/index.html[Google Chrome] debugging tools. It is very simple to use and can be helpful to inspect and edit the user interface of applications, either on the device or on an emulator.

[NOTE]
.Follow along
====
The code of this section is located in the `Debugging/StethoSample` folder.
====

To use it, just add the required dependencies in the module Gradle file:

[[src-debugging-stetho-gradle]]
.Stetho dependencies in the module Gradle file
[source,groovy,indent=0]
----
include::{codedir}/Debugging/StethoSample/app/build.gradle[tags=gradle]
----

After adding the dependencies, create a subclass of `android.app.Application` in your project:

[[src-debugging-stetho-app]]
.Application subclass
[source,kotlin,indent=0]
----
include::{codedir}/Debugging/StethoSample/app/src/main/java/training/akosma/stethosample/StethoSampleApplication.kt[tags=usage]
----

Of course, you need to declare this new subclass in your `AndroidManifest.xml` file:

[[src-debugging-stetho-manifest]]
.AndroidManifest.xml file with the new Application subclass
[source,xml,indent=0]
----
include::{codedir}/Debugging/StethoSample/app/src/main/AndroidManifest.xml[tags=app]
----

Launch the application, either on the device or the simulator, and open ((Google Chrome)). Navigate to `chrome://inspect/#devices` and you should see something similar to the following image:

[[img-debugging-stetho-chrome-1]]
.Google Chrome showing available devices for debugging
image::Debugging/Stetho_chrome_1.png[Google Chrome showing available devices for debugging]

Clicking on the "inspect" link will open a developer tool window, which will be extremely familiar to those using Google Chrome for frontend web development. This editor can be used to inspect the UI of the application, drilling down until finding the right component, and can be used to modify the state of the interface as shown in the screenshot below:

[[img-debugging-stetho-chrome-2]]
.Editing the UI using Stetho
image::Debugging/Stetho_chrome_2.png[Editing the UI using Stetho]

Stetho can be used to inspect the state of SQLite databases, but there is a very useful module available https://github.com/uPhyca/stetho-realm[to inspect the state of Realm databases].

== Summary

Android applications can not only be debugged on the Android Studio IDE, but also on the command line; remember that Android Studio can be seen as a huge user interface built on top of Gradle and ADB. Other tools, such as NSLogger, Stetho and pidcat, provide additional services for inspecting the behavior of applications in different devices.

