DIR = _build
INPUT = master
OUTPUT = Android_for_iOS_Devs_Kotlin_Ed_2018
PRESENTATIONS = presentations
DIAGRAM = --require=asciidoctor-diagram
MATH = --require=asciidoctor-mathematical
REQUIRES = ${DIAGRAM}
OUTPUT_FOLDER = --destination-dir=${DIR}
MANPAGE = --backend=manpage
HTML = --backend=html5 --attribute max-width=70em
RAW_HTML = --backend=html5 --attribute stylesheet! --attribute source-highlighter!
PDF =  --backend=pdf --require=asciidoctor-pdf
EPUB = --backend=epub3 --require=asciidoctor-epub3
KINDLE = ${EPUB} --attribute ebook-format=kf8

# Public targets

all: html raw_html pdf epub kindle

html: _build/Android_for_iOS_Devs_Kotlin_Ed_2018.html

raw_html: _build/Android_for_iOS_Devs_Kotlin_Ed_2018.raw.html

pdf: _build/Android_for_iOS_Devs_Kotlin_Ed_2018.pdf

epub: _build/Android_for_iOS_Devs_Kotlin_Ed_2018.epub

kindle: _build/Android_for_iOS_Devs_Kotlin_Ed_2018.mobi

clean:
	if [ -d ".asciidoctor" ]; \
		then rm -r .asciidoctor; \
	fi; \
	if [ -d "${DIR}" ]; \
		then rm -r ${DIR}; \
	fi; \

stats:
	wc -w course/*.adoc

slides:
	cd ${PRESENTATIONS}; \
	./build.py; \
	cd ..

slides_pdf: create_html slides
	cd ${PRESENTATIONS}; \
	osascript pdf.scpt; \
	mv *.pdf ../_build; \
	cd ..

reduce_image_size:
	find . -name '*.png'  -exec pngquant --force --ext '.png' {} +

clean_code:
	find code -type d -name 'build' -exec rm -r {} +; \
	find code -type d -name '.gradle' -exec rm -r {} +; \
	find code -type d -name 'node_modules' -exec rm -r {} +; \

zip: clean_code html raw_html pdf epub kindle
	cd ${DIR}; \
	cp -R ../code ./${OUTPUT}_code; \
	zip -r ${OUTPUT}_code.zip ${OUTPUT}_code; \
	rm -r ${OUTPUT}_code; \
	zip -r ${OUTPUT}_book.zip ${OUTPUT}.pdf ${OUTPUT}.raw.html ${OUTPUT}.html ${OUTPUT}.epub ${OUTPUT}.mobi; \

# Private targets

_build/Android_for_iOS_Devs_Kotlin_Ed_2018.html:
	asciidoctor ${HTML} ${REQUIRES} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.html ${INPUT}.adoc; \

_build/Android_for_iOS_Devs_Kotlin_Ed_2018.raw.html:
	asciidoctor ${RAW_HTML} ${REQUIRES} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.raw.html ${INPUT}.adoc; \

_build/Android_for_iOS_Devs_Kotlin_Ed_2018.pdf:
	asciidoctor ${PDF} ${REQUIRES} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.pdf ${INPUT}.adoc; \

_build/Android_for_iOS_Devs_Kotlin_Ed_2018.epub:
	asciidoctor ${EPUB} ${REQUIRES} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.epub ${INPUT}.adoc; \

_build/Android_for_iOS_Devs_Kotlin_Ed_2018.mobi:
	asciidoctor ${KINDLE} ${REQUIRES} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.mobi ${INPUT}.adoc; \
	if [ -e "${DIR}/${OUTPUT}-kf8.epub" ]; \
		then rm ${DIR}/${OUTPUT}-kf8.epub; \
	fi; \

