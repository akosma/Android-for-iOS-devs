footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Multimedia

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

|                       | Android                           | iOS                           |
|-----------------------|-----------------------------------|-------------------------------|
| Display image         | `android.widget.ImageView`        | `UIImageView`                 |
| Display video         | `android.widget.VideoView`        | `MPMoviePlayerViewController` |
| Viewer application    | Gallery                           | Photos                        |
| Image picker          | `Intent.ACTION_PICK`              | `UIImagePickerController`     |
| Audio recorder        | `android.media.MediaRecorder`     | `AVAudioRecorder`             |
| Audio player          | `android.media.MediaPlayer`       | `AVAudioPlayer`               |
| Text to speech engine | `android.speech.tts.TextToSpeech` | `AVSpeechSynthesizer`         |

---

![inline](../images/Multimedia/Image_picker.png)
