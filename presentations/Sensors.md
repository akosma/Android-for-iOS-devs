footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Sensors

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

| Name                | Sensor Identifier          | Type                 | Unit         | Available in Emulator |
|---------------------|----------------------------|----------------------|--------------|-----------------------|
| Linear Acceleration | `TYPE_LINEAR_ACCELERATION` | Software or Hardware | m/s^2^       |                       |
| Accelerometer       | `TYPE_ACCELEROMETER`       | Hardware             | m/s^2^       | Yes                   |
| Air Pressure        | `TYPE_PRESSURE`            | Hardware             | hPa or mbar  | Yes                   |
| Gravity             | `TYPE_GRAVITY`             | Software or Hardware | m/s^2^       |                       |
| Gyroscope           | `TYPE_GYROSCOPE`           | Hardware             | rad/s        |                       |
| Humidity            | `TYPE_RELATIVE_HUMIDITY`   | Hardware             | %            | Yes                   |
| Light               | `TYPE_LIGHT`               | Hardware             | lx           | Yes                   |
| Magnetometer        | `TYPE_MAGNETIC_FIELD`      | Hardware             | μT           | Yes                   |
| Orientation         | `TYPE_ORIENTATION`         | Software             | rad          |                       |
| Proximity           | `TYPE_PROXIMITY`           | Hardware             | cm           | Yes                   |
| Rotation            | `TYPE_ROTATION_VECTOR`     | Software or Hardware |              |                       |
| Temperature         | `TYPE_AMBIENT_TEMPERATURE` | Hardware             | °C (Celsius) | Yes                   |

---

|                  | Android               | iOS                                                 |
|------------------|-----------------------|-----------------------------------------------------|
| Framework        | `android.hardware`    | Core Motion & Core Location                         |
| Main class       | `SensorManager`       | `CMMotionManager`                                   |
| Callback methods | `SensorEventListener` | Blocks                                              |
| Sensor data      | `SensorEvent`         | `CMGyroData` – `CMAccelerometerData` – `CMAttitude` |
| Location         | `LocationManager`     | `CLLocationManager`                                 |

---

![inline](../images/Sensors/Accelerometer.png)

---

![inline](../images/Sensors/Sensors_list.png)
