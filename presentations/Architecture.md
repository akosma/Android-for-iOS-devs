footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Architecture

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

|                      | Android                        | iOS                            |
|----------------------|--------------------------------|--------------------------------|
| Notification center  | `LocalBroadcastManager`        | `NSNotificationCenter`         |
| Flexible designs     | Interface-oriented programming | Protocol-oriented programming  |
| Dependency injection | Dagger                         | Typhoon – Swinject – Cleanse … |
| ReactiveX            | RxJava & RxAndroid             | RxSwift                        |
| Observing data       | `rx.Observable`                | `willSet` & `didSet` – KVO     |
