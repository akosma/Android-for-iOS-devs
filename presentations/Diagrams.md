footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# UML Diagrams

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

![inline](../images/diagram-activity-states.png)


---

![inline](../images/diagram-animator-classes.png)


---

![inline](../images/diagram-compilation.png)


---

![inline](../images/diagram-context-class.png)


---

![inline](../images/diagram-edittext-classes.png)


---

![inline](../images/diagram-exceptions.png)


---

![inline](../images/diagram-string-classes.png)


---

![inline](../images/diagram-view-classes.png)

