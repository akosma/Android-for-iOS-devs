footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# _appendix

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

| Extension                 | Container Format        | Data Format/Codec |
|---------------------------|-------------------------|-------------------|
| .3gp – .mp4 – .m4a – .aac | 3GPP                    | AAC – AMR         |
| .flac                     | FLAC                    | FLAC              |
| .mp3                      | MP3                     | MP3               |
| .mid – .xmf – .rtx – .ota | MIDI Type 0 – RTTTL/RTX |  MIDI             |
| .ogg                      | Ogg                     | Vorbis            |
| .wav                      | PCM – WAVE              | WAVE              |
| .mkv                      | Matroska                | Opus              |

---

| Extension | Container Format | Data Format/Codec |
|-----------|------------------|-------------------|
| .jpg      | JPEG             | JPEG              |
| .gif      | GIF              | GIF               |
| .png      | PNG              | PNG               |
| .bmp      | BMP              | BMP               |
| .webp     | WebP             | WebP              |

---

| Name                     | Kind                         | Link                                                     |
|--------------------------|------------------------------|----------------------------------------------------------|
| ACRA                     | Crash reporter               | http://acra.ch/                                          |
| Android Dev Metrics      | Performance library          | https://github.com/frogermcs/AndroidDevMetrics/          |
| Android File Transfer    | File management              | https://www.android.com/filetransfer/                    |
| Appium                   | UI testing                   | http://appium.io/                                        |
| Briefs                   | UI prototyping               | http://giveabrief.com/                                   |
| Butter Knife             | UI library                   | http://jakewharton.github.io/butterknife/                |
| Dagger                   | Dependency injection         | http://google.github.io/dagger/                          |
| Droid @ Screen           | Screen sharing               | http://droid-at-screen.org/                              |
| FindBugs Static Analyzer | Static analyzer              | http://findbugs.sourceforge.net/                         |
| Genymotion               | Android Emulator             | https://www.genymotion.com/                              |
| Glide                    | Network library              | https://github.com/bumptech/glide                        |
| Gson                     | Serialization library        | https://github.com/google/gson/                          |
| JUnit                    | Unit testing                 | http://junit.org/                                        |
| LogCat                   | Log viewer                   | https://github.com/yepher/LogCat/                        |
| NimbleDroid              | Performance analysis service | https://nimbledroid.com/                                 |
| OkHttp                   | Network library              | https://square.github.io/okhttp/                         |
| OrmLite                  | Storage library              | http://ormlite.com/                                      |
| Origami                  | UI prototyping               | http://origami.design/                                   |
| PaintCode                | UI prototyping               | https://www.paintcodeapp.com/                            |
| PID cat                  | Log viewer                   | https://github.com/JakeWharton/pidcat/                   |
| Realm                    | Storage library              | https://realm.io/                                        |
| Reflector 2              | Screen sharing               | http://www.airsquirrels.com/reflector/                   |
| Retrofit                 | Networking library           | https://square.github.io/retrofit/                       |
| Retrolambda              | Library                      | https://github.com/orfjackal/retrolambda/                |
| Robolectric              | Unit testing runner          | http://robolectric.org/                                  |
| Robotium                 | UI testing                   | https://github.com/RobotiumTech/robotium/                |
| Socket.io                | Network library              | http://socket.io/                                        |
| SQLCipher for Android    | Storage library              | https://www.zetetic.net/sqlcipher/sqlcipher-for-android/ |
| Stetho                   | Debugging tool               | http://facebook.github.io/stetho/                        |
| Vysor                    | Screen sharing               | http://vysor.io/                                         |

---

| Name             | Programming Language | Link                                         |
|------------------|----------------------|----------------------------------------------|
| Corona           | Lua                  | https://coronalabs.com/                      |
| Delphi           | Object Pascal        | https://www.embarcadero.com/products/delphi/ |
| Kivy             | Python               | https://kivy.org/                            |
| Kotlin           | Kotlin               | http://kotlinlang.org/                       |
| Lazarus          | Free Pascal          | http://www.lazarus-ide.org/                  |
| Processing       | Processing           | https://processing.org/                      |
| Qt               | C++                  | https://www.qt.io/                           |
| RubyMotion       | Ruby                 | http://www.rubymotion.com/                   |
| Scala on Android | Scala                | http://scala-android.org/                    |
| Silver           | Swift                | http://elementscompiler.com/elements/silver/ |
| Visual Studio    | C++                  | https://www.visualstudio.com/                |
| Xamarin          | C#                   | https://www.xamarin.com/                     |

---

| Shortcut                       | Purpose                                              |
|--------------------------------|------------------------------------------------------|
| kbd:[{commandkey} + L]                  | Go to line                                           |
| kbd:[Option + Return]          | Autocomplete imports                                 |
| kbd:[Ctrl + {commandkey} + F12]         | Maximize/minimize editor                             |
| kbd:[Ctrl + G]                 | Start multiple select/replace session                |
| kbd:[{commandkey} + O]                  | Find class                                           |
| kbd:[{commandkey} + Shift + A]          | Find action                                          |
| kbd:[Shift + Shift]            | Quick search everywhere                              |
| kbd:[{commandkey} + Shift + O]          | Quick open file                                      |
| kbd:[Option + ↓]               | Jump to Lint issue in code                           |
| kbd:[{commandkey} + E]                  | Recently opened files pop-up                         |
| kbd:[{commandkey} + N]                  | Generate code (getters, setters, constructors, etc.) |
| kbd:[Ctrl + T]                 | Refactor selected element                            |
| kbd:[Shift + F6]               | Rename refactoring                                   |
| kbd:[{commandkey} + O]                  | Override methods                                     |
| kbd:[{commandkey} + D]                  | Duplicate current line                               |
| kbd:[Ctrl + Space]             | Basic code completion                                |
| kbd:[{commandkey} + U]                  | Go to super method/class                             |
| kbd:[{commandkey} + /]                  | Comment/uncomment current line(s)                    |
| kbd:[{commandkey} + Shift + /]          | Comment/uncomment current line(s) with C style       |
| kbd:[{commandkey} + Option + L]         | Reformat code                                        |
| kbd:[Shift + {commandkey} + Option + L] | Reformat code dialog                                 |
| kbd:[Control + Option + O]     | Optimize imports                                     |
| kbd:[Ctrl + R]                 | Build and run                                        |
| kbd:[Ctrl + D]                 | Debug                                                |
| kbd:[F8]                       | Step over                                            |
| kbd:[F7]                       | Step into                                            |
| kbd:[{commandkey} + F8]                 | Toggle breakpoint                                    |
| kbd:[Shift + {commandkey} + F8]         | Open Breakpoints window                              |
| kbd:[{commandkey} + Option + M]         | Extract method                                       |
| kbd:[{commandkey} + Shift + T]          | Edit or create test for current class                |
| kbd:[Ctrl + Shift + R]         | Run current test                                     |

---

| Extension    | Container Format | Data Format/Codec |
|--------------|------------------|-------------------|
| .3gp – .mp4  | 3GPP – MPEG-4    | H.263 – H.265     |
| .mp4         | MPEG-4           | H.265             |
| .webm – .mkv | WebM – Matroska  | VP8 – VP9         |
