#!/usr/bin/env python

import csvconverter
import os
import codecs
import datetime

def make_presentation(name, title, year):
    presentation = codecs.open("./" + name + ".md", "w", "utf-8")
    presentation.write("footer: :copyright: Adrian Kosmaczewski - " + year + " - All Rights Reserved\n")
    presentation.write("slidenumbers: true\n")
    presentation.write("theme: Zurich, 6\n\n")
    presentation.write("# " + title + "\n\n")
    presentation.write("## Android for iOS Developers\n")
    presentation.write("### Adrian Kosmaczewski\n")
    presentation.write("![inline left 30%](../images/logo.png)\n")
    return presentation

def add_page(presentation):
    presentation.write("\n---\n\n")

def get_year():
    now = datetime.datetime.now()
    year = str(now.year)
    return year

def get_dirs(datadir):
    dirs = [name for name in os.listdir(datadir) if os.path.isdir(os.path.join(datadir, name))]
    return dirs

def generate_chapters(datadir, imagesdir, dirs, year):
# Make a presentation of each chapter
# including all the tables and then all the images
    for dir in dirs:
        presentation = make_presentation(dir, dir, year)
        currentdir = datadir + "/" + dir
        files = os.listdir(currentdir)
        for file in files:
            add_page(presentation)
            path = currentdir + "/" + file
            table = csvconverter.make_table(path)
            presentation.write(table)

        currentdir = imagesdir + "/" + dir
        images = [f for f in os.listdir(currentdir) if f.endswith('.png')]
        for image in images:
            add_page(presentation)
            path = currentdir + "/" + image
            presentation.write("![inline](" + path + ")\n")

        presentation.close()

def generate_diagrams(imagesdir, year):
# Make a presentation with all the UML Diagrams
    diagrams = [f for f in os.listdir(imagesdir) if f.endswith('.png') and f.startswith('diagram-')]
    presentation = make_presentation("Diagrams", "UML Diagrams", year)
    for diagram in diagrams:
        add_page(presentation)
        path = imagesdir + "/" + diagram
        presentation.write("![inline](" + path + ")\n\n")

    presentation.close()

def generate_tldrs(datadir, dirs, year):
# Make a presentation with all the TL;DR tables
    presentation = make_presentation("tldr", "TL;DR", year)
    for dir in dirs:
        path = datadir + "/" + dir + "/tldr.csv"
        if os.path.isfile(path):
            add_page(presentation)
            presentation.write("# " + dir + "\n\n")
            table = csvconverter.make_table(path)
            presentation.write(table)

    presentation.close()

def main():
    datadir = "../data"
    imagesdir = "../images"
    dirs = get_dirs(datadir)
    year = get_year()
    generate_chapters(datadir, imagesdir, dirs, year)
    generate_diagrams(imagesdir, year)
    generate_tldrs(datadir, dirs, year)

if __name__ == "__main__":
    main()

