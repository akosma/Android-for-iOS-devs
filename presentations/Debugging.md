footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Debugging

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

|                  | Android          | iOS                              |
|------------------|------------------|----------------------------------|
| Debugger         | JDB              | LLDB                             |
| Log output       | logcat           | Xcode console                    |
| Remote debugging | yes              | no                               |
| Log viewers      | PID Cat & LogCat | libimobiledevice & deviceconsole |
| Network logger   | NSLogger         | NSLogger                         |

---

![inline](../images/Debugging/ADB_network.png)

---

![inline](../images/Debugging/Already_developer.png)

---

![inline](../images/Debugging/Breakpoints_window.png)

---

![inline](../images/Debugging/Developer_options.png)

---

![inline](../images/Debugging/NSLogger_session.png)

---

![inline](../images/Debugging/NSLogger_settings.png)

---

![inline](../images/Debugging/PIDCat_example.png)
