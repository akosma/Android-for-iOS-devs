footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# Toolchain

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

| Code Name          | Version Number | Release Date       | API Level | Support status        | %     |
|--------------------|----------------|--------------------|-----------|-----------------------|-------|
| Alpha              | 1.0            | September 23, 2008 | 1         | Discontinued          | –     |
| Beta               | 1.1            | February 9, 2009   | 2         | Discontinued          | –     |
| Cupcake            | 1.5            | April 27, 2009     | 3         | Discontinued          | –     |
| Donut              | 1.6            | September 15, 2009 | 4         | Discontinued          | –     |
| Eclair             | 2.0 - 2.1      | October 26, 2009   | 5 - 7     | Discontinued          | –     |
| Froyo              | 2.2 - 2.2.3    | May 20, 2010       | 8         | Discontinued          | 0.1%  |
| Gingerbread        | 2.3 - 2.3.7    | December 6, 2010   | 9 - 10    | Discontinued          | 1.3%  |
| Honeycomb          | 3.0 - 3.2.6    | February 22, 2011  | 11 - 13   | Discontinued          | –     |
| Ice Cream Sandwich | 4.0 - 4.0.4    | October 18, 2011   | 14 - 15   | Discontinued          | 1.3%  |
| Jelly Bean         | 4.1 - 4.3.1    | July 9, 2012       | 16 - 18   | Discontinued          | 13.7% |
| KitKat             | 4.4 - 4.4.4    | October 31, 2013   | 19        | Security updates only | 25.2% |
| Lollipop           | 5.0 - 5.1.1    | November 12, 2014  | 21 - 22   | Supported             | 34.1% |
| Marshmallow        | 6.0 - 6.0.1    | October 5, 2015    | 23        | Supported             | 24%   |
| Nougat             | 7.0 - 7.1.1    | August 22, 2016    | 24 - 25   | Supported             | 0.3%  |

---

|                      | Java 1.7                                       | Objective-C 2.0                                     | Swift 3                                                    |
|----------------------|------------------------------------------------|-----------------------------------------------------|------------------------------------------------------------|
| Inheritance          | Simple, with interfaces                        | Simple, with protocols                              | Simple, with protocols and protocol extensions             |
| Semicolons           | Mandatory                                      | Mandatory                                           | Optional                                                   |
| Class definition     | `class`                                        | `@interface & @implementation`                      | `class`                                                    |
| Interfaces           | implements `interface`                         | conforms to `@protocol`                             | conforms to `protocol`                                     |
| Including code       | `import` (symbols)                             | `#import` (files)                                   | `import` (symbols)                                         |
| Class extensions     | no                                             | Categories                                          | Extensions                                                 |
| Dynamic typing       | no                                             | `id`                                                | `Any`                                                      |
| Private field suffix | `m` (Java standard)                            | `_` (underscore)                                    | `_` (underscore)                                           |
| Memory management    | Garbage collection                             | Manual or Automatic Reference Counting              | Automatic Reference Counting                               |
| Generics             | yes (type erasure)                             | yes (type erasure)                                  | yes                                                        |
| Method pointers      | no                                             | `@selector`                                         | `#selector`                                                |
| Callbacks            | Listeners via anonymous classes                | Delegate objects and blocks                         | Delegate objects and functions                             |
| Pointers             | no                                             | yes                                                 | Via library classes                                        |
| Root class           | `Object`                                       | `NSObject` / `NSProxy` / …                          | `NSObject` / `NSProxy` / …                                 |
| Visibility           | `public` / `protected` / `package` / `private` | `@public` / `@protected` / `@private` (only fields) | `open` / `public` / `internal` / `fileprivate` / `private` |
| Exception handling   | `try` / `catch` / `finally` + `Exception`      | `@try` / `@catch` / `@finally` + `NSException`      | `do` / `try` / `catch` + `Error`                           |
| Namespaces           | Packages                                       | Through class prefixes                              | Implicit, via modules                                      |

---

|                  | iOS Simulator                             | Android Emulator                            |
|------------------|-------------------------------------------|---------------------------------------------|
| Type of Code     | x86                                       | ARM                                         |
| Hardware support | Limited: orientation, memory warnings, UI | Extended: camera, accelerometer, telephony… |

---

|                              | Android                           | iOS                   |
|------------------------------|-----------------------------------|-----------------------|
| IDE                          | Android Studio                    | Xcode                 |
| Profiling                    | Android Device Monitor            | Instruments           |
| Preview                      | Android Emulator                  | iOS Simulator         |
| Blocks in previous versions  | Retrolambda                       | PLBlocks              |
| Programming Language         | Java                              | Swift or Objective-C  |
| Command Line                 | `gradlew` – `ant`                 | `xcodebuild`          |
| Going beyond                 | Rooting                           | Jailbreaking          |
| Application metadata         | AndroidManifest.xml               | Info.plist            |
| Dependency Manager           | Gradle                            | CocoaPods – Carthage  |
| Distribution                 | APK                               | IPA                   |
| Debugger                     | ADB + DDMS                        | LLDB                  |
| Logger                       | LogCat                            | NSLog() or print()    |
| View Debugging               | Hierarchy viewer                  | Xcode view debugging  |
| Static Analysis              | Android Lint                      | Clang Static Analyzer |
| Classic programming language | Java                              | Objective-C           |
| Hype programming language    | Kotlin – Groovy – Scala – Clojure | Swift                 |

---

![inline](../images/Toolchain/Android_Menu.png)

---

![inline](../images/Toolchain/Android_Setup_Wizard_01.png)

---

![inline](../images/Toolchain/Android_Setup_Wizard_02.png)

---

![inline](../images/Toolchain/Android_Setup_Wizard_03.png)

---

![inline](../images/Toolchain/Android_Tips.png)

---

![inline](../images/Toolchain/Android_Virtual_Devices_01.png)

---

![inline](../images/Toolchain/Android_Virtual_Devices_02.png)

---

![inline](../images/Toolchain/Android_Virtual_Devices_03.png)

---

![inline](../images/Toolchain/Deployment_target.png)

---

![inline](../images/Toolchain/Emulator.png)

---

![inline](../images/Toolchain/Genymotion.png)

---

![inline](../images/Toolchain/Genymotion_main.png)

---

![inline](../images/Toolchain/Genymotion_settings.png)

---

![inline](../images/Toolchain/Javadoc_generator.png)

---

![inline](../images/Toolchain/Launcher_script.png)

---

![inline](../images/Toolchain/New_project_01.png)

---

![inline](../images/Toolchain/New_project_02.png)

---

![inline](../images/Toolchain/New_project_03.png)

---

![inline](../images/Toolchain/New_project_04.png)

---

![inline](../images/Toolchain/New_project_05.png)

---

![inline](../images/Toolchain/Plugin_browse.png)

---

![inline](../images/Toolchain/Run_configurations.png)

---

![inline](../images/Toolchain/SDK_Manager.png)

---

![inline](../images/Toolchain/SDK_Quickfix_01.png)

---

![inline](../images/Toolchain/SDK_Quickfix_02.png)

---

![inline](../images/Toolchain/Splash_screen.png)
