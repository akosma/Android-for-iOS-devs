footer: :copyright: Adrian Kosmaczewski - 2017 - All Rights Reserved
slidenumbers: true
theme: Zurich, 6

# UI

## Android for iOS Developers
### Adrian Kosmaczewski
![inline left 30%](../images/logo.png)

---

|                                | Android                    | iOS                           |
|--------------------------------|----------------------------|-------------------------------|
| UI design                      | Layout files               | NIB/XIB/Storyboard            |
| Controllers                    | Activity                   | UIViewController              |
| Callbacks                      | Anonymous Classes          | `IBAction`                    |
| Views                          | `android.view.View`        | `UIView`                      |
| Connecting views               | `findViewById(R.id.xxxxx)` | `IBOutlet`                    |
| Text fields                    | `EditText`                 | `UITextField`                 |
| Buttons                        | `Button`                   | `UIButton`                    |
| Text labels                    | `TextView`                 | `UILabel`                     |
| Translatable strings           | strings.xml                | Localizable.strings           |
| Navigation between controllers | Intent                     | Storyboard Segue              |
| UI decomposition               | Fragment                   | Children UIViewController     |
| Serialization                  | `Parcelable`               | `NSPropertyListSerialization` |
| Dialog boxes                   | `AlertDialog`              | `UIAlertController`           |

---

![inline](../images/UI/Hierarchy_viewer.png)

---

![inline](../images/UI/Lint_dialog.png)

---

![inline](../images/UI/Lint_results.png)

---

![inline](../images/UI/Strings_form.png)
