package training.akosma.location

import android.Manifest
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_main.*

// tag::class[]
class MainActivity : AppCompatActivity(),
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
// end::class[]

    private var googleApiClient: GoogleApiClient? = null

    // tag::setup[]
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        googleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onStart() {
        super.onStart()
        googleApiClient?.connect()
    }

    override fun onStop() {
        super.onStop()
        googleApiClient?.disconnect()
    }
    // end::setup[]

    // tag::connected[]
    override fun onConnected(bundle: Bundle?) {

        val fine = Manifest.permission.ACCESS_FINE_LOCATION
        val coarse = Manifest.permission.ACCESS_COARSE_LOCATION
        if (!hasPermission(fine) && !hasPermission(coarse)) {
            val permissions = arrayOf(fine, coarse)
            ActivityCompat.requestPermissions(this, permissions, 0)
            return
        }
        retrieveLocation()
    }

    private fun hasPermission(permission: String): Boolean {
        val check = ActivityCompat.checkSelfPermission(this, permission)
        return check == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 0) {
            var granted = true

            for (grantResult in grantResults) {
                granted = granted && grantResult == PERMISSION_GRANTED
            }

            if (granted) {
                retrieveLocation()
            }
        }
    }
    // end::connected[]

    override fun onConnectionSuspended(i: Int) {
        Log.e("MainActivity", "Connection suspended")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("MainActivity", "Connection failed: " + connectionResult.toString())
    }

    // tag::retrieve[]
    private fun retrieveLocation() {
        try {
            val request = LocationRequest.create()
            request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            request.numUpdates = 1
            request.interval = 0

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                    request
            ) { location ->
                val lat = location.latitude
                val lng = location.longitude
                latitudeView.text = lat.toString()
                longitudeView.text = lng.toString()
            }
        } catch (e: SecurityException) {
            Log.e("MainActivity", "Error: ", e)
        }

    }
    // end::retrieve[]
}
