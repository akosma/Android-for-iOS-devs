package training.akosma.sensorlist

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import kotlinx.android.synthetic.main.activity_main.*

// Adapted from
// http://stackoverflow.com/a/27691727/133764
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::sensorlist[]
        val manager = getSystemService(Context.SENSOR_SERVICE) as SensorManager  // <1>
        var sensorList: List<Sensor>? = null
        sensorList = manager.getSensorList(Sensor.TYPE_ALL)

        val layout = android.R.layout.simple_list_item_1
        var adapter: ListAdapter? = null
        if (sensorList != null) {
            adapter = ArrayAdapter(this, layout, sensorList)
        }
        listView.adapter = adapter
        // end::sensorlist[]
    }
}
