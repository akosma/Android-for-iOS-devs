package training.akosma.address

import android.app.IntentService
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.support.v4.os.ResultReceiver
import android.text.TextUtils
import android.util.Log

import java.io.IOException
import java.util.ArrayList
import java.util.Locale

class AddressService : IntentService(TAG) {
    protected var receiver: ResultReceiver? = null

    override fun onHandleIntent(intent: Intent?) {
        var error = ""
        receiver = intent?.getParcelableExtra(Constants.RECEIVER)
        val location = intent?.getParcelableExtra<Location>(Constants.LOCATION_DATA_EXTRA)

        if (location != null) {
            var addresses: List<Address>? = null

            try {
                // tag::geocoder[]
                val geocoder = Geocoder(this, Locale.getDefault())
                addresses = geocoder.getFromLocation(
                        location.latitude,
                        location.longitude,
                        1)
                // end::geocoder[]
            } catch (ioException: IOException) {
                error = getString(R.string.service_not_available)
                Log.e(TAG, error, ioException)
            } catch (illegalArgumentException: IllegalArgumentException) {
                error = getString(R.string.invalid_lat_long_used)
                Log.e(TAG, error + ". " +
                        "(" + location.latitude + ", " +
                        location.longitude + ")", illegalArgumentException)
            }

            if (addresses == null || addresses.size == 0) {
                if (error.isEmpty()) {
                    error = getString(R.string.no_address_found)
                    Log.e(TAG, error)
                }
                deliverResultToReceiver(Constants.FAILURE_RESULT, error)
            } else {
                val address = addresses[0]
                val fragments = ArrayList<String>()
                // getMaxAddressLineIndex() returns -1 if no addresses found.
                // Hence we add one and we have a count of addresses available.
                val count = address.maxAddressLineIndex + 1
                for (i in 0 until count) {
                    fragments.add(address.getAddressLine(i))
                }
                Log.i(TAG, getString(R.string.address_found))
                deliverResultToReceiver(Constants.SUCCESS_RESULT,
                        TextUtils.join(System.getProperty("line.separator"),
                                fragments))
            }
        }
    }

    // tag::send[]
    private fun deliverResultToReceiver(resultCode: Int, message: String) {
        val bundle = Bundle()
        bundle.putString(Constants.RESULT_DATA_KEY, message)
        receiver?.send(resultCode, bundle)
    }
    // end::send[]

    companion object {
        private val TAG = "AddressService"
    }
}
