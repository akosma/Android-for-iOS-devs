package training.akosma.address

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.os.ResultReceiver
import android.support.v7.app.AppCompatActivity
import android.util.Log

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

import android.content.pm.PackageManager.PERMISSION_GRANTED
import kotlinx.android.synthetic.main.activity_main.*

// This project adapted from
// https://github.com/googlesamples/android-play-location/tree/master/LocationAddress
class MainActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private var client: GoogleApiClient? = null
    private var lastLocation: Location? = null

    private var resultReceiver: AddressResultReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultReceiver = AddressResultReceiver(Handler())

        client = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    override fun onStart() {
        super.onStart()
        client!!.connect()
    }

    override fun onStop() {
        super.onStop()
        client!!.disconnect()
    }

    override fun onConnected(bundle: Bundle?) {
        val fine = Manifest.permission.ACCESS_FINE_LOCATION
        val coarse = Manifest.permission.ACCESS_COARSE_LOCATION
        if (!hasPermission(fine) && !hasPermission(coarse)) {
            val permissions = arrayOf(fine, coarse)
            ActivityCompat.requestPermissions(this, permissions, 0)
            return
        }
        retrieveLocation()
    }

    private fun hasPermission(permission: String): Boolean {
        val check = ActivityCompat.checkSelfPermission(this, permission)
        return check == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 0) {
            var granted = true

            for (grantResult in grantResults) {
                granted = granted && grantResult == PERMISSION_GRANTED
            }

            if (granted) {
                retrieveLocation()
            }
        }
    }

    override fun onConnectionSuspended(i: Int) {
        Log.e("MainActivity", "Connection suspended")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("MainActivity", "Connection failed: " + connectionResult.toString())
    }

    private fun retrieveLocation() {
        try {
            val request = LocationRequest.create()
            request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            request.numUpdates = 1
            request.interval = 0

            LocationServices.FusedLocationApi.requestLocationUpdates(client,
                    request
            ) { location ->
                val lat = location.latitude
                val lng = location.longitude
                view_latitude.text = lat.toString()
                view_longitude.text = lng.toString()
                lastLocation = location
                startIntentService()
            }
        } catch (e: SecurityException) {
            Log.e("MainActivity", "Error: ", e)
        }

    }

    // tag::intent[]
    protected fun startIntentService() {
        val intent = Intent(this, AddressService::class.java)
        intent.putExtra(Constants.RECEIVER, resultReceiver)
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, lastLocation)
        startService(intent)
    }
    // end::intent[]

    // tag::receive[]
    internal inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {

            // Display the address string
            // or an error message sent from the intent service.
            val address = resultData!!.getString(Constants.RESULT_DATA_KEY)
            view_address.text = address
        }
    }
    // end::receive[]
}
