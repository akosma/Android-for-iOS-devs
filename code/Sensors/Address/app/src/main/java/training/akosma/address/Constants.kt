package training.akosma.address

internal object Constants {
    val SUCCESS_RESULT = 0
    val FAILURE_RESULT = 1
    private val PACKAGE_NAME = "training.akosma.address"
    val RECEIVER = PACKAGE_NAME + ".RECEIVER"
    val RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY"
    val LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA"
}
