package training.akosma.accelerometer

import android.content.Context
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

import java.util.Locale

// tag::implements[]
class MainActivity : AppCompatActivity(), SensorEventListener {
// end::implements[]

    private var sensorManager: SensorManager? = null
    private var accelerometer: Sensor? = null

    // tag::oncreate[]
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    // end::oncreate[]

        // tag::create[]
        // <1>
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        if (sensorManager != null) {
            accelerometer = sensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        }
        // end::create[]
    }

    // tag::onchanged[]
    override fun onSensorChanged(sensorEvent: SensorEvent) {  // <2>
        val values = sensorEvent.values.clone()

        // Uncomment this to filter the values with a low-pass filter
        // values = lowPass(values);

        val xValue = values[0]
        val yValue = values[1]
        val zValue = values[2]

        view_x.text = String.format(Locale.ENGLISH, "%1.1f", xValue)
        view_y.text = String.format(Locale.ENGLISH, "%1.1f", yValue)
        view_z.text = String.format(Locale.ENGLISH, "%1.1f", zValue)

        val vector = Math.sqrt((xValue * xValue +
                yValue * yValue +
                zValue * zValue).toDouble())
        view_vector.text = String.format(Locale.ENGLISH, "%1.1f", vector)
        if (vector < 3) {
            activity_main.setBackgroundColor(Color.RED)
        } else if (vector < 10) {
            activity_main.setBackgroundColor(Color.GREEN)
        } else if (vector < 40) {
            activity_main.setBackgroundColor(Color.YELLOW)
        } else {
            activity_main.setBackgroundColor(Color.RED)
        }
    }
    // end::onchanged[]

    override fun onAccuracyChanged(sensor: Sensor, i: Int) {}

    // tag::register[]
    override fun onResume() {    // <3>
        super.onResume()
        sensorManager?.registerListener(this,
                accelerometer,
                SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {     // <4>
        super.onPause()
        sensorManager?.unregisterListener(this)
    }

    protected fun lowPass(input: FloatArray): FloatArray {    // <5>
        val acceleration = floatArrayOf(0f, 0f, 0f)
        val FACTOR = 0.1f

        for (i in 0..2) {
            acceleration[i] = input[i] * (1.0f - FACTOR)
        }
        return acceleration
    }
    // end::register[]
}
