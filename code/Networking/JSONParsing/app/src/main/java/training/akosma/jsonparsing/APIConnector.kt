package training.akosma.jsonparsing

import android.net.Uri
import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class APIConnector {
    var baseURL = "http://api.geonames.org/findNearbyWikipedia"
    var isFormatted = true
    var format = APIConnectorFormat.JSON

    // If you receive an error saying that this user has exceeded its limit
    // of that it is not registered, you can register one for free
    // in geonames.org
    var username = "steve"

    // default value
    val stringData: String
        @Throws(IOException::class)
        get() {
            val urlString = buildFullUrl()
            Log.i("APIConnector", "Connecting to URL: " + urlString)
            val url = URL(urlString)
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            conn.connectTimeout = 30000

            try {
                val inputStream = conn.inputStream
                if (conn.responseCode != HttpURLConnection.HTTP_OK) {
                    throw IOException(conn.responseMessage + " (" + urlString + ")")
                }
                return inputStream.bufferedReader().readText()
            } finally {
                conn.disconnect()
            }
        }

    private val objectData: JSONObject
        @Throws(IOException::class, JSONException::class)
        get() = JSONObject(stringData)

    internal val pointsOfInterest: List<PointOfInterest>
        @Throws(IOException::class, JSONException::class)
        get() {
            val points = ArrayList<PointOfInterest>()
            val json = objectData
            parsePointsOfInterest(points, json)
            return points
        }

    private fun buildFullUrl(): String {
        val url = baseURL + format.value
        val lat = 47.0
        val lng = 9.0
        val uri = Uri.parse(url).buildUpon()
                .appendQueryParameter("lat", lat.toString())
                .appendQueryParameter("lng", lng.toString())
                .appendQueryParameter("username", username)
                .appendQueryParameter("style", "full")
                .appendQueryParameter("formatted", if (isFormatted) "true" else "false").build()
        return uri.toString()
    }

    @Throws(IOException::class, JSONException::class)
    private fun parsePointsOfInterest(points: MutableList<PointOfInterest>, json: JSONObject) {
        val jsonObject = objectData
        val geonames = jsonObject.getJSONArray("geonames")

        for (i in 0 until geonames.length()) {
            val obj = geonames.getJSONObject(i)
            val poi = PointOfInterest(obj)
            points.add(poi)
        }
    }
}
