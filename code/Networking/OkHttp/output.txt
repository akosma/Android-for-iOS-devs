I/MainActivity: Fetched data: {"geonames": [
                  {
                    "summary": "The Glärnisch is a mountain of the North-Eastern Swiss Alps, overlooking the valley of the Linth in the Swiss canton of Glarus. It consists of several summits of which the highest is 2,915 metres above sea level (...)",
                    "elevation": 2880,
                    "geoNameId": 2660595,
                    "feature": "mountain",
                    "lng": 8.998611,
                    "distance": "0.1869",
                    "countryCode": "CH",
                    "rank": 91,
                    "lang": "en",
                    "title": "Glärnisch",
                    "lat": 46.998611,
                    "wikipediaUrl": "en.wikipedia.org/wiki/Gl%C3%A4rnisch"
                  },
                  {
                    "summary": "Oberblegisee is a lake in the Canton of Glarus, Switzerland. It is located at an elevation of , above the village of Luchsingen and below the peaks of Glärnisch. Its surface area is .  (...)",
                    "elevation": 1410,
                    "feature": "waterbody",
                    "lng": 9.013333,
                    "distance": "2.3589",
                    "countryCode": "CH",
                    "rank": 70,
                    "lang": "en",
                    "title": "Oberblegisee",
                    "lat": 46.980833,
                    "wikipediaUrl": "en.wikipedia.org/wiki/Oberblegisee"
                  },
                  {
                    "summary": "The Rüchigrat (2,657 m) is a mountain of the Glarus Alps, located west of Luchsingen in the canton of Glarus. It lies south of the Glärnisch, on the range between the valley of the Klöntalersee and the main Linth valley (...)",
                    "elevation": 2625,
                    "geoNameId": 6938281,
                    "feature": "mountain",
                    "lng": 8.97765,
                    "distance": "2.8585",
                    "countryCode": "CH",
                    "rank": 40,
                    "lang": "en",
                    "title": "Rüchigrat",
                    "lat": 46.9793,
                    "wikipediaUrl": "en.wikipedia.org/wiki/R%C3%BCchigrat"
                  },
                  {
                    "summary": "Klöntalersee is a natural lake in the Canton of Glarus, Switzerland. Since 1908, it has been used as a reservoir for electricity production. The dam's construction substantially increased the lake's volume. Klöntalersee is drained by the Löntsch, a left tributary of the Linth.  (...)",
                    "elevation": 835,
                    "feature": "waterbody",
                    "lng": 8.980555555555556,
                    "distance": "3.2011",
                    "countryCode": "CH",
                    "rank": 88,
                    "thumbnailImg": "http://www.geonames.org/img/wikipedia/2000/thumb-1904-100.jpg",
                    "lang": "en",
                    "title": "Klöntalersee",
                    "lat": 47.025555555555556,
                    "wikipediaUrl": "en.wikipedia.org/wiki/Kl%C3%B6ntalersee"
                  },
                  {
                    "summary": "The Vorder Glärnisch (2,327 m) is a mountain of the Glarus Alps, overlooking the valley of the Linth in the canton of Glarus. It lies east of the higher Glärnisch (2,914 m). Unlike its higher neighbour, the Vorder Glärnisch can be ascended via a trail on its north-west side.  (...)",
                    "elevation": 2219,
                    "geoNameId": 6938509,
                    "lng": 9.0411,
                    "distance": "3.9629",
                    "countryCode": "CH",
                    "rank": 64,
                    "lang": "en",
                    "title": "Vorder Glärnisch",
                    "lat": 47.02202,
                    "wikipediaUrl": "en.wikipedia.org/wiki/Vorder_Gl%C3%A4rnisch"
                  }
                ]}

