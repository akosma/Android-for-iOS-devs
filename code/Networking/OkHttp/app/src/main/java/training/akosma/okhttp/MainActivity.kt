package training.akosma.okhttp

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // This creates an instance of the background task
        // and launches it. The main loop stays unfazed.
        ConnectToAPITask().execute()
    }

    private inner class ConnectToAPITask : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg voids: Void): String? {
            try {
                // If you receive an error saying that this user has exceeded its limit
                // of that it is not registered, you can register one for free
                // in geonames.org
                val url = API_URL.replace("USERNAME", "steve")
                val data = APIConnector().getStringData(url)
                Log.i("MainActivity", "Fetched data: " + data)
                return data
            } catch (e: IOException) {
                Log.e("MainActivity", "Failed to fetch URL: ", e)
            }

            return null
        }

        override fun onPostExecute(s: String) {
            textView.text = s
        }
    }

    companion object {
        private val API_URL = "http://api.geonames.org/findNearbyWikipediaJSON?formatted=true&lat=47&lng=9&username=USERNAME&style=full"
    }
}
