package training.akosma.restclient

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_main.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::usage[]
        val client = ServiceGenerator.createService(UsersClient::class.java)

        val newUser = User()
        newUser.age = 10
        newUser.country = "Latveria"
        newUser.name = "Doctor Doom"

        val getUsersCall = client.users
        getUsersCall.enqueue(object : Callback<List<User>> {
            override fun onResponse(call: Call<List<User>>,
                                    response: Response<List<User>>) {
                if (response.isSuccessful) {
                    val users = response.body()
                    if (users != null) {
                        Log.i("MainActivity", "User: " + users.toString())
                        updateList(users)
                    }
                } else {
                    Log.e("MainActivity", "Error calling the API")
                }
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                Log.d("Error", t.message)
            }
        })
        // end::usage[]

        val postUserCall = client.createUser(newUser)
        postUserCall.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful) {
                    Log.i("MainActivity", "User created")
                } else {
                    Log.e("MainActivity", "Error calling the API")
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                Log.d("Error", t.message)
            }
        })

        val getUserCall = client.getUser(2)
        getUserCall.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful) {
                    Log.i("MainActivity", "User: " + response.body()!!)
                } else {
                    Log.e("MainActivity", "Error calling the API")
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                Log.d("Error", t.message)
            }
        })

        val updateUserCall = client.updateUser(1, newUser)
        updateUserCall.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful) {
                    Log.i("MainActivity", "Update succcessful!")
                } else {
                    Log.e("MainActivity", "Error calling the API")
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                Log.d("Error", t.message)
            }
        })

        val deleteUserCall = client.deleteUser(6)
        deleteUserCall.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful) {
                    Log.i("MainActivity", "Delete succcessful!")
                } else {
                    Log.e("MainActivity", "Error calling the API")
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                Log.d("Error", t.message)
            }
        })
    }

    private fun updateList(users: List<User>?) {
        Log.i("MainActivity", "Success, passing points to the main thread: " + users!!)
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, users)
        listView.adapter = adapter
    }
}
