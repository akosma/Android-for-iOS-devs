package training.akosma.restclient

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// tag::generator[]
internal object ServiceGenerator {

    // Do not use "localhost" or "127.0.0.1" here!
    // Those point to the emulator, not to the local machine!
    private val API_BASE_URL = "https://da9621ea.eu.ngrok.io/"

    private val httpClient = OkHttpClient.Builder()

    private val builder = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())

    fun <S> createService(serviceClass: Class<S>): S {
        val retrofit = builder.client(httpClient.build()).build()
        return retrofit.create(serviceClass)
    }
}
// end::generator[]
