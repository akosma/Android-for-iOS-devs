#!/usr/bin/env xcrun swift

import Cocoa

class ApplicationDelegate: NSObject, NSApplicationDelegate {
    var service: NetService? = nil

    internal var window: NSWindow

    init(window: NSWindow) {
        self.window = window
    }

    func applicationDidFinishLaunching(_ notification: Notification) {
        // tag::server[]
        let domain = "local."
        let type = "_zeroconfserv._tcp."
        let name = "ZeroconfServer"
        let port : CInt = 9876
        service = NetService(domain: domain, type: type, name: name, port: port)
        service?.publish()
        // end::server[]

        let label = NSTextField(frame: self.window.contentView!.frame)
        label.alignment = .center
        label.stringValue = "\(name):\(port)"
        label.font = NSFont(name: "Helvetica", size: 25)

        self.window.contentView?.addSubview(label)
    }
}

class WindowDelegate: NSObject, NSWindowDelegate {
    func windowWillClose(_ notification: Notification) {
        NSApplication.shared.terminate(0)
    }
}

let windowDelegate = WindowDelegate()
let window = NSWindow(contentRect: NSMakeRect(0, 0, 480, 320),
                      styleMask: [.titled, .closable, .miniaturizable],
                      backing: .buffered,
                      defer: false)

window.center()
window.title = "Zeroconf Service"
window.delegate = windowDelegate
window.makeKeyAndOrderFront(window)

let applicationDelegate = ApplicationDelegate(window: window)

let application = NSApplication.shared
application.setActivationPolicy(.regular)
application.delegate = applicationDelegate
application.activate(ignoringOtherApps: true)
application.run()

