package training.akosma.zeroconf

internal object Constants {
    val BROADCAST_NAME = "SERVER_READY"
    val SERVICE_TYPE = "_zeroconfserv._tcp."
    val DEFAULT_SERVICE_NAME = "ZeroconfServer"
}
