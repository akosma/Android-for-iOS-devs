package training.akosma.zeroconf

import android.content.Context
import android.content.Intent
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.support.v4.content.LocalBroadcastManager
import android.util.Log

import java.io.IOException
import java.net.ServerSocket

internal class ZeroconfServer(private val context: Context) {
    private val nsdManager: NsdManager
    private var registrationListener: NsdManager.RegistrationListener? = null
    private var backingServiceName: String? = null
    private var port: Int = 0

    val serviceName: String
        get() = String.format("%s:%s", backingServiceName, port)

    init {
        try {
            // This gives us an available port in the local machine
            val mServerSocket = ServerSocket(0)
            port = mServerSocket.localPort
        } catch (e: IOException) {
            Log.e(TAG, "Could not create temporary socket")
        }

        nsdManager = context.getSystemService(Context.NSD_SERVICE) as NsdManager
    }

    fun stop() {
        if (registrationListener != null) {
            nsdManager.unregisterService(registrationListener)
            registrationListener = null
        }
    }

    fun start() {
        initializeRegistrationListener()

        // tag::server[]
        val serviceInfo = NsdServiceInfo()
        serviceInfo.serviceName = Constants.DEFAULT_SERVICE_NAME
        serviceInfo.serviceType = Constants.SERVICE_TYPE
        serviceInfo.port = port
        nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationListener)
        // end::server[]
    }

    private fun initializeRegistrationListener() {
        registrationListener = object : NsdManager.RegistrationListener {

            // tag::registration[]
            override fun onServiceRegistered(serviceInfo: NsdServiceInfo) {
                Log.i(TAG, "Service registered")
                backingServiceName = serviceInfo.serviceName
                val intent = Intent(Constants.BROADCAST_NAME)
                val manager = LocalBroadcastManager.getInstance(context)
                manager.sendBroadcast(intent)
            }
            // end::registration[]

            override fun onRegistrationFailed(arg0: NsdServiceInfo, arg1: Int) {
                Log.w(TAG, "Service registration failed")
            }

            override fun onServiceUnregistered(arg0: NsdServiceInfo) {
                Log.i(TAG, "Service unregistered")
            }

            override fun onUnregistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
                Log.w(TAG, "Service unregistration failed")
            }
        }
    }

    companion object {
        private val TAG = "ZeroconfServer"
    }
}
