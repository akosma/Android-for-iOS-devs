package training.akosma.zeroconf

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var zeroconfClient: ZeroconfClient? = null
    private var zeroconfServer: ZeroconfServer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupUI()
    }

    override fun onResume() {
        super.onResume()
        if (zeroconfClient != null) {
            textView.setText(R.string.discovery_in_progress)
            zeroconfClient!!.start()
        }
    }

    override fun onPause() {
        super.onPause()
        stop()
    }

    private fun setupUI() {
        nativeClientButton.setOnClickListener {
            zeroconfClient = NativeZeroconfClient(this)
            setupZeroconfClient()
            setButtonsActive(false)
        }

        jmdnsClientButton.setOnClickListener {
            zeroconfClient = JmDNSZeroconfClient(this)
            setupZeroconfClient()
            setButtonsActive(false)
        }

        serverButton.setOnClickListener {
            setupZeroconfServer()
            setButtonsActive(false)
        }

        stopButton.setOnClickListener { stop() }
    }

    private fun stop() {
        if (zeroconfClient != null) {
            textView.setText(R.string.discovery_stopped)
            zeroconfClient!!.stop()
            zeroconfClient = null
        }
        if (zeroconfServer != null) {
            zeroconfServer!!.stop()
            zeroconfServer = null
        }
        setButtonsActive(true)
        textView.setText(R.string.ready)
    }

    private fun setButtonsActive(enabled: Boolean) {
        nativeClientButton.isEnabled = enabled
        jmdnsClientButton.isEnabled = enabled
        serverButton.isEnabled = enabled
        stopButton.isEnabled = !enabled
    }

    private fun setupZeroconfClient() {
        zeroconfClient!!.setListener(object : ZeroconfClientListener {
            override fun onServiceResolutionSucceeded() {
                runOnUiThread {
                    Toast.makeText(this@MainActivity, R.string.service_resolution_success, Toast.LENGTH_LONG).show()
                    val service = zeroconfClient!!.service
                    textView.text = service
                }
            }

            override fun onServiceResolutionFailed() {
                runOnUiThread {
                    Toast.makeText(this@MainActivity, R.string.service_resolution_failed, Toast.LENGTH_LONG).show()
                    textView.setText(R.string.service_resolution_failed)
                }
            }
        })
        textView.setText(R.string.discovery_in_progress)
        zeroconfClient!!.start()
    }

    private fun setupZeroconfServer() {
        val manager = LocalBroadcastManager.getInstance(this)
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                textView.text = zeroconfServer!!.serviceName
            }
        }
        manager.registerReceiver(receiver, IntentFilter(Constants.BROADCAST_NAME))

        zeroconfServer = ZeroconfServer(this)
        zeroconfServer!!.start()
    }
}