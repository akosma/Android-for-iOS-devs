package training.akosma.zeroconf

import android.content.Context
import android.net.wifi.WifiManager
import android.util.Log
import java.io.IOException
import java.net.InetAddress
import java.net.UnknownHostException
import java.util.*
import javax.jmdns.JmDNS
import javax.jmdns.ServiceEvent
import javax.jmdns.ServiceListener

internal class JmDNSZeroconfClient(private val context: Context) : ZeroconfClient, ServiceListener {
    private var lock: WifiManager.MulticastLock? = null
    private var jmdns: JmDNS? = null
    private var listener: ZeroconfClientListener? = null
    override var service: String? = null
        private set

    override fun setListener(listener: ZeroconfClientListener) {
        this.listener = listener
    }

    override fun start() {
        try {
            // tag::start[]
            val wifi = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val deviceIpAddress = getDeviceIpAddress(wifi)
            lock = wifi.createMulticastLock(javaClass.name)
            lock?.setReferenceCounted(true)
            lock?.acquire()
            val hostname = "localhost"
            jmdns = JmDNS.create(deviceIpAddress, hostname)
            jmdns?.addServiceListener(Constants.SERVICE_TYPE + "local.", this)
            // end::start[]
        } catch (ex: IOException) {
            Log.e(TAG, ex.message, ex)
        }

    }

    override fun stop() {
        try {
            jmdns?.unregisterAllServices()
            jmdns?.close()
            jmdns = null
            lock?.release()
            lock = null
        } catch (ex: Exception) {
            Log.e(TAG, ex.message, ex)
        }

    }

    // tag::found[]
    override fun serviceAdded(serviceEvent: ServiceEvent) {
        Log.i(TAG, "Service added: " + serviceEvent)
        jmdns?.requestServiceInfo(serviceEvent.type,
                serviceEvent.name, 1)
    }
    // end::found[]

    override fun serviceRemoved(serviceEvent: ServiceEvent) {
        Log.i(TAG, "Service removed: " + serviceEvent)
    }

    // tag::resolved[]
    override fun serviceResolved(serviceEvent: ServiceEvent) {
        Log.i(TAG, "Service resolved: " + serviceEvent)
        val host = serviceEvent.info.hostAddresses[0]
        val port = serviceEvent.info.port
        service = String.format(Locale.ENGLISH, "%s:%d", host, port)
        if (listener != null) {
            listener?.onServiceResolutionSucceeded()
        }
    }
    // end::resolved[]

    // Courtesy of
    // https://stackoverflow.com/a/23854825/133764
    private fun getDeviceIpAddress(wifi: WifiManager?): InetAddress? {
        var result: InetAddress? = null
        try {
            result = InetAddress.getByName("10.0.0.2")
            val wifiinfo = wifi?.connectionInfo
            if (wifiinfo != null) {
                val intaddr = wifiinfo.ipAddress
                val byteaddr = byteArrayOf((intaddr and 0xff).toByte(), (intaddr shr 8 and 0xff).toByte(), (intaddr shr 16 and 0xff).toByte(), (intaddr shr 24 and 0xff).toByte())
                result = InetAddress.getByAddress(byteaddr)
            }
        } catch (ex: UnknownHostException) {
            Log.w(TAG, String.format("getDeviceIpAddress Error: %s", ex.message))
        }

        return result
    }

    companion object {
        private val TAG = "JmDNSZeroconfClient"
    }
}
