package training.akosma.zeroconf

interface ZeroconfClient {
    val service: String?
    fun start()
    fun stop()
    fun setListener(listener: ZeroconfClientListener)
}
