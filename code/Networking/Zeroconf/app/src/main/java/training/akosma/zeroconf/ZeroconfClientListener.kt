package training.akosma.zeroconf

interface ZeroconfClientListener {
    fun onServiceResolutionSucceeded()
    fun onServiceResolutionFailed()
}
