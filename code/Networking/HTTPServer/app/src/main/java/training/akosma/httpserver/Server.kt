package training.akosma.httpserver

import fi.iki.elonen.NanoHTTPD

// tag::server[]
class Server(port: Int = 8080) : NanoHTTPD(port) {
    override fun serve(session: IHTTPSession?): Response {
        var msg = header(session)

        val parameters = session?.parameters
        if (parameters != null) {
            val users = parameters["username"]
            msg += if (users != null && users.count() > 0) {
                greet(users[0])
            } else {
                form()
            }
        }
        return newFixedLengthResponse(msg + footer());
    }

    private fun footer() = "</body></html>\n"

    private fun header(session: IHTTPSession?): String {
        var msg = """
                <html>
                <head>
                <title>Android Server</title>
                </head>

                <body><h1>Android Server</h1>
                <p>Called with `${session?.uri}`</p>
                """.trimIndent()  // <1>
        return msg
    }
// end::server[]

    private fun greet(user: String?) = "<p>Hello, $user!</p>"

    private fun form(): String {
        return """
                <form method="GET">
                <p>Name: <input type="text" name="username"></p>
                <p><input type="submit"></p>
                </form>
                """.trimIndent()
    }
}