package training.akosma.httpserver

import android.content.Context
import android.net.wifi.WifiManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.format.Formatter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::usage[]
        val port = 8080
        val ip = getCurrentIPAddress()
        textField.text = "http://$ip:$port"

        val server = Server(port)
        server.start()
        // end::usage[]
    }

    @Suppress("DEPRECATION")
    fun getCurrentIPAddress(): String {
        val wm = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return Formatter.formatIpAddress(wm.connectionInfo.ipAddress)
    }
}
