package training.akosma.xmlparsing

enum class APIConnectorFormat private constructor(val value: String) {
    JSON("JSON"),
    XML("")
}
