package training.akosma.xmlparsing

import android.net.Uri
import android.util.Log
import org.json.JSONException
import org.json.JSONObject
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class APIConnector {
    var baseURL = "http://api.geonames.org/findNearbyWikipedia"
    var isFormatted = true
    var format = APIConnectorFormat.JSON

    // If you receive an error saying that this user has exceeded its limit
    // of that it is not registered, you can register one for free
    // in geonames.org
    var username = "steve"

    private// default value
    val connection: HttpURLConnection
        @Throws(IOException::class)
        get() {
            val urlString = buildFullUrl()
            Log.i("APIConnector", "Connecting to URL: " + urlString)
            val url = URL(urlString)
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            conn.connectTimeout = 30000
            return conn
        }

    val stringData: String
        @Throws(IOException::class)
        get() {
            val conn = connection

            try {
                val inputStream = conn.inputStream
                if (conn.responseCode != HttpURLConnection.HTTP_OK) {
                    throw IOException(conn.responseMessage)
                }
                return inputStream.bufferedReader().readText()
            } finally {
                conn.disconnect()
            }
        }

    private val objectData: JSONObject
        @Throws(IOException::class, JSONException::class)
        get() = JSONObject(stringData)

    internal val pointsOfInterest: List<PointOfInterest>
        @Throws(IOException::class, JSONException::class, XmlPullParserException::class)
        get() {
            var points: List<PointOfInterest> = ArrayList()
            if (format == APIConnectorFormat.JSON) {
                val json = objectData
                points = parsePointsOfInterest(json)
            } else if (format == APIConnectorFormat.XML) {
                val conn = connection
                val inputStream = conn.inputStream
                try {
                    val parser = XmlPOIParser()
                    points = parser.parse(inputStream)
                } finally {
                    inputStream.close()
                    conn.disconnect()
                }
            }
            return points
        }

    private fun buildFullUrl(): String {
        val url = baseURL + format.value
        val lat = 47.0
        val lng = 9.0
        val uri = Uri.parse(url).buildUpon()
                .appendQueryParameter("lat", lat.toString())
                .appendQueryParameter("lng", lng.toString())
                .appendQueryParameter("username", username)
                .appendQueryParameter("style", "full")
                .appendQueryParameter("formatted", if (isFormatted) "true" else "false").build()
        return uri.toString()
    }

    @Throws(IOException::class, JSONException::class)
    private fun parsePointsOfInterest(json: JSONObject): List<PointOfInterest> {
        val jsonObject = objectData
        val geonames = jsonObject.getJSONArray("geonames")
        val points: MutableList<PointOfInterest> = ArrayList()

        // Unfortunately this is not possible…
        // for (JSONObject o : geonames) { ... }

        for (i in 0 until geonames.length()) {
            val obj = geonames.getJSONObject(i)
            val poi = PointOfInterest(obj)
            points.add(poi)
        }
        return points
    }
}
