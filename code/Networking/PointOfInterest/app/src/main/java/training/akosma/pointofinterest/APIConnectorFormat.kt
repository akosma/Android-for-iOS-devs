package training.akosma.pointofinterest

enum class APIConnectorFormat private constructor(val value: String) {
    JSON("JSON"),
    XML("")
}
