package training.akosma.pointofinterest

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_list.*

import org.json.JSONException
import org.xmlpull.v1.XmlPullParserException

import java.io.IOException

class PointOfInterestListFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listView.layoutManager = LinearLayoutManager(activity)
        ConnectToAPITask().execute()
    }

    private fun updateUI(points: List<PointOfInterest>) {
        val adapter = PointOfInterestAdapter(points)
        listView.adapter = adapter
    }

    private inner class ConnectToAPITask : AsyncTask<Void, Void, List<PointOfInterest>>() {
        override fun doInBackground(vararg voids: Void): List<PointOfInterest>? {
            try {
                val connector = APIConnector()
                connector.format = APIConnectorFormat.XML
                return connector.pointsOfInterest
            } catch (e: IOException) {
                Log.e("Fragment", "Failed to retrieve data", e)
                cancel(true)
            } catch (e: JSONException) {
                Log.e("Fragment", "Failed to parse JSON", e)
                cancel(true)
            } catch (e: XmlPullParserException) {
                Log.e("Fragment", "Failed to parse XML", e)
            }

            return null
        }

        override fun onPostExecute(points: List<PointOfInterest>) {
            Log.i("Fragment", "Success, passing points to the main thread: " + points)
            updateUI(points)
        }

        override fun onCancelled() {
            Log.i("MainActivity", "BOOM")
        }
    }

    // tag::holder[]
    private inner class PointOfInterestHolder internal constructor(v: View)
        : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var poi: PointOfInterest? = null
        private val titleTextView: TextView
        private val latitudeTextView: TextView
        private val longitudeTextView: TextView
        private val summaryTextView: TextView

        init {
            v.setOnClickListener(this)
            titleTextView = v.findViewById(R.id.name_text_view)
            latitudeTextView = v.findViewById(R.id.latitude_text_view)
            longitudeTextView = v.findViewById(R.id.longitude_text_view)
            summaryTextView = v.findViewById(R.id.summary_text_view)
        }

        internal fun bind(poi: PointOfInterest) {
            this.poi = poi
            titleTextView.text = poi.title
            latitudeTextView.text = poi.lat.toString()
            longitudeTextView.text = poi.lng.toString()
            summaryTextView.text = poi.summary
        }

        override fun onClick(view: View) {
            Toast.makeText(activity, poi?.summary, Toast.LENGTH_LONG).show()
        }
    }
    // end::holder[]

    // tag::adapter[]
    private inner class PointOfInterestAdapter internal constructor(private val data: List<PointOfInterest>)
        : RecyclerView.Adapter<PointOfInterestHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PointOfInterestHolder {
            val inflater = LayoutInflater.from(activity)
            val view = inflater.inflate(R.layout.list_item_poi, parent, false)
            return PointOfInterestHolder(view)
        }

        override fun onBindViewHolder(holder: PointOfInterestHolder, position: Int) {
            val poi = data[position]
            holder.bind(poi)
        }

        override fun getItemCount(): Int {
            return data.size
        }
    }
    // end::adapter[]
}
