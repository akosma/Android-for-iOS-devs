package training.akosma.gson

import android.net.Uri
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class APIConnector {
    var baseURL = "http://api.geonames.org/findNearbyWikipedia"
    var isFormatted = true
    var format = APIConnectorFormat.JSON

    // If you receive an error saying that this user has exceeded its limit
    // of that it is not registered, you can register one for free
    // in geonames.org
    var username = "steve"

    // default value
    val data: String
        @Throws(IOException::class)
        get() {
            val urlString = buildFullUrl()
            Log.i("APIConnector", "Connecting to URL: " + urlString)
            val url = URL(urlString)
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            conn.connectTimeout = 30000

            try {
                val inputStream = conn.inputStream
                if (conn.responseCode != HttpURLConnection.HTTP_OK) {
                    throw IOException(conn.responseMessage + " (" + urlString + ")")
                }
                return inputStream.bufferedReader().readText()
            } finally {
                conn.disconnect()
            }
        }

    // tag::decode[]
    internal val pointsOfInterest: List<PointOfInterest>?
        @Throws(IOException::class)
        get() {
            val parser = JsonParser()
            val root = parser.parse(data).asJsonObject
            val geonames = root.get("geonames").asJsonArray
            val collectionType = object : TypeToken<List<PointOfInterest>>() {}.type
            val gson = Gson()
            return gson.fromJson<List<PointOfInterest>>(geonames, collectionType)
        }
    // end::decode[]

    private fun buildFullUrl(): String {
        val url = baseURL + format.value
        val lat = 47.0
        val lng = 9.0
        val uri = Uri.parse(url).buildUpon()
                .appendQueryParameter("lat", lat.toString())
                .appendQueryParameter("lng", lng.toString())
                .appendQueryParameter("username", username)
                .appendQueryParameter("style", "full")
                .appendQueryParameter("formatted", if (isFormatted) "true" else "false").build()
        return uri.toString()
    }
}
