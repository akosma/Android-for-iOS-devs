package training.akosma.gson

enum class APIConnectorFormat private constructor(val value: String) {
    JSON("JSON"),
    XML("")
}
