/*
 * Launch the API using the command
 * node app.js
 *
 * Use the following commands to interact with this API:
 *
 * curl http://localhost:3000/users --include
 * curl http://localhost:3000/users --header "Content-Type: application/json" --include --data "@request.json" --request POST
 * curl http://localhost:3000/user/3 --header "Content-Type: application/json" --include --data "@request.json" --request PUT
 * curl http://localhost:3000/user/1 --include --request DELETE
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());

var DB = function () {
    var users = {
        1: { "name": "John Smith", "age": 46, "country": "United States" },
        2: { "name": "Paul Stevens", "age": 27, "country": "Zambia" },
        3: { "name": "Maria Johnson", "age": 12, "country": "Australia" },
        4: { "name": "Paula Mill", "age": 92, "country": "Myanmar" },
        5: { "name": "Roseanne Miller", "age": 35, "country": "Switzerland" },
        6: { "name": "Rogelio Suarez", "age": 60, "country": "Argentina" },
    };

    var nextId = Object.keys(users).length + 1;

    return {
        findAll: function() {
            var results = [];
            for (var i in users) {
                var user = users[i];
                user.id = i;
                results.push(user);
            }
            return results;
        },

        find: function (id) {
            return users[id];
        },

        create: function (obj) {
            nextId += 1;
            users[nextId] = obj;
            return obj;
        },

        update: function (id, obj) {
            var o = users[id];
            if (o === null) {
                return null;
            }
            else {
                users[id] = obj;
                return obj;
            }
        },

        delete: function (id) {
            var o = users[id];
            if (o === null) {
                return null;
            }
            else {
                delete users[id];
                return o;
            }
        }
    };

}();

app.get('/users', function (req, res) {
    console.log("GET /users");
    res.status(200).send(DB.findAll());
});

app.get('/user/:id', function (req, res) {
    var id = parseInt(req.params.id);
    console.log("GET /user/%s", id);
    var user = DB.find(id);
    if (user === undefined) {
        res.status(404).send("Not found");
    }
    else {
        res.status(200).send(user);
    }
});

app.post('/users', function (req, res) {
    console.log("POST /users");
    var obj = DB.create(req.body);
    res.status(201).send(obj);
});

app.put('/user/:id', function (req, res) {
    var id = parseInt(req.params.id);
    console.log("PUT /user/%s", id);
    var user = DB.update(id, req.body);
    if (user === undefined) {
        res.status(404).send("Not found");
    }
    else {
        res.status(204).send(user);
    }
});

app.delete('/user/:id', function (req, res) {
    var id = parseInt(req.params.id);
    console.log("DELETE /user/%s", id);
    var user = DB.delete(id);
    if (user === undefined) {
        res.status(404).send("Not found");
    }
    else {
        res.status(204).send(user);
    }
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

