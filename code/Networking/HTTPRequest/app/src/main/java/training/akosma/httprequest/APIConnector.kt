package training.akosma.httprequest

import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

// tag::apiconnector[]
internal class APIConnector {
    @Throws(IOException::class)
    fun getStringData(urlString: String): String {
        val url = URL(urlString)
        val conn = url.openConnection() as HttpURLConnection
        conn.useCaches = true
        conn.requestMethod = "GET" // default value
        conn.connectTimeout = 30000

        try {
            val inputStream = conn.inputStream
            if (conn.responseCode != HttpURLConnection.HTTP_OK) {
                throw IOException(conn.responseMessage + " (" + urlString + ")")
            }
            return inputStream.bufferedReader().readText()
        } finally {
            conn.disconnect()
        }
    }
}
// end::apiconnector[]
