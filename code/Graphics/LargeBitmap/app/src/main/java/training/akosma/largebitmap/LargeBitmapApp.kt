package training.akosma.largebitmap

import android.app.Application

// tag::app[]
class LargeBitmapApp : Application() {
    override fun onLowMemory() {
        super.onLowMemory()
        System.runFinalization()
        Runtime.getRuntime().gc()
        System.gc()
    }
}
// end::app[]
