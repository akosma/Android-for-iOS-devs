package training.akosma.largebitmap

import android.app.ActivityManager
import android.content.ComponentCallbacks2
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

// Adapted from
// https://developer.android.com/training/displaying-bitmaps/load-bitmap.html
class MainActivity : AppCompatActivity() {

    // Image credit:
    // https://commons.wikimedia.org/wiki/File:Panorama_de_La_Garde_depuis_la_colline_de_l'Universit%C3%A9.jpg
    private var resourceId: Int = R.drawable.large_image

    private val availableMemory: ActivityManager.MemoryInfo
        get() {
            val activityManager = this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val memoryInfo = ActivityManager.MemoryInfo()
            activityManager.getMemoryInfo(memoryInfo)
            return memoryInfo
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        memoryButton.setOnClickListener {
            // Borrowed from
            // https://developer.android.com/topic/performance/memory.html
            // https://developer.android.com/guide/topics/ui/dialogs.html
            // tag::lowmemory[]
            val memoryInfo = availableMemory
            val mem = humanReadableByteCount(memoryInfo.availMem, false)
            var message = String.format("Available memory: %s", mem)

            if (memoryInfo.lowMemory) {
                message += "\nLow memory!"
            }

            val builder = AlertDialog.Builder(this)
            builder.setTitle("Memory status").setMessage(message)
            val dialog = builder.create()
            dialog.show()
            // end::lowmemory[]
        }

        crashButton.setOnClickListener {
            val drawable = resources.getDrawable(resourceId, this.applicationContext.theme)
            val bitmap = (drawable as BitmapDrawable).bitmap
            imageView.setImageBitmap(bitmap)
        }

        noCrashButton.setOnClickListener {
            toastDetails()
            loadSmallerBitmap(400, 400)
        }
    }

    // tag::ontrim[]
    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)

        when (level) {
            // Called when the app went to the background
            ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN -> {  }

            // These happen at runtime, even if your app is
            // on the foreground. If CRITICAL, the operating
            // system will begin to kill processes.
            ComponentCallbacks2.TRIM_MEMORY_RUNNING_MODERATE,
            ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW,
            ComponentCallbacks2.TRIM_MEMORY_RUNNING_CRITICAL -> {  }

            // This is even worse; this process is in the
            // list to be terminated as soon as possible.
            // This might be your last chance to survive.
            ComponentCallbacks2.TRIM_MEMORY_BACKGROUND,
            ComponentCallbacks2.TRIM_MEMORY_MODERATE,
            ComponentCallbacks2.TRIM_MEMORY_COMPLETE -> {  }

            // This is a generic low-memory level message.
            // Do your job. Release memory. Now.
            else -> {  }
        }
    }
    // end::ontrim[]

    private fun loadSmallerBitmap(width: Int, height: Int) {
        val smaller = sampledBitmap(width, height)
        imageView.setImageBitmap(smaller)

        // (you should probably store the image in the local
        // storage for reuse later!)
    }

    private fun toastDetails() {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeResource(resources, resourceId, options)
        val width = options.outWidth
        val height = options.outHeight
        val type = options.outMimeType

        val message = String.format(Locale.ENGLISH, "%s / %d x %d", type, width, height)
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun sampledBitmap(width: Int, height: Int): Bitmap {
        val resource = resources
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeResource(resource, resourceId, options)
        options.inSampleSize = sampleSize(options, width, height)
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeResource(resource, resourceId, options)
    }

    private fun sampleSize(options: BitmapFactory.Options,
                           requiredWidth: Int,
                           requiredHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > requiredHeight || width > requiredWidth) {

            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize >= requiredHeight && halfWidth / inSampleSize >= requiredWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }

    companion object {

        // Courtesy of
        // http://stackoverflow.com/a/3758880/133764
        fun humanReadableByteCount(bytes: Long, si: Boolean): String {
            val unit = if (si) 1000 else 1024
            if (bytes < unit) return bytes.toString() + " B"
            val exp = (Math.log(bytes.toDouble()) / Math.log(unit.toDouble())).toInt()
            val pre = (if (si) "kMGTPE" else "KMGTPE")[exp - 1] + if (si) "" else "i"
            return String.format(Locale.ENGLISH, "%.1f %sB", bytes / Math.pow(unit.toDouble(), exp.toDouble()), pre)
        }
    }
}
