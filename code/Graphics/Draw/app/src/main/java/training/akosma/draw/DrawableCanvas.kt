package training.akosma.draw

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import java.util.*

// tag::class[]
class DrawableCanvas(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private var strokeColor = Color.BLACK
    private var strokeWidth = 3.0f
    private var currentLine: Line? = null
    private var lines: ArrayList<Line> = ArrayList()
// end::class[]

    fun clear() {
        lines = ArrayList()
        invalidate()
    }

    fun setStrokeWidth(strokeWidth: Float) {
        this.strokeWidth = strokeWidth
    }

    fun setStrokeColor(strokeColor: Int) {
        this.strokeColor = strokeColor
    }

    // tag::draw[]
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        for (line in lines) {
            val path = line.path
            val paint = line.paint
            canvas.drawPath(path, paint)
        }
    }
    // end::draw[]

    // tag::touch[]
    override fun onTouchEvent(event: MotionEvent): Boolean {
        val result = super.onTouchEvent(event)
        if (!result) {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    val newLine = Line(strokeColor, strokeWidth)
                    lines.add(newLine)
                    newLine.moveTo(event.x, event.y)
                    currentLine = newLine
                    invalidate()
                    return true
                }

                MotionEvent.ACTION_MOVE -> {
                    val x = event.x
                    val y = event.y
                    currentLine?.lineTo(x, y)
                    invalidate()
                    return true
                }

                MotionEvent.ACTION_UP -> {
                    currentLine = null
                    invalidate()
                    return true
                }
            }
        }
        return result
    }
    // end::touch[]

    // tag::save[]
    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable("superState", super.onSaveInstanceState())
        bundle.putParcelableArrayList("lines", lines)
        return bundle
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        if (state is Bundle) {
            lines = state.getParcelableArrayList("lines")
            super.onRestoreInstanceState(state.getParcelable("superState"))
        }
    }
    // end::save[]
}
