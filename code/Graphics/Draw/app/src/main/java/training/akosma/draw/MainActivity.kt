package training.akosma.draw

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        radioBlack.setOnClickListener {
            canvasView.setStrokeColor(Color.BLACK)
        }

        radioGreen.setOnClickListener {
            canvasView.setStrokeColor(Color.GREEN)
        }

        radioRed.setOnClickListener {
            canvasView.setStrokeColor(Color.RED)
        }

        radioBlue.setOnClickListener {
            canvasView.setStrokeColor(Color.BLUE)
        }

        sizeSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                canvasView.setStrokeWidth(i.toFloat())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) { }
            override fun onStopTrackingTouch(seekBar: SeekBar) { }
        })

        clearButton.setOnClickListener {
            canvasView.clear()
        }
    }
}
