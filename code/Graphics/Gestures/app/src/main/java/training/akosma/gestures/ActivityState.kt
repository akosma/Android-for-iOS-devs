package training.akosma.gestures

enum class ActivityState {
    NONE,
    DRAG,
    ZOOM
}
