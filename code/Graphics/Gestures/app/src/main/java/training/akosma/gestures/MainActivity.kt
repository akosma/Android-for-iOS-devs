package training.akosma.gestures

import android.graphics.Matrix
import android.graphics.PointF
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

import training.akosma.gestures.ActivityState.DRAG
import training.akosma.gestures.ActivityState.NONE
import training.akosma.gestures.ActivityState.ZOOM

// Adapted and extended from
// https://judepereira.com/blog/multi-touch-in-android-translate-scale-and-rotate/
class MainActivity : AppCompatActivity(), View.OnTouchListener {

    private val matrix = Matrix()
    private val savedMatrix = Matrix()
    private var state = NONE
    private val originalPoint = PointF()
    private val midPoint = PointF()
    private var oldDistance = 1f
    private var distance = 0f
    private var lastEvent: FloatArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        label.setOnTouchListener { view, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> originalPoint.set(motionEvent.x, motionEvent.y)

                MotionEvent.ACTION_MOVE -> {
                    val x = motionEvent.x
                    val y = motionEvent.y
                    val lp = view.layoutParams as FrameLayout.LayoutParams
                    val left = lp.leftMargin + (x - originalPoint.x)
                    val top = lp.topMargin + (y - originalPoint.y)
                    lp.leftMargin = left.toInt()
                    lp.topMargin = top.toInt()
                    view.layoutParams = lp
                }
            }
            true
        }

        // Turtle image by Wexor Tmg
        // https://unsplash.com/photos/L-2p8fapOA8
        imageView.setOnTouchListener(this)
    }

    // tag::touch[]
    override fun onTouch(v: View, motionEvent: MotionEvent): Boolean {
        val view = v as ImageView
        when (motionEvent.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                savedMatrix.set(matrix)
                originalPoint.set(motionEvent.x, motionEvent.y)
                state = DRAG
                lastEvent = null
            }

            MotionEvent.ACTION_POINTER_DOWN -> {
                oldDistance = spacing(motionEvent)
                if (oldDistance > 10f) {
                    savedMatrix.set(matrix)
                    midPoint(midPoint, motionEvent)
                    state = ZOOM
                }
                val event = FloatArray(4)
                event[0] = motionEvent.getX(0)
                event[1] = motionEvent.getX(1)
                event[2] = motionEvent.getY(0)
                event[3] = motionEvent.getY(1)
                lastEvent = event
                distance = rotation(motionEvent)
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_POINTER_UP -> {
                state = NONE
                lastEvent = null
            }

            MotionEvent.ACTION_MOVE -> if (state == DRAG) {
                matrix.set(savedMatrix)
                val dx = motionEvent.x - originalPoint.x
                val dy = motionEvent.y - originalPoint.y
                matrix.postTranslate(dx, dy)
            } else if (state == ZOOM) {
                val newDist = spacing(motionEvent)
                if (newDist > 10f) {
                    matrix.set(savedMatrix)
                    val scale = newDist / oldDistance
                    matrix.postScale(scale, scale, midPoint.x, midPoint.y)
                }
                if (lastEvent != null && motionEvent.pointerCount == 2) {
                    val newRot = rotation(motionEvent)
                    val r = newRot - distance
                    val values = FloatArray(9)
                    matrix.getValues(values)
                    val tx = values[2]
                    val ty = values[5]
                    val sx = values[0]
                    val xc = view.width / 2 * sx
                    val yc = view.height / 2 * sx
                    matrix.postRotate(r, tx + xc, ty + yc)
                }
            }
        }

        view.imageMatrix = matrix
        return true
    }
    // end::touch[]

    private fun spacing(event: MotionEvent): Float {
        val x = event.getX(0) - event.getX(1)
        val y = event.getY(0) - event.getY(1)
        return Math.sqrt((x * x + y * y).toDouble()).toFloat()
    }

    private fun midPoint(point: PointF, event: MotionEvent) {
        val x = event.getX(0) + event.getX(1)
        val y = event.getY(0) + event.getY(1)
        point.set(x / 2, y / 2)
    }

    private fun rotation(event: MotionEvent): Float {
        val delta_x = (event.getX(0) - event.getX(1)).toDouble()
        val delta_y = (event.getY(0) - event.getY(1)).toDouble()
        val radians = Math.atan2(delta_y, delta_x)
        return Math.toDegrees(radians).toFloat()
    }
}
