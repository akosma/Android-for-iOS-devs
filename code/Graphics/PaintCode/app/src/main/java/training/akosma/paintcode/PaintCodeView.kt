package training.akosma.paintcode

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View


class PaintCodeView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        StyleKit.drawCanvas2(canvas, context)
    }
}
