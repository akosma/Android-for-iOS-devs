package training.akosma.animations

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), Animator.AnimatorListener {

    private var menuItem: MenuItem? = null
    private var duration: Int = 0
    private var textViewVisible = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView.movementMethod = ScrollingMovementMethod()

        duration = resources.getInteger(
                android.R.integer.config_shortAnimTime)
    }

    // tag::app_menu[]
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.app_menu, menu)
        menuItem = menu.getItem(0)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_disappear -> {
                // end::app_menu[]

                // tag::animation[]
                var transparency = 1.0f
                if (textViewVisible) {
                    transparency = 0.0f
                }
                textViewVisible = !textViewVisible
                textView.animate()
                        .alpha(transparency)
                        .setDuration(duration.toLong())
                        .setListener(this)
                // end::animation[]
                return true
            }

            R.id.action_more -> {
                // tag::animator[]
                val rotate = ObjectAnimator.ofFloat(textView,
                        "rotation", 0f, 360f)
                val moveH = ObjectAnimator.ofFloat(textView,
                        "translationX", 0f, 100f)
                val moveV = ObjectAnimator.ofFloat(textView,
                        "translationY", 0f, 100f)
                val backH = ObjectAnimator.ofFloat(textView,
                        "translationX", 100f, 0f)
                val backV = ObjectAnimator.ofFloat(textView,
                        "translationY", 100f, 0f)
                val set = AnimatorSet()
                set.setDuration(duration.toLong())
                        .play(rotate)
                        .before(backH).before(backV)
                        .after(moveH).after(moveV)
                set.addListener(this)
                set.start()
                // end::animator[]
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onAnimationStart(animator: Animator) {}

    // tag::callback[]
    override fun onAnimationEnd(animator: Animator) {
        if (textViewVisible) {
            menuItem?.title = "Disappear"
        } else {
            menuItem?.title = "Appear"
        }
        Toast.makeText(this,
                "Animation finished!",
                Toast.LENGTH_SHORT)
                .show()
    }
    // end::callback[]

    override fun onAnimationCancel(animator: Animator) {}

    override fun onAnimationRepeat(animator: Animator) {}
}
