package training.akosma.pickimage

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Courtesy of
        // http://stackoverflow.com/a/33292700/133764

        // tag::morepermissions[]
        val permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            )
        }
        // end::morepermissions[]

        button.setOnClickListener {

            // Courtesy of
            // http://stackoverflow.com/a/5309217/133764

            // tag::intent[]
            val getIntent = Intent(Intent.ACTION_GET_CONTENT)
            getIntent.type = "image/*"

            val picker = Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            picker.type = "image/*"
            val intents = arrayOf(picker)

            val chooser = Intent.createChooser(getIntent, "Select Image")
            chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents)

            startActivityForResult(chooser, CODE)
            // end::intent[]
        }
    }

    // tag::result[]
    override fun onActivityResult(requestCode: Int,
                                  resultCode: Int,
                                  data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CODE && resultCode == Activity.RESULT_OK) {
            val uri = data.data
            view_picture.setImageURI(uri)
        }
    }

    companion object {
        private val CODE = 1
        private val REQUEST_EXTERNAL_STORAGE = 1
        private val PERMISSIONS_STORAGE = arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
    // end::result[]
}
