package training.akosma.music

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // tag::notification[]
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            buttonsPlaying(false)
        }
    }
    // end::notification[]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val manager = LocalBroadcastManager.getInstance(this)
        manager.registerReceiver(receiver, IntentFilter("STOPPED"))

        playButton.setOnClickListener {
            buttonsPlaying(true)

            // tag::start[]
            val intent = Intent(this, MusicService::class.java)
            intent.putExtra("PLAY", true)
            startService(intent)
            // end::start[]
        }

        stopButton.setOnClickListener {
            buttonsPlaying(true)

            val intent = Intent(this, MusicService::class.java)
            intent.putExtra("PLAY", false)
            startService(intent)
        }
    }

    private fun buttonsPlaying(playing: Boolean) {
        playButton.isEnabled = (!playing)
        stopButton.isEnabled = playing
    }
}
