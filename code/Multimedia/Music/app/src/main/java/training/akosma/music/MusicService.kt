package training.akosma.music

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager

// tag::service[]
class MusicService : Service() {
    private var player: MediaPlayer? = null

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val play = intent.getBooleanExtra("PLAY", true)
        if (play) {
            createPlayerIfNeeded()
            player?.start()
        } else {
            player?.pause()
        }

        return Service.START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        player?.stop()
    }

    private fun createPlayerIfNeeded() {
        if (player == null) {
            player = MediaPlayer.create(this, R.raw.song)
            player?.setOnCompletionListener {
                val notification = Intent("STOPPED")
                val manager = LocalBroadcastManager.getInstance(this)
                manager.sendBroadcast(notification)
                stopSelf()
            }
        }
    }
}
// end::service[]
