package training.akosma.recorder

enum class ApplicationState {
    IDLE,
    RECORDING,
    PLAYING,
    FORBIDDEN
}
