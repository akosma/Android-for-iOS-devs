package training.akosma.recorder

import android.Manifest
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.IOException

// Adapted from
// https://developer.android.com/guide/topics/media/audio-capture.html
// and
// https://developer.android.com/training/permissions/requesting.html
class MainActivity : AppCompatActivity() {

    private var fileName: String? = null
    private var player: MediaPlayer? = null
    private var recorder: MediaRecorder? = null
    private var state = ApplicationState.FORBIDDEN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fileName = Environment.getExternalStorageDirectory().absolutePath
        fileName += "/filename.mp3"

        button_rec.setOnClickListener { rec() }
        button_play.setOnClickListener { play() }
        button_stop.setOnClickListener { stop() }
    }

    // tag::haspermission[]
    private fun hasPermission(permission: String): Boolean {
        val check = ActivityCompat.checkSelfPermission(this, permission)
        return check == PackageManager.PERMISSION_GRANTED
    }
    // end::haspermission[]

    // tag::permissionsresult[]
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            setState(ApplicationState.IDLE)
        } else {
            setState(ApplicationState.FORBIDDEN)
            Toast.makeText(this,
                    "Unfortunately, cannot do much without permissions...",
                    Toast.LENGTH_LONG).show()
        }
    }
    // end::permissionsresult[]

    override fun onStart() {
        super.onStart()

        // tag::permissions[]
        val write = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val record = Manifest.permission.RECORD_AUDIO
        if (!hasPermission(write) || !hasPermission(record)) {
            val permissions = arrayOf(write, record)
            setState(ApplicationState.FORBIDDEN)
            ActivityCompat.requestPermissions(this, permissions, 0)
            return
        }
        setState(ApplicationState.IDLE)
        // end::permissions[]
    }

    private fun rec() {
        try {
            // tag::recorder[]
            recorder = MediaRecorder()
            recorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
            recorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            recorder?.setOutputFile(fileName)
            recorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            recorder?.prepare()

            setState(ApplicationState.RECORDING)
            recorder?.start()
            // end::recorder[]
        } catch (e: IOException) {
            recorder?.release()
            recorder = null
            Log.e("MainActivity", "Cannot record audio: ", e)
        }

    }

    private fun play() {
        val exists = fileExists()
        if (exists) {
            try {
                // tag::player[]
                player = MediaPlayer()
                player?.setDataSource(fileName)
                player?.setOnCompletionListener { stop() }
                player?.prepare()

                setState(ApplicationState.PLAYING)
                player?.start()
                // end::player[]
            } catch (e: IOException) {
                player?.release()
                player = null
                Log.e("MainActivity", "Cannot play audio: ", e)
            }

        }
    }

    // tag::stop[]
    private fun stop() {
        if (ApplicationState.RECORDING == state && recorder != null) {
            recorder?.stop()
            recorder?.release()
            recorder = null
        } else if (ApplicationState.PLAYING == state && player != null) {
            player?.stop()
            player?.release()
            player = null
        }
        setState(ApplicationState.IDLE)
    }
    // end::stop[]

    private fun setState(state: ApplicationState) {
        this.state = state
        when (this.state) {
            ApplicationState.RECORDING -> {
                button_rec.isEnabled = false
                button_play.isEnabled = false
                button_stop.isEnabled = true
            }

            ApplicationState.PLAYING -> {
                button_rec.isEnabled = false
                button_play.isEnabled = false
                button_stop.isEnabled = true
            }

            ApplicationState.IDLE -> {
                val exists = fileExists()
                button_rec.isEnabled = true
                button_play.isEnabled = exists
                button_stop.isEnabled = false
            }

            ApplicationState.FORBIDDEN -> {
                button_rec.isEnabled = false
                button_play.isEnabled = false
                button_stop.isEnabled = false
            }
        }
    }

    private fun fileExists(): Boolean {
        val file = File(fileName)
        return file.exists()
    }
}
