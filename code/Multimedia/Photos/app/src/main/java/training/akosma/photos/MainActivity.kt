package training.akosma.photos

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            // tag::intent[]
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, CODE)
            }
            // end::intent[]
        }
    }

    // tag::result[]
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CODE && resultCode == Activity.RESULT_OK) {
            val extras = data.extras
            if (extras != null) {
                val imageBitmap = extras.get(KEY) as Bitmap
                view_picture.setImageBitmap(imageBitmap)
            }
        }
    }

    companion object {
        private val CODE = 1
        private val KEY = "data"
    }
    // end::result[]
}
