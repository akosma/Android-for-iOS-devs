package training.akosma.videos

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.widget.MediaController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::controller[]
        val controller = MediaController(this)
        videoView.setMediaController(controller)
        // end::controller[]

        button.setOnClickListener {
            // tag::intent[]
            val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            if (intent.resolveActivity(packageManager) != null) {
                startActivityForResult(intent, CODE)
            }
            // end::intent[]
        }
    }

    // tag::result[]
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (requestCode == CODE && resultCode == Activity.RESULT_OK) {
            val videoUri = intent.data
            videoView.setVideoURI(videoUri)
            videoView.start()
        }
    }

    companion object {
        private val CODE = 1
    }
    // end::result[]
}
