package training.akosma.runnable

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message

import java.util.concurrent.TimeUnit

class LongRunningTask(private val listener: LongRunningTaskListener) : Runnable {

    // tag::volatile[]
    @Volatile
    var isCancelled = false
        private set
    // end::volatile[]

    // tag::handler[]
    private val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                WHAT_STARTED -> listener.started(this@LongRunningTask)
                WHAT_PROGRESS -> {
                    val progress = msg.data.getInt(PROGRESS)
                    listener.reportProgress(this@LongRunningTask, progress)
                }
                WHAT_FINISHED -> listener.finished(this@LongRunningTask)
                WHAT_CANCELLED -> listener.cancelled(this@LongRunningTask)
            }
        }
    }
    // end::handler[]

    // tag::run[]
    override fun run() {
        started()
        var count = -1
        while (count < 10 && !isCancelled) {
            count += 1
            val progress = count * 10
            try {
                TimeUnit.SECONDS.sleep(1)
            } catch (e: InterruptedException) { /* Do nothing */ }

            reportProgress(progress)
        }
        if (isCancelled) {
            cancelled()
        } else {
            finished()
        }
    }
    // end::run[]

    fun cancel() {
        isCancelled = true
    }

    private fun started() {
        val message = Message()
        message.what = WHAT_STARTED
        handler.sendMessage(message)
    }

    // tag::progress[]
    private fun reportProgress(progress: Int) {
        val bundle = Bundle()
        bundle.putInt(PROGRESS, progress)
        val message = Message()
        message.data = bundle
        message.what = WHAT_PROGRESS
        handler.sendMessage(message)
    }
    // end::progress[]

    private fun finished() {
        val message = Message()
        message.what = WHAT_FINISHED
        handler.sendMessage(message)
    }

    private fun cancelled() {
        val message = Message()
        message.what = WHAT_CANCELLED
        handler.sendMessage(message)
    }

    companion object {
        private val WHAT_STARTED = 0
        private val WHAT_PROGRESS = 1
        private val WHAT_FINISHED = 2
        private val WHAT_CANCELLED = 3
        private val PROGRESS = "progress"
    }
}
