package training.akosma.runnable

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), LongRunningTaskListener {
    private var task1: LongRunningTask? = null
    private var task2: LongRunningTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        executeButton.setOnClickListener {
            // tag::start[]
            task1 = LongRunningTask(this)
            val thread1 = Thread(task1)
            thread1.start()
            // end::start[]

            task2 = LongRunningTask(this)
            val thread2 = Thread(task2)
            thread2.start()
        }

        cancelButton.setOnClickListener {
            task1?.cancel()
            task2?.cancel()
        }
    }

    override fun started(task: LongRunningTask) {
        if (task === task1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (task === task2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    override fun finished(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.done)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.done)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun cancelled(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.cancelled)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.cancelled)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (task === task1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (task === task2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }
}
