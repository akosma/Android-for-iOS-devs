package training.akosma.coroutines

import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch

class LongRunningTask(private val listener: LongRunningTaskListener) {
    var isCancelled = false
        private set

    // tag::suspend[]
    suspend fun run() {
        launch(UI) { // <1>
            listener.started(this@LongRunningTask)
        }
        var count = -1
        while (count < 10 && !isCancelled) {
            count += 1
            val progress = count * 10
            delay(1000) // <2>

            launch(UI) {
                listener.reportProgress(this@LongRunningTask, progress)
            }
        }
        if (isCancelled) {
            launch(UI) {
                listener.cancelled(this@LongRunningTask)
            }
        } else {
            launch(UI) {
                listener.finished(this@LongRunningTask)
            }
        }
    }
    // end::suspend[]

    fun cancel() {
        isCancelled = true
    }
}
