package training.akosma.coroutines

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.*
import java.util.*

class MainActivity : AppCompatActivity(), LongRunningTaskListener {
    private var task1: LongRunningTask? = null
    private var task2: LongRunningTask? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::run[]
        executeButton.setOnClickListener {
            task1 = LongRunningTask(this)
            task2 = LongRunningTask(this)
            async {
                task1?.run()
            }
            async {
                task2?.run()
            }
        }
        // end::run[]

        cancelButton.setOnClickListener {
            task1?.cancel()
            task2?.cancel()
        }
    }

    override fun started(task: LongRunningTask) {
        if (task === task1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (task === task2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    override fun finished(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.done)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.done)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun cancelled(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.cancelled)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.cancelled)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (task === task1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (task === task2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }
}
