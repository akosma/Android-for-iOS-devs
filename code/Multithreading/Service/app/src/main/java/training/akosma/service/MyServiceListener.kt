package training.akosma.service

interface MyServiceListener {
    fun started(taskID: Int)
    fun reportProgress(taskID: Int, progress: Int)
    fun finished(taskID: Int)
    fun cancelled(taskID: Int)
}
