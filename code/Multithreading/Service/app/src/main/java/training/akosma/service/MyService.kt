package training.akosma.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

// tag::class[]
class MyService : Service(), LongRunningTaskListener {
// end::class[]

    val binder: IBinder = MyBinder()
    private val executor: ThreadPoolExecutor
    private val tasks = ArrayList<LongRunningTask>()
    private var listener: MyServiceListener? = null

    init {
        val mFactory = ThreadFactory { runnable -> Thread(runnable) }

        val coresCount = Runtime.getRuntime().availableProcessors()
        executor = ThreadPoolExecutor(coresCount * 2,
                coresCount * 2,
                60L,
                TimeUnit.SECONDS,
                LinkedBlockingQueue(),
                mFactory)
    }

    fun setListener(listener: MyServiceListener?) {
        this.listener = listener
    }

    // tag::onstart[]
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            val action = intent.action
            val taskID = intent.getIntExtra(TASK_ID, 0)
            if (ACTION_START == action) {
                val task = LongRunningTask(this)
                task.taskID = taskID
                tasks.add(task)
                executor.execute(task)
            }
        }

        return Service.START_NOT_STICKY // <1>
    }
    // end::onstart[]

    // tag::onbind[]
    override fun onBind(intent: Intent): IBinder? {
        return binder
    }
    // end::onbind[]

    internal fun cancel() {
        for (task in tasks) {
            task.cancel()
        }
    }

    override fun started(task: LongRunningTask) {
        this.listener?.started(task.taskID)
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        this.listener?.reportProgress(task.taskID, progress)
    }

    override fun finished(task: LongRunningTask) {
        this.listener?.finished(task.taskID)
        stopSelf()
    }

    override fun cancelled(task: LongRunningTask) {
        this.listener?.cancelled(task.taskID)
        stopSelf()
    }

    // tag::binder[]
    inner class MyBinder : Binder() {
        internal val service: MyService
            get() = this@MyService
    }
    // end::binder[]

    companion object {
        val ACTION_START = "training.akosma.service.action.START"
        val TASK_ID = "TASK_ID"
    }
}
