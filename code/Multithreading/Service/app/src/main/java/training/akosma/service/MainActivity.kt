package training.akosma.service

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), ServiceConnection, MyServiceListener {
    private var taskID1 = INVALID_TASK_ID
    private var taskID2 = INVALID_TASK_ID
    private var service: MyService? = null

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_main)

        if (bundle != null) {
            taskID1 = bundle.getInt(TASK_ID_1)
            taskID2 = bundle.getInt(TASK_ID_2)
            textView1.text = bundle.getString(TEXT_VIEW_1)
            textView2.text = bundle.getString(TEXT_VIEW_2)
            executeButton.isEnabled = bundle.getBoolean(EXEC_BTN_ENABLED)
            cancelButton.isEnabled = bundle.getBoolean(CANCEL_BTN_ENABLED)
        }

        executeButton.setOnClickListener {
            // tag::start[]
            taskID1 = 0
            val intent1 = Intent(this@MainActivity, MyService::class.java)
            intent1.action = MyService.ACTION_START
            intent1.putExtra(MyService.TASK_ID, taskID1)
            startService(intent1)
            // end::start[]

            taskID2 = 1
            val intent2 = Intent(this@MainActivity, MyService::class.java)
            intent2.action = MyService.ACTION_START
            intent2.putExtra(MyService.TASK_ID, taskID2)
            startService(intent2)
        }

        cancelButton.setOnClickListener {
            service?.cancel()
        }
    }

    // tag::lifecycle[]
    override fun onResume() {
        super.onResume()
        val intent = Intent(this, MyService::class.java)
        bindService(intent, this, Context.BIND_AUTO_CREATE)
    }

    override fun onPause() {
        super.onPause()
        unbindService(this)
    }

    override fun onSaveInstanceState(bundle: Bundle?) {
        super.onSaveInstanceState(bundle)
        if (bundle != null) {
            bundle.putInt(TASK_ID_1, taskID1)
            bundle.putInt(TASK_ID_2, taskID2)
            bundle.putString(TEXT_VIEW_1, textView1.text.toString())
            bundle.putString(TEXT_VIEW_2, textView2.text.toString())
            bundle.putBoolean(EXEC_BTN_ENABLED, executeButton.isEnabled)
            bundle.putBoolean(CANCEL_BTN_ENABLED, cancelButton.isEnabled)
        }
    }
    // end::lifecycle[]

    // tag::binding[]
    override fun onBindingDied(name: ComponentName) {
        service?.setListener(null)
        service = null
    }

    override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
        val binder = iBinder as MyService.MyBinder
        service = binder.service // <1>
        service?.setListener(this)     // <2>
    }

    override fun onServiceDisconnected(componentName: ComponentName) {
        service?.setListener(null)
        service = null
    }
    // end::binding[]

    override fun started(taskID: Int) {
        if (taskID == taskID1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (taskID == taskID2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    override fun finished(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.done)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.done)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun cancelled(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.cancelled)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.cancelled)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun reportProgress(taskID: Int, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (taskID == taskID1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (taskID == taskID2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }

    companion object {
        private val TASK_ID_1 = "TASK_ID_1"
        private val TASK_ID_2 = "TASK_ID_2"
        private val TEXT_VIEW_1 = "TEXT_VIEW_1"
        private val TEXT_VIEW_2 = "TEXT_VIEW_2"
        private val EXEC_BTN_ENABLED = "EXEC_BTN_ENABLED"
        private val CANCEL_BTN_ENABLED = "CANCEL_BTN_ENABLED"
        private val INVALID_TASK_ID = -1
    }
}
