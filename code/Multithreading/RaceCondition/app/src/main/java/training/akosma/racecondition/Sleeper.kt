package training.akosma.racecondition

import java.util.concurrent.TimeUnit

internal object Sleeper {
    fun sleepSeconds(amount: Int) {
        try {
            TimeUnit.SECONDS.sleep(amount.toLong())
        } catch (e: InterruptedException) {
            Thread.currentThread().interrupt()
        }
    }

    fun sleepNanoseconds(amount: Int) {
        try {
            TimeUnit.NANOSECONDS.sleep(amount.toLong())
        } catch (e: InterruptedException) {
            Thread.currentThread().interrupt()
        }
    }
}
