package training.akosma.racecondition

// tag::task[]
internal class Task(start: Runnable,
                    end: Runnable,
                    update: Runnable) {
    private val handler = TaskHandler(start, end, update)

    fun execute() {
        handler.sendStartMessage()
        Sleeper.sleepSeconds(Constants.SECONDS_PAUSE)
        handler.sendEndMessage()
        Sleeper.sleepNanoseconds(Constants.NANOSECONDS_PAUSE)
        handler.sendUpdateMessage()
    }
}
// end::task[]
