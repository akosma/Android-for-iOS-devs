package training.akosma.racecondition

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // Volatile fields can be modified by many threads at once
    @Volatile private var alive = false
    @Volatile private var counter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val start = Runnable {
            textView.text = "…"
            textView.setTextColor(Color.BLACK)
        }

        val end = Runnable {
            // tag::callback[]
            val text = if (alive) "Cat is alive" else "Cat is dead"
            textView.text = text
            // The value of "alive" can change in the meantime,
            // and we end up with "Cat is alive" in red!
            val color = if (alive) Color.GREEN else Color.RED
            textView.setTextColor(color)
            // end::callback[]
        }

        val update = Runnable {
            val value = counter + 1
            val progress = value * 100 / RUN_COUNT
            progressBar.progress = progress

            if (value == RUN_COUNT) {
                button.isEnabled = true
            }
        }

        button.setOnClickListener {
            // tag::runnable[]
            val runnable = Runnable {
                val task = Task(start, end, update)

                while (counter < RUN_COUNT) {
                    alive = true  // <1>
                    task.execute()
                    alive = false // <2>

                    Sleeper.sleepSeconds(Constants.SECONDS_PAUSE)
                    counter += 1
                }
            }

            // Launch the execution
            button.isEnabled = false
            progressBar.progress = 0
            counter = 0

            val thread = Thread(runnable)
            thread.start()
            // end::runnable[]
        }
    }

    companion object {
        private val RUN_COUNT = 20
    }
}
