package training.akosma.racecondition

internal object Constants {
    // Benchmarks done on a OnePlus 3 device running Android Oreo (8, API 26)
    // 0 ns: the cat is always dead in red
    // 20 ns: mix of values between green and red, alive and dead
    // 100 ns: the cat is almost always alive in red, some green and dead appear
    // 1000 ns (1 microseconds): the cat is almost always alive in red
    // 10000 ns (10 microseconds): the cat is almost always alive in red
    // 100000 ns (100 microseconds): the cat is almost always alive in red
    // 1000000 ns (1 millisecond): the cat is almost always alive in red
    // 10000000 ns (10 milliseconds): the cat is always alive in green
    // 100000000 ns (100 milliseconds): the cat is always alive in green
    val NANOSECONDS_PAUSE = 5
    val SECONDS_PAUSE = 1
}