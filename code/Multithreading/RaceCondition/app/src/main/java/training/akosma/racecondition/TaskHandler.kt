package training.akosma.racecondition

import android.os.Handler
import android.os.Looper
import android.os.Message

class TaskHandler internal constructor(private val start: Runnable,
                                       private val end: Runnable,
                                       private val update: Runnable) : Handler(Looper.getMainLooper()) {

    internal fun sendStartMessage() {
        sendEmptyMessage(START_MESSAGE)
    }

    internal fun sendEndMessage() {
        sendEmptyMessage(END_MESSAGE)
    }

    internal fun sendUpdateMessage() {
        sendEmptyMessage(UPDATE_MESSAGE)
    }

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            START_MESSAGE -> start.run()
            END_MESSAGE -> end.run()
            UPDATE_MESSAGE -> update.run()
        }
    }

    companion object {
        private val START_MESSAGE = 0
        private val END_MESSAGE = 1
        private val UPDATE_MESSAGE = 2
    }
}