package training.akosma.threadpoolexecutor

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), LongRunningTaskListener {
    private var task1: LongRunningTask? = null
    private var task2: LongRunningTask? = null
    private var executor: ThreadPoolExecutor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::setup[]
        val factory = ThreadFactory { runnable -> Thread(runnable) }

        val coresCount = Runtime.getRuntime().availableProcessors()
        executor = ThreadPoolExecutor(coresCount * 2,
                coresCount * 2,
                60L,
                TimeUnit.SECONDS,
                LinkedBlockingQueue(),
                factory)
        // end::setup[]

        executeButton.setOnClickListener {
            // tag::execute[]
            task1 = LongRunningTask(this)
            executor?.execute(task1)
            // end::execute[]

            task2 = LongRunningTask(this)
            executor?.execute(task2)
        }

        cancelButton.setOnClickListener {
            task1?.cancel()
            task2?.cancel()
        }
    }

    override fun started(task: LongRunningTask) {
        if (task === task1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (task === task2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    override fun finished(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.done)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.done)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun cancelled(task: LongRunningTask) {
        if (task === task1) {
            textView1.setText(R.string.cancelled)
            task1 = null
        } else if (task === task2) {
            textView2.setText(R.string.cancelled)
            task2 = null
        }
        if (task1 == null && task2 == null) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
        }
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (task === task1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (task === task2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }
}
