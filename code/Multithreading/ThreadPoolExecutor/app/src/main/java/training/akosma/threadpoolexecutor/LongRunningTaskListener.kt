package training.akosma.threadpoolexecutor

interface LongRunningTaskListener {
    fun started(task: LongRunningTask)
    fun reportProgress(task: LongRunningTask, progress: Int)
    fun finished(task: LongRunningTask)
    fun cancelled(task: LongRunningTask)
}
