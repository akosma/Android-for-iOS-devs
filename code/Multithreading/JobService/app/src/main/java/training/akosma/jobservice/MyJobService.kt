package training.akosma.jobservice

import android.app.job.JobParameters
import android.app.job.JobService
import org.greenrobot.eventbus.EventBus
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

class MyJobService : JobService(), LongRunningTaskListener {
    private val executor: ThreadPoolExecutor
    private val tasks = ArrayList<LongRunningTask>()

    init {
        val factory = ThreadFactory { runnable -> Thread(runnable) }

        val coresCount = Runtime.getRuntime().availableProcessors()
        executor = ThreadPoolExecutor(coresCount * 2,
                coresCount * 2,
                60L,
                TimeUnit.SECONDS,
                LinkedBlockingQueue(),
                factory)
    }

    // tag::start[]
    override fun onStartJob(jobParameters: JobParameters): Boolean {
        val task = LongRunningTask(this)
        task.taskID = jobParameters.jobId
        task.parameters = jobParameters
        tasks.add(task)
        executor.execute(task)

        // Return true when it is a long-running service
        return true
    }
    // end::start[]

    // tag::stop[]
    override fun onStopJob(jobParameters: JobParameters): Boolean {
        for (task in tasks) {
            if (jobParameters.jobId == task.taskID) {
                task.cancel()
            }
        }

        // Return false to drop this job, and avoid rescheduling
        return false
    }
    // end::stop[]

    override fun started(task: LongRunningTask) {
        val event = StartEvent(task.taskID)
        EventBus.getDefault().post(event)
    }

    override fun reportProgress(task: LongRunningTask, progress: Int) {
        val event = ProgressEvent(task.taskID, progress)
        EventBus.getDefault().post(event)
    }

    override fun finished(task: LongRunningTask) {
        jobFinished(task.parameters, false)
        val event = FinishEvent(task.taskID)
        EventBus.getDefault().post(event)
    }

    override fun cancelled(task: LongRunningTask) {
        // tag::postevent[]
        jobFinished(task.parameters, false) // <1>
        val event = CancelEvent(task.taskID)
        EventBus.getDefault().post(event)
        // end::postevent[]
    }

    // tag::event[]
    class ProgressEvent(val taskID: Int, val progress: Int)
    // end::event[]

    class StartEvent(val taskID: Int)

    class CancelEvent(val taskID: Int)

    class FinishEvent(val taskID: Int)
}
