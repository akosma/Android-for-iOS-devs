package training.akosma.jobservice

import android.app.job.JobParameters
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message

import java.util.concurrent.TimeUnit

class LongRunningTask(private val listener: LongRunningTaskListener) : Runnable {
    @Volatile
    var isCancelled = false
        private set
    var taskID = -1
    var parameters: JobParameters? = null

    private val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                WHAT_STARTED -> listener.started(this@LongRunningTask)
                WHAT_PROGRESS -> {
                    val progress = msg.data.getInt(PROGRESS)
                    listener.reportProgress(this@LongRunningTask, progress)
                }
                WHAT_FINISHED -> listener.finished(this@LongRunningTask)
                WHAT_CANCELLED -> listener.cancelled(this@LongRunningTask)
            }
        }
    }

    override fun run() {
        started()
        var count = -1
        while (count < 10 && !isCancelled) {
            count += 1
            val progress = count * 10
            try {
                TimeUnit.SECONDS.sleep(1)
            } catch (e: InterruptedException) { /* Do nothing */
            }

            reportProgress(progress)
        }
        if (isCancelled) {
            cancelled()
        } else {
            finished()
        }
    }

    fun cancel() {
        isCancelled = true
    }

    private fun started() {
        val message = Message()
        message.what = WHAT_STARTED
        handler.sendMessage(message)
    }

    private fun reportProgress(progress: Int) {
        val bundle = Bundle()
        bundle.putInt(PROGRESS, progress)
        val message = Message()
        message.data = bundle
        message.what = WHAT_PROGRESS
        handler.sendMessage(message)
    }

    private fun finished() {
        val message = Message()
        message.what = WHAT_FINISHED
        handler.sendMessage(message)
    }

    private fun cancelled() {
        val message = Message()
        message.what = WHAT_CANCELLED
        handler.sendMessage(message)
    }

    companion object {
        private val WHAT_STARTED = 0
        private val WHAT_PROGRESS = 1
        private val WHAT_FINISHED = 2
        private val WHAT_CANCELLED = 3
        private val PROGRESS = "progress"
    }
}
