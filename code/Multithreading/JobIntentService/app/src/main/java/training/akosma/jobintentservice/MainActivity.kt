package training.akosma.jobintentservice

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.JobIntentService
import android.support.v7.app.AppCompatActivity
import java.util.*
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class MainActivity : AppCompatActivity() {
    private var taskID1 = INVALID_TASK_ID
    private var taskID2 = INVALID_TASK_ID

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.activity_main)

        if (bundle != null) {
            taskID1 = bundle.getInt(TASK_ID_1)
            taskID2 = bundle.getInt(TASK_ID_2)
            textView1.text = bundle.getString(TEXT_VIEW_1)
            textView2.text = bundle.getString(TEXT_VIEW_2)
            executeButton.isEnabled = bundle.getBoolean(EXEC_BTN_ENABLED)
            cancelButton.isEnabled = bundle.getBoolean(CANCEL_BTN_ENABLED)
        }

        executeButton.setOnClickListener {
            // tag::launch[]
            taskID1 = 0
            val intent1 = Intent(this,
                    MyJobIntentService::class.java)
            intent1.action = MyJobIntentService.ACTION_START
            intent1.putExtra(MyJobIntentService.TASK_ID, taskID1)
            startService(intent1)

            JobIntentService.enqueueWork(this,
                    MyJobIntentService::class.java,
                    JOB_ID + 1,
                    intent1)
            // end::launch[]

            taskID2 = 1
            val intent2 = Intent(this, MyJobIntentService::class.java)
            intent2.action = MyJobIntentService.ACTION_START
            intent2.putExtra(MyJobIntentService.TASK_ID, taskID2)

            JobIntentService.enqueueWork(this, MyJobIntentService::class.java, JOB_ID + 1, intent2)
        }

        cancelButton.setOnClickListener {
            val event1 = MyJobIntentService.CancelActionEvent(taskID1)
            EventBus.getDefault().post(event1)

            val event2 = MyJobIntentService.CancelActionEvent(taskID2)
            EventBus.getDefault().post(event2)
        }
    }

    public override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    public override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onSaveInstanceState(bundle: Bundle?) {
        super.onSaveInstanceState(bundle)
        if (bundle != null) {
            bundle.putInt(TASK_ID_1, taskID1)
            bundle.putInt(TASK_ID_2, taskID2)
            bundle.putString(TEXT_VIEW_1, textView1.text.toString())
            bundle.putString(TEXT_VIEW_2, textView2.text.toString())
            bundle.putBoolean(EXEC_BTN_ENABLED, executeButton.isEnabled)
            bundle.putBoolean(CANCEL_BTN_ENABLED, cancelButton.isEnabled)
        }
    }

    @Subscribe
    fun onStartEvent(event: MyJobIntentService.StartEvent) {
        started(event.taskID)
    }

    @Subscribe
    fun onProgressEvent(event: MyJobIntentService.ProgressEvent) {
        reportProgress(event.taskID, event.progress)
    }

    @Subscribe
    fun onFinishEvent(event: MyJobIntentService.FinishEvent) {
        finished(event.taskID)
    }

    @Subscribe
    fun onCancelEvent(event: MyJobIntentService.CancelledEvent) {
        cancelled(event.taskID)
    }

    fun started(taskID: Int) {
        if (taskID == taskID1) {
            progressBar1.progress = 0
            textView1.setText(R.string.processing)
        } else if (taskID == taskID2) {
            progressBar2.progress = 0
            textView2.setText(R.string.processing)
        }
        executeButton.isEnabled = false
        cancelButton.isEnabled = true
    }

    private fun finished(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.done)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.done)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
            val intent = Intent(this, MyJobIntentService::class.java)
            stopService(intent)
        }
    }

    private fun cancelled(taskID: Int) {
        if (taskID == taskID1) {
            textView1.setText(R.string.cancelled)
            taskID1 = INVALID_TASK_ID
        } else if (taskID == taskID2) {
            textView2.setText(R.string.cancelled)
            taskID2 = INVALID_TASK_ID
        }
        if (taskID1 == INVALID_TASK_ID && taskID2 == INVALID_TASK_ID) {
            executeButton.isEnabled = true
            cancelButton.isEnabled = false
            val intent = Intent(this, MyJobIntentService::class.java)
            stopService(intent)
        }
    }

    private fun reportProgress(taskID: Int, progress: Int) {
        val template = getString(R.string.progress)
        val text = String.format(Locale.ENGLISH, template, progress)
        if (taskID == taskID1) {
            textView1.text = text
            progressBar1.progress = progress
        } else if (taskID == taskID2) {
            textView2.text = text
            progressBar2.progress = progress
        }
    }

    companion object {
        private val TASK_ID_1 = "TASK_ID_1"
        private val TASK_ID_2 = "TASK_ID_2"
        private val TEXT_VIEW_1 = "TEXT_VIEW_1"
        private val TEXT_VIEW_2 = "TEXT_VIEW_2"
        private val EXEC_BTN_ENABLED = "EXEC_BTN_ENABLED"
        private val CANCEL_BTN_ENABLED = "CANCEL_BTN_ENABLED"
        private val INVALID_TASK_ID = -1
        private val JOB_ID = 100
    }
}
