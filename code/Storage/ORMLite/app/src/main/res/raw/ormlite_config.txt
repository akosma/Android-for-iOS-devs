#
# generated on 2012/06/06 05:55:57
#
# --table-start--
dataClass=training.akosma.ormlite.Person
tableName=people
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=name
indexName=person_string_idx
# --field-end--
# --field-start--
fieldName=age
# --field-end--
# --table-fields-end--
# --table-end--
#################################