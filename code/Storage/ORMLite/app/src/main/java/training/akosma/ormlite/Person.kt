package training.akosma.ormlite

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable

import java.util.Locale

// tag::person[]
@DatabaseTable(tableName = "people")
class Person {

    @DatabaseField(generatedId = true)
    var id: Long = 0

    @DatabaseField(index = true)
    var name: String? = null

    @DatabaseField
    var age: Int = 0

    override fun toString(): String {
        return String.format(Locale.ENGLISH, "%s (%d)", name, age)
    }
}
// end::person[]
