package training.akosma.ormlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils

import java.sql.SQLException

// tag::helper[]
class DatabaseHelper(context: Context) : OrmLiteSqliteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config) {

    private var personDao: Dao<Person, Int>? = null
    private var personRuntimeDao: RuntimeExceptionDao<Person, Int>? = null

    val dao: Dao<Person, Int>
        @Throws(SQLException::class)
        get() {
            if (personDao == null) {
                personDao = getDao(Person::class.java)
            }
            return personDao!!
        }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase, connectionSource: ConnectionSource) {
        try {
            Log.i(DatabaseHelper::class.java.name, "onCreate")
            TableUtils.createTable(connectionSource, Person::class.java)
        } catch (e: SQLException) {
            Log.e(DatabaseHelper::class.java.name, "Can't create database", e)
            throw RuntimeException(e)
        }
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, connectionSource: ConnectionSource, i: Int, i1: Int) {
        try {
            Log.i(DatabaseHelper::class.java.name, "onUpgrade")
            TableUtils.dropTable<Person, Any>(connectionSource, Person::class.java, true)
            // after we drop the old databases, we create the new ones
            onCreate(sqLiteDatabase, connectionSource)
        } catch (e: SQLException) {
            Log.e(DatabaseHelper::class.java.name, "Can't drop databases", e)
            throw RuntimeException(e)
        }
    }

    fun getPersonDao(): RuntimeExceptionDao<Person, Int> {
        if (personRuntimeDao == null) {
            personRuntimeDao = getRuntimeExceptionDao(Person::class.java)
        }
        return personRuntimeDao!!
    }

    override fun close() {
        super.close()
        personDao = null
        personRuntimeDao = null
    }

    companion object {
        private val DATABASE_NAME = "helloAndroid.db"
        private val DATABASE_VERSION = 1
    }
}
// end::helper[]
