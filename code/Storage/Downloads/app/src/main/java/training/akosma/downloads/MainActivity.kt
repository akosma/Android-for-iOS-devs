package training.akosma.downloads

import android.Manifest
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity() {

    private var queueID: Long = 0

    private fun hasPermission(permission: String): Boolean {
        val check = ActivityCompat.checkSelfPermission(this, permission)
        return check == PERMISSION_GRANTED
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val read = Manifest.permission.READ_EXTERNAL_STORAGE
        val write = Manifest.permission.WRITE_EXTERNAL_STORAGE
        if (!hasPermission(read) && !hasPermission(write)) {
            val permissions = arrayOf(read, write)
            ActivityCompat.requestPermissions(this, permissions, 0)
            return
        }
        download()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 0) {
            var granted = true

            for (grantResult in grantResults) {
                granted = granted and (grantResult == PERMISSION_GRANTED)
            }

            if (granted) {
                download()
            }
        }
    }

    private fun download() {
        // tag::service[]
        val manager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        // end::service[]

        // tag::notification[]
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == action) {
                    intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0)
                    val query = DownloadManager.Query()
                    query.setFilterById(queueID)
                    val c: Cursor? = manager.query(query)
                    if (c != null && c.moveToFirst()) {
                        val columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS)
                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            val index = c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)
                            val uriString = c.getString(index)
                            imageView.setImageURI(Uri.parse(uriString))
                        }
                        c.close()
                    }
                }
            }
        }
        registerReceiver(receiver, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        // end::notification[]

        // tag::start[]
        val path = Environment.getExternalStorageDirectory().toString()
        val folder = File(path + "/AndroidTutorial")
        if (!folder.exists()) {
            folder.mkdirs()
        }

        val url = "https://upload.wikimedia.org/wikipedia/commons/b/b4/LocationItaly.png"
        val uri = Uri.parse(url)
        val request = DownloadManager.Request(uri)

        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
                or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle("AndroidTutorial")
                .setDescription("Wikipedia image")
                .setDestinationInExternalPublicDir("/AndroidTutorial",
                        "photo.jpg")
        queueID = manager.enqueue(request)
        // end::start[]
    }
}
