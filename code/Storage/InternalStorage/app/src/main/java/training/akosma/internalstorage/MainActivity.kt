package training.akosma.internalstorage

import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class MainActivity : AppCompatActivity() {
    // tag::auto[]
    private val handler = Handler()

    internal var periodicWriter: Runnable = object : Runnable {
        override fun run() {
            writeFile()
            val interval = 5000
            handler.postDelayed(this, interval.toLong())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onPause() {
        super.onPause()
        writeFile()
        handler.removeCallbacks(periodicWriter)
    }

    override fun onResume() {
        super.onResume()
        readFile()
        periodicWriter.run()
    }
    // end::auto[]

    // tag::readwrite[]
    private fun writeFile() {
        try {
            val file = File(filesDir, filename)
            val os = FileOutputStream(file)
            val text = editor.text.toString()
            os.write(text.toByteArray())
            os.close()
            Log.i("MainActivity", "File saved")
        } catch (e: Exception) {
            Log.e("MainActivity", "Problem writing file", e)
        }

    }

    private fun readFile() {
        try {
            val file = File(filesDir, filename)
            if (file.exists()) {
                val inputStream = FileInputStream(file)
                val text = inputStream.bufferedReader().readText()
                editor.setText(text)
            }
        } catch (e: Exception) {
            Log.e("MainActivity", "Problem reading file", e)
        }

    }
    // end::readwrite[]

    companion object {
        private val filename = "ideas.txt"
    }
}
