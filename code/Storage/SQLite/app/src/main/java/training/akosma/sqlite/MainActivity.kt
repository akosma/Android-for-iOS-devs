package training.akosma.sqlite

import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import training.akosma.sqlite.DatabaseHelper.Companion.AGE_FIELD
import training.akosma.sqlite.DatabaseHelper.Companion.ID_FIELD
import training.akosma.sqlite.DatabaseHelper.Companion.NAME_FIELD
import training.akosma.sqlite.DatabaseHelper.Companion.PEOPLE_TABLE
import java.util.*

class MainActivity : AppCompatActivity() {

    private var helper: DatabaseHelper? = null
    private var database: SQLiteDatabase? = null
    private var random = Random()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        helper = DatabaseHelper(this)
        database = helper?.writableDatabase

        insertValues()
        showAllPeople()
    }

    // tag::select[]
    private fun queryValues(): List<Person> {
        val table = PEOPLE_TABLE
        val columns = arrayOf(ID_FIELD, NAME_FIELD, AGE_FIELD)
        val selection: String? = null
        val selectionArgs: Array<String>? = null
        val groupBy: String? = null
        val having: String? = null
        val orderBy = NAME_FIELD
        val peopleCursor = database?.query(table,
                columns,
                selection,
                selectionArgs,
                groupBy,
                having,
                orderBy)

        val people = ArrayList<Person>()
        if (peopleCursor != null) {
            var p: Person

            peopleCursor.moveToFirst()
            if (!peopleCursor.isAfterLast) {
                do {
                    val id = peopleCursor.getLong(0)
                    val name = peopleCursor.getString(1)
                    val age = peopleCursor.getInt(2)
                    p = Person()
                    p.id = id
                    p.name = name
                    p.age = age
                    people.add(p)
                } while (peopleCursor.moveToNext())
            }
            peopleCursor.close()

            Log.i("MainActivity", "Found people: " + people)
        }
        return people
    }
    // end::select[]

    // tag::insert[]
    private fun insertValues() {
        val p1 = Person()
        p1.name = "John Smith"
        p1.age = 56

        val p2 = Person()
        p2.name = "Maria Rogers"
        p2.age = 33

        val values1 = p1.contentValues
        database?.insert(PEOPLE_TABLE, null, values1)

        val values2 = p2.contentValues
        database?.insert(PEOPLE_TABLE, null, values2)
    }
    // end::insert[]

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> {
                val name = generateRandomName() + " " + generateRandomName()
                val age = generateRandomNumberBetween(15, 75)

                val p1 = Person()
                p1.name = name
                p1.age = age

                val values1 = p1.contentValues
                database?.insert(PEOPLE_TABLE, null, values1)

                showAllPeople()

                return true
            }

            R.id.clean -> {
                database?.delete(PEOPLE_TABLE, null, null)
                showAllPeople()
                return super.onOptionsItemSelected(item)
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showAllPeople() {
        val people = queryValues()
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, people)
        listView.adapter = adapter
    }

    // Adapted from
    // https://stackoverflow.com/a/5271613/133764
    private fun generateRandomNumberBetween(min: Int, max: Int): Int {
        return random.nextInt(max - min) + min
    }

    // Adapted from
    // https://stackoverflow.com/a/12116327/133764
    private fun generateRandomName(): String {
        val randomStringBuilder = StringBuilder()
        var tempChar = generateRandomNumberBetween(65, 90).toChar()
        randomStringBuilder.append(tempChar)
        val randomLength = generateRandomNumberBetween(5, 15)
        for (i in 0 until randomLength) {
            tempChar = generateRandomNumberBetween(97, 122).toChar()
            randomStringBuilder.append(tempChar)
        }
        return randomStringBuilder.toString()
    }
}
