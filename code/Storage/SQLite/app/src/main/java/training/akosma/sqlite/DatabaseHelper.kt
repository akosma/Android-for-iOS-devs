package training.akosma.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

// tag::helper[]
class DatabaseHelper internal constructor(context: Context) : SQLiteOpenHelper(context, FILE_NAME, null, VERSION) {

    override fun onCreate(database: SQLiteDatabase) {
        val sb = StringBuilder("CREATE TABLE ")
        sb.append(PEOPLE_TABLE)
                .append(" (")
                .append(ID_FIELD)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL")
                .append(", ")
                .append(NAME_FIELD)
                .append(" TEXT")
                .append(", ")
                .append(AGE_FIELD)
                .append(" INT")
                .append(");")
        database.execSQL(sb.toString())
    }

    override fun onUpgrade(database: SQLiteDatabase, i: Int, i1: Int) {

    }

    companion object {
        private val FILE_NAME = "data.sqlite"
        private val VERSION = 1
        internal val PEOPLE_TABLE = "people"
        internal val ID_FIELD = "id"
        internal val NAME_FIELD = "name"
        internal val AGE_FIELD = "age"
    }
}
// end::helper[]
