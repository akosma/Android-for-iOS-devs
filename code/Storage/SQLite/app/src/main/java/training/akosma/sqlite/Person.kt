package training.akosma.sqlite

import android.content.ContentValues
import training.akosma.sqlite.DatabaseHelper.Companion.AGE_FIELD
import training.akosma.sqlite.DatabaseHelper.Companion.NAME_FIELD
import java.util.*

class Person {
    var id: Long = 0
    var name: String? = null
    var age: Int = 0

    // tag::values[]
    val contentValues: ContentValues
        get() {
            val values = ContentValues()
            values.put(NAME_FIELD, name)
            values.put(AGE_FIELD, age)
            return values
        }

    override fun toString(): String {
        return String.format(Locale.ENGLISH, "%s(%d)", name, age)
    }
    // end::values[]
}
