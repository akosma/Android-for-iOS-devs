package training.akosma.resources

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import org.xml.sax.SAXException
import java.io.IOException
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.ParserConfigurationException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buffer = StringBuilder()
        try {
            // tag::activity[]
            val factory = DocumentBuilderFactory.newInstance()
            val builder = factory.newDocumentBuilder()

            val inputStream = resources.openRawResource(R.raw.data)

            val doc = builder.parse(inputStream)
            val root = doc.documentElement
            val entries = root.childNodes
            // end::activity[]

            for (i in 0 until entries.length) {
                val entry = entries.item(i)
                val fields = entry.childNodes

                for (j in 0 until fields.length) {
                    val field = fields.item(j)
                    val name = field.nodeName

                    if (name == "title") {
                        val title = field.textContent
                        buffer.append(title)
                        buffer.append("\n")
                        Log.i("MainActivity", "Title node found: " + title)
                        break
                    }
                }
            }
            textView.text = buffer.toString()
        } catch (e: SAXException) {
            Log.e("MainActivity", "Error parsing XML", e)
        } catch (e: IOException) {
            Log.e("MainActivity", "Error reading file", e)
        } catch (e: ParserConfigurationException) {
            Log.e("MainActivity", "XML parser configuration error", e)
        }
    }
}
