package training.akosma.preferences

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            val i = Intent(this, SettingsActivity::class.java)
            startActivity(i)
        }

        // tag::prefs[]
        val pm = PreferenceManager.getDefaultSharedPreferences(this)
        val value = pm.getString("value", "default value")
        val decision = pm.getBoolean("decision", true)
        val age = pm.getInt("age", 30)
        // end::prefs[]
    }
}
