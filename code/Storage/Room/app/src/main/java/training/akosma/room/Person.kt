package training.akosma.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

import java.util.Locale

// tag::person[]
@Entity
class Person {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "age")
    var age: Int = 0

    override fun toString(): String {
        return String.format(Locale.ENGLISH, "%s (%d)", name, age)
    }
}
// end::person[]
