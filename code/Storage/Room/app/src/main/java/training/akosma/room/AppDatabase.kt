package training.akosma.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

// tag::database[]
@Database(entities = arrayOf(Person::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun personDao(): PersonDao
}
// end::database[]
