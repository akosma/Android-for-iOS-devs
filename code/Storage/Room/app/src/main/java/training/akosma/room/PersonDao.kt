package training.akosma.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

// tag::persondao[]
@Dao
interface PersonDao {
    @get:Query("SELECT * FROM person")
    val all: List<Person>

    @Query("SELECT * FROM person WHERE id IN (:arg0)")
    fun loadAllByIds(userIds: IntArray): List<Person>

    @Query("SELECT * FROM person WHERE name LIKE :arg0 LIMIT 1")
    fun findByName(name: String): Person

    @Insert
    fun insertAll(vararg people: Person)

    @Delete
    fun delete(person: Person)

    @Query("DELETE FROM person")
    fun deleteAll()
}
// end::persondao[]
