package training.akosma.room

import android.arch.persistence.room.Room
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private var database: AppDatabase? = null
    private val random = Random()
    private val queue = DispatchQueue("db")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::setup[]
        database = Room.databaseBuilder(applicationContext,
                AppDatabase::class.java, "people").build()

        val john = Person()
        john.name = "John Smith"
        john.age = 35
        val lucy = Person()
        lucy.name = "Lucy Skies"
        lucy.age = 42
        // end::setup[]

        insertPeople(john, lucy)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> {
                val name = generateRandomName() + " " + generateRandomName()
                val age = generateRandomNumberBetween(15, 75)

                val newPerson = Person()
                newPerson.name = name
                newPerson.age = age
                insertPeople(newPerson)
                return true
            }

            R.id.clean -> {
                deletePeople()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun insertPeople(vararg people: Person) {
        // tag::usage[]
        queue.async {
            val personDao = database?.personDao()
            personDao?.insertAll(*people)
            val savedPeople = personDao?.all

            DispatchQueue.main.async {
                showPeople(savedPeople)
            }
        }
        // end::usage[]
    }

    private fun deletePeople() {
        queue.async {
            val personDao = database?.personDao()
            personDao?.deleteAll()
            val savedPeople = personDao?.all

            DispatchQueue.main.async {
                showPeople(savedPeople)
            }
        }
    }

    private fun showPeople(people: List<Person>?) {
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, people)
        listView.adapter = adapter
    }

    // Adapted from
    // https://stackoverflow.com/a/5271613/133764
    private fun generateRandomNumberBetween(min: Int, max: Int): Int {
        return random.nextInt(max - min) + min
    }

    // Adapted from
    // https://stackoverflow.com/a/12116327/133764
    private fun generateRandomName(): String {
        val randomStringBuilder = StringBuilder()
        var tempChar = generateRandomNumberBetween(65, 90).toChar()
        randomStringBuilder.append(tempChar)
        val randomLength = generateRandomNumberBetween(5, 15)
        for (i in 0 until randomLength) {
            tempChar = generateRandomNumberBetween(97, 122).toChar()
            randomStringBuilder.append(tempChar)
        }
        return randomStringBuilder.toString()
    }
}
