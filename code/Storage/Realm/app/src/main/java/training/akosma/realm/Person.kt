package training.akosma.realm

import java.util.Locale

import io.realm.RealmObject

// tag::person[]
open class Person : RealmObject() { // <1>
    var name: String? = null
    var age: Int = 0

    override fun toString(): String {
        return String.format(Locale.ENGLISH, "%s (%d)", name, age)
    }
}
// end::person[]
