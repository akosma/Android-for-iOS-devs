package training.akosma.realm

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import io.realm.Realm
import io.realm.RealmConfiguration
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    internal var random = Random()
    private var realm: Realm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::usage[]
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder().build()
        Realm.setDefaultConfiguration(realmConfiguration)

        realm = Realm.getDefaultInstance()

        realm?.executeTransaction { realm ->
            val john = realm.createObject(Person::class.java)
            john.name = "John Smith"
            john.age = 40

            val mary = realm.createObject(Person::class.java)
            mary.name = "Mary Poppins"
            mary.age = 60
        }
        val person = realm?.where(Person::class.java)?.equalTo("age", 60L)?.findFirst()
        Log.i("MainActivity", "Person found: " + person!!)
        showPeople()
        // end::usage[]
    }

    override fun onDestroy() {
        super.onDestroy()
        realm!!.close()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> {

                realm?.executeTransaction { realm ->
                    val name = generateRandomName() + " " + generateRandomName()
                    val age = generateRandomNumberBetween(15, 75)

                    val newPerson = realm.createObject(Person::class.java)
                    newPerson.name = name
                    newPerson.age = age
                }

                showPeople()

                return true
            }

            R.id.clean -> {
                realm?.executeTransaction { realm -> realm.deleteAll() }
                showPeople()
                return super.onOptionsItemSelected(item)
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showPeople() {
        val people = realm?.where(Person::class.java)?.findAll()
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, people)
        listView.adapter = adapter
    }

    // Adapted from
    // https://stackoverflow.com/a/5271613/133764
    private fun generateRandomNumberBetween(min: Int, max: Int): Int {
        return random.nextInt(max - min) + min
    }

    // Adapted from
    // https://stackoverflow.com/a/12116327/133764
    private fun generateRandomName(): String {
        val randomStringBuilder = StringBuilder()
        var tempChar = generateRandomNumberBetween(65, 90).toChar()
        randomStringBuilder.append(tempChar)
        val randomLength = generateRandomNumberBetween(5, 15)
        for (i in 0 until randomLength) {
            tempChar = generateRandomNumberBetween(97, 122).toChar()
            randomStringBuilder.append(tempChar)
        }
        return randomStringBuilder.toString()
    }
}
