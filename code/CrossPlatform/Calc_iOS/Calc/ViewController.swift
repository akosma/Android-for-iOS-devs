import UIKit
import JavaScriptCore

// Adapted from
// https://gist.github.com/zeitiger/1387f7d996f64b493608
public typealias LoggerFunction = (@convention(block) (String) -> Void)

@objc
public protocol ConsoleProtocol: JSExport {
    var log: LoggerFunction? { get }
}

@objc
public class Console: NSObject, ConsoleProtocol {
    public var log: LoggerFunction?
    public override init() {
        super.init()
        self.log = { (message:String) in
            print(message)
        }
    }
}

class ViewController: UIViewController {
    private let console: ConsoleProtocol = Console()

    lazy var context: JSContext? = {
        let context = JSContext()
        
        context?.setObject(console, forKeyedSubscript: "console" as NSCopying & NSObjectProtocol)
        
        guard let path = Bundle.main.path(forResource: "bundle", ofType: "js") else {
            return nil
        }
        
        do {
            let js = try String(contentsOfFile: path, encoding: .utf8)
            _ = context?.evaluateScript(js)
        }
        catch {
            print("Could not load JS code")
        }
        
        return context
    }()
    
    @IBOutlet weak var label: UILabel!
    
    @IBAction func execute(_ sender: Any) {
        _ = context?.evaluateScript("console.log('Testing console.log');")
        _ = context?.evaluateScript("calc.a = 10;")
        _ = context?.evaluateScript("calc.b = 23;")
        let result = context?.evaluateScript("calc.execute('+');")
        label.text = result?.toString()
    }
}
