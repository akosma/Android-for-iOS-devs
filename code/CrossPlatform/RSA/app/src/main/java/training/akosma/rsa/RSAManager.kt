package training.akosma.rsa

// To install GMP just clone
// https://github.com/Rupan/gmp
// into this project folder:
// app/src/main/jniLibs
// as explained in
// http://stackoverflow.com/a/29508776/133764

// tag::jni[]
class RSAManager {
    val p = "162259276829213363391578010288127"
    val q = "618970019642690137449562111"
    val e = "170141183460469231731687303715884105727"
    var n = ""
    var d = ""

    init {
        initialize()
        keys()
    }

    external fun initialize() // <2>
    external fun destroy()
    external fun keys()
    external fun encrypt(text: String): String
    external fun decrypt(text: String): String

    companion object {
        init {
            System.loadLibrary("rsa") // <1>
        }
    }
}
// end::jni[]
