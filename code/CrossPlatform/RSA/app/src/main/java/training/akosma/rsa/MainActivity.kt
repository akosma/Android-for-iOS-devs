package training.akosma.rsa

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Example of a call to a native method
        // tag::usage[]
        val rsa = RSAManager()
        val message = "1234567890"
        val encrypted = rsa.encrypt(message)
        val decrypted = rsa.decrypt(encrypted)

        originalView.text = "Message: $message"
        encryptedView.text = "Encrypted: $encrypted"
        decryptedView.text = "Decrypted: $decrypted"
        publicKeyView.text = "Public key: (${rsa.e}, ${rsa.n})"
        privateKeyView.text = "Private key: (${rsa.d}, ${rsa.n})"

        rsa.destroy()
        // end::usage[]
    }
}
