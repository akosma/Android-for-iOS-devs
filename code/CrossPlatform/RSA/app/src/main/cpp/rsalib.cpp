#include "rsalib.h"
#include <gmpxx.h>
#include <assert.h>

using std::make_pair;

using internal_keyset = map<string, pair<mpz_class, mpz_class>>;

// Calculation of the RSA internal_keyset using GMP
// https://gmplib.org/
// Based on an earlier version in PHP
// https://gist.github.com/akosma/9058c43c76da2e6691637b1332058ddc
internal_keyset rsa_keys(const mpz_t p, const mpz_t q, const mpz_t e) {
    mpz_t d, lambda, gcd;
    mpz_inits(d, lambda, gcd, NULL);

    mpz_class pc{p};
    mpz_class qc{q};
    mpz_class nc = pc * qc;
    mpz_class pc_1 = pc - 1;
    mpz_class qc_1 = qc - 1;

    mpz_lcm(lambda, pc_1.get_mpz_t(), qc_1.get_mpz_t());
    mpz_class lambdac{lambda};

    // e must be bigger than 1
    mpz_class ec{e};
    assert(ec > 1);

    // e must be smaller than lambda
    assert(ec < lambdac);

    // GCD(e, lambda) must be 1
    mpz_gcd(gcd, e, lambda);
    mpz_class gcdc{gcd};
    assert(gcdc == 1);

    mpz_invert(d, e, lambda);
    mpz_class dc{d};

    // e * d MOD lambda must be 1
    mpz_class calc = ec * dc % lambdac;
    assert(calc == 1);

    internal_keyset result{{"public",  make_pair(ec, nc)},
                           {"private", make_pair(dc, nc)}};

    mpz_clears(d, gcd, lambda, NULL);

    return result;
}

// tag::encrypt[]
// RSA encryption
mpz_class rsa_encrypt(const mpz_t message,
                      const mpz_t e,
                      const mpz_t n) {
    mpz_t encrypted;
    mpz_init(encrypted);
    mpz_powm(encrypted, message, e, n);
    mpz_class result{encrypted};
    mpz_clear(encrypted);
    return result;
}
// end::encrypt[]

// RSA decryption
mpz_class rsa_decrypt(const mpz_t encrypted,
                      const mpz_t d,
                      const mpz_t n) {
    mpz_t original;
    mpz_init(original);
    mpz_powm(original, encrypted, d, n);
    mpz_class result{original};
    mpz_clear(original);
    return result;
}

keyset rsa_keys(string p, string q, string e) {
    mpz_class pc{p};
    mpz_class qc{q};
    mpz_class ec{e};
    auto k = rsa_keys(pc.get_mpz_t(), qc.get_mpz_t(), ec.get_mpz_t());
    string n = k["public"].second.get_str(10);
    string d = k["private"].first.get_str(10);
    keyset result{{"public",  make_pair(e, n)},
                  {"private", make_pair(d, n)}};
    return result;
}

string rsa_encrypt(string message, string e, string n) {
    mpz_class original{message};

    mpz_class ec{e};
    mpz_class nc{n};

    mpz_class encrypted = rsa_encrypt(original.get_mpz_t(), ec.get_mpz_t(), nc.get_mpz_t());
    string result = encrypted.get_str(10);
    return result;
}

string rsa_decrypt(string message, string d, string n) {
    mpz_class original{message};

    mpz_class dc{d};
    mpz_class nc{n};

    mpz_class encrypted = rsa_decrypt(original.get_mpz_t(), dc.get_mpz_t(), nc.get_mpz_t());
    string result = encrypted.get_str(10);
    return result;
}
