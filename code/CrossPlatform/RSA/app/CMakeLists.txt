cmake_minimum_required(VERSION 3.4.1)
set(CMAKE_CXX_STANDARD 11)

include_directories(${CMAKE_SOURCE_DIR}/src/main/cpp/include/)
set(SOURCES
    src/main/cpp/rsa_interface.cpp
    src/main/cpp/rsalib.cpp)

add_library(rsa SHARED ${SOURCES})
add_library(gmp SHARED IMPORTED)
add_library(gmpxx SHARED IMPORTED)

set_target_properties(gmp
                      PROPERTIES IMPORTED_LOCATION
                      ${CMAKE_SOURCE_DIR}/libs/${ANDROID_ABI}/libgmp.so)

set_target_properties(gmpxx
                      PROPERTIES IMPORTED_LOCATION
                      ${CMAKE_SOURCE_DIR}/libs/${ANDROID_ABI}/libgmpxx.so)

target_link_libraries(rsa gmp gmpxx)
