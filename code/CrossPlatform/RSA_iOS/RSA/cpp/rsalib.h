//
// Created by Adrian Kosmaczewski on 12.03.17.
//
#ifndef CPPAPP_RSALIB_H
#define CPPAPP_RSALIB_H

#import <string>
#import <map>
#import <utility>

using std::string;
using std::pair;
using std::map;
using keyset = map<string, pair<string, string>>;

keyset rsa_keys(string p, string q, string e);
string rsa_encrypt(string message, string e, string n);
string rsa_decrypt(string message, string d, string n);

#endif //CPPAPP_RSALIB_H
