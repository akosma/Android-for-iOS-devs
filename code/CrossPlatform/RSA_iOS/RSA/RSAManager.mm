#import "RSAManager.h"
#import "rsalib.h"

@implementation RSAManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.p = @"162259276829213363391578010288127";
        self.q = @"618970019642690137449562111";
        self.e = @"170141183460469231731687303715884105727";
        
        [self keys];
    }
    return self;
}

- (void)keys
{
    string p = [self.p cStringUsingEncoding:NSUTF8StringEncoding];
    string q = [self.q cStringUsingEncoding:NSUTF8StringEncoding];
    string e = [self.e cStringUsingEncoding:NSUTF8StringEncoding];
    keyset keys = rsa_keys(p, q, e);
    string d = keys["private"].first;
    string n = keys["private"].second;
    self.d = [NSString stringWithCString:d.c_str() encoding:NSUTF8StringEncoding];
    self.n = [NSString stringWithCString:n.c_str() encoding:NSUTF8StringEncoding];
}

- (nonnull NSString *)encrypt:(nonnull NSString *)message
{
    string e = [self.e cStringUsingEncoding:NSUTF8StringEncoding];
    string n = [self.n cStringUsingEncoding:NSUTF8StringEncoding];
    string msg = [message cStringUsingEncoding:NSUTF8StringEncoding];
    string encrypted = rsa_encrypt(msg, e, n);
    return [NSString stringWithCString:encrypted.c_str() encoding:NSUTF8StringEncoding];
}

- (nonnull NSString *)decrypt:(nonnull NSString *)message
{
    string d = [self.d cStringUsingEncoding:NSUTF8StringEncoding];
    string n = [self.n cStringUsingEncoding:NSUTF8StringEncoding];
    string msg = [message cStringUsingEncoding:NSUTF8StringEncoding];
    string decrypted = rsa_decrypt(msg, d, n);
    return [NSString stringWithCString:decrypted.c_str() encoding:NSUTF8StringEncoding];
}

@end
