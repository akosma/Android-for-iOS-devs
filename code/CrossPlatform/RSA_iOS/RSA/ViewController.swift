import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var encryptedLabel: UILabel!
    @IBOutlet weak var decryptedLabel: UILabel!
    @IBOutlet weak var privateKeyLabel: UILabel!
    @IBOutlet weak var publicKeyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rsa = RSAManager()
        let message = "1234567890"
        let encrypted = rsa.encrypt(message)
        let decrypted = rsa.decrypt(encrypted)
        
        messageLabel.text = "Message: \(message)"
        encryptedLabel.text = "Encrypted: \(encrypted)"
        decryptedLabel.text = "Decrypted: \(decrypted)"
        publicKeyLabel.text = "Public key: (\(rsa.e), \(rsa.n))"
        privateKeyLabel.text = "Private key: (\(rsa.d), \(rsa.n))"
    }
}
