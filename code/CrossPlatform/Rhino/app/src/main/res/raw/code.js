var Controller = function () {
    const privateValue = 42;

    return {
        specialMethod: (input) => {
            var s = "Input: " + input + "; private value: " + privateValue;
            console.log(s);
            alert("Date: " + new Date());
            return s;
        }
    };
}();
