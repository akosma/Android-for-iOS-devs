package training.akosma.rhino

import android.util.Log

// tag::console[]
class Console {
    fun log(s: String) {
        Log.i("JavaScript", s)
    }
}
// end::console[]
