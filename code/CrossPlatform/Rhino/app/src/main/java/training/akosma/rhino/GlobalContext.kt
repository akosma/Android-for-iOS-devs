package training.akosma.rhino

import android.support.v7.app.AlertDialog
import org.mozilla.javascript.Context
import org.mozilla.javascript.Function
import org.mozilla.javascript.Scriptable
import org.mozilla.javascript.ScriptableObject
import java.util.*

class GlobalContext(private val androidContext: android.content.Context)
    : ScriptableObject() {

    private val jsContext: Context
    private val scope: ScriptableObject
    private var specialMethod: Function? = null
    private val console = Console()

    init {
        // tag::init[]
        // Start a JavaScript androidContext
        jsContext = Context.enter()

        // Courtesy of
        // https://stackoverflow.com/a/3859485/133764
        jsContext.optimizationLevel = -1  // <1>

        // Initialize the androidContext and return a scope
        scope = jsContext.initStandardObjects()

        // Add a `console` object
        val wrappedConsole = Context.javaToJS(console, scope)
        ScriptableObject.putProperty(scope, "console", wrappedConsole)

        // Add an `alert()` function, because,
        // what would be JavaScript without `alert()`?
        val params: Array<Class<*>> = arrayOf(String::class.java)
        val alertMethod = GlobalContext::class.java.getMethod("alert", *params) // <2>
        val alertFunction = GlobalFunctionObject("alert", alertMethod, this)
        scope.put("alert", scope, alertFunction)
        // end::init[]
    }

    override fun getClassName() = "global"

    fun evaluateScript(script: String) {
        // tag::eval[]
        jsContext.evaluateString(scope, script,
                "evaluated",
                1,
                null);
        // end::eval[]
    }

    fun compileScript(name: String, script: String) {
        // tag::compile[]
        val compiledScript = jsContext.compileString(script,
                name,
                1,
                null)
        compiledScript.exec(jsContext, scope)
        // end::compile[]
    }

    fun parseObjects() {
        // tag::parse[]
        // Find the `Controller` object defined in the scope
        val controller = scope.get("Controller", scope) as Scriptable
        if (controller != Scriptable.NOT_FOUND) {
            // Find `specialMethod()` in the Controller object
            val method = controller.get("specialMethod", scope) as Function?
            if (method is Function) {
                this.specialMethod = method
            }
        }
        // end::parse[]
    }

    fun callSpecialMethod(): Any? {
        // tag::call[]
        val date = Date()
        val arguments: Array<Any> = arrayOf(date.toString())
        val result = specialMethod?.call(jsContext, scope, scope, arguments)
        // end::call[]
        return result
    }

    fun alert(message: String) {
        val builder = AlertDialog.Builder(androidContext)
        builder.setTitle("JavaScript alert").setMessage(message)
        val dialog = builder.create()
        dialog.show()
    }

    fun finish() {
        // tag::finish[]
        Context.exit()
        // end::finish[]
    }
}