package training.akosma.rhino

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    val global = GlobalContext(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Read JS code bundled in resources
        val inputStream = resources.openRawResource(R.raw.code)
        val script = inputStream.bufferedReader().readText()

        try {
            global.compileScript("compiled", script)
            global.parseObjects()
        }
        catch (e: Exception) {
            Log.i("JavaScript Exception", "Exception compiling script: ${e.message}")
        }

        button.setOnClickListener {
            val result = global.callSpecialMethod()
            textView.text = result?.toString()
        }
    }

    override fun onStop() {
        super.onStop()
        global.finish()
    }
}
