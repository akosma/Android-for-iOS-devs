package training.akosma.rhino

import org.mozilla.javascript.Context
import org.mozilla.javascript.FunctionObject
import org.mozilla.javascript.Scriptable
import java.lang.reflect.Member

// Adapted from
// https://stackoverflow.com/a/16479685/133764
// tag::globalfunc[]
class GlobalFunctionObject constructor(name: String,
                                       methodOrConstructor: Member,
                                       parentScope: Scriptable)
    : FunctionObject(name, methodOrConstructor, parentScope) {

    override fun call(cx: Context,
                      scope: Scriptable,
                      thisObj: Scriptable,
                      args: Array<Any>): Any {
        return super.call(cx, scope, parentScope, args)
    }
}
// end::globalfunc[]
