export interface Operation {
    (a: number, b: number): number;
}

export function sum (a: number, b: number): number {
    return a + b;
}

export function subtraction (a: number, b: number): number {
    return a - b;
}

export function product (a: number, b: number): number {
    return a * b;
}

export function division (a: number, b: number): number {
    return a / b;
}
