import { Operation, sum, subtraction, product, division } from "./operations/Operation";

export class Calc {
    private _a: number = 0;
    private _b: number = 0;

    constructor() {
        console.log("Building new calculator")
    }
    
    public get a() : number {
        return this._a;
    }
    
    public set a(v : number) {
        console.log("Setting a")
        this._a = v;
    }
    
    public get b() : number {
        return this._b;
    }
    
    public set b(v : number) {
        console.log("Setting b")
        this._b = v;
    }

    public reset() {
        this._a = 0;
        this._b = 0;
    }

    public async longCalculation(a: number): Promise<number> {
        return new Promise<number>((resolve) => {
            setTimeout(() => {
                resolve(a);
            }, 2000);
        });
    }

    public execute(operator: string): number {
        console.log("Executing calculation")
        let op: Operation = this.operationFactory(operator);
        return op(this.a, this.b);
    }

    private operationFactory(operator: string): Operation {
        switch (operator) {
            case "+":
                return sum;

            case "-":
                return subtraction;

            case "*":
                return product;

            case "/":
                return division;

            default:
                throw "Invalid operation";
        }
    }
}
