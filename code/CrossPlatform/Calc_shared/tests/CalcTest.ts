import { expect } from "chai";
import { Calc } from "../src/Calc";

describe("Calc class", () => {
    const calc = new Calc();

    beforeEach(() => {
        calc.reset();
        expect(calc.a).to.equal(0);
        expect(calc.b).to.equal(0);
    });

    it("can sum", () => {
        calc.a = 10;
        calc.b = 23;
        const result = calc.execute("+");
        expect(result).to.equal(33);
    });

    it("can subtract", () => {
        calc.a = 10;
        calc.b = 23;
        const result = calc.execute("-");
        expect(result).to.equal(-13);
    });

    it("can multiply", () => {
        calc.a = 10;
        calc.b = 23;
        const result = calc.execute("*");
        expect(result).to.equal(230);
    });

    it("can divide", () => {
        calc.a = 10;
        calc.b = 23;
        const result = calc.execute("/");
        expect(result).closeTo(0.4347, 0.001);
    });

    it("can divide by zero and return infinity", () => {
        calc.a = 1;
        calc.b = 0;
        const result = calc.execute("/");
        expect(result).to.equal(Infinity);
    });

    it("can perform a long calculation", async () => {
        let result = await calc.longCalculation(1000000000)
        expect(result).to.equal(1000000000);
    }).timeout(5000);
});
