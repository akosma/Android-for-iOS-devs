package training.akosma.rxcalculator.engine

import dagger.Module
import dagger.Provides
import training.akosma.rxcalculator.engine.storage.MemoryStorage
import training.akosma.rxcalculator.engine.storage.Storage

@Module
class CalculatorModule {

    @Provides
    internal fun provideCalculator(): Calculator {
        return LongIntegerCalculator()
    }

    @Provides
    internal fun provideStorage(): Storage {
        return MemoryStorage()
    }
}
