package training.akosma.rxcalculator.engine

import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subjects.PublishSubject
import training.akosma.rxcalculator.engine.operations.Operation
import training.akosma.rxcalculator.engine.storage.Storage

internal class LongIntegerCalculator : Calculator {

    private var currentOperation: Operation<Long>? = null
    private var tempRegister: Long = 0
    private var register: Long = 0
    private var digitsCount: Long = 0
    private var storage: Storage? = null

    private val subject = PublishSubject.create<Long>()

    override val observable: Observable<Long>
        get() = subject

    override fun setStorage(storage: Storage) {
        this.storage = storage
    }

    // tag::assert[]
    override fun enterDigit(digit: Digit) {
        require(digit.value >= 0 && digit.value < 10) { // <1>
            "The value must always be a single digit"
        }

        if (digitsCount == 0L) {
            register = digit.value.toLong()
        } else {
            register = register * 10 + digit.value
        }

        check(register >= digit.value) { // <2>
            "The current state of the register must make sense"
        }

        digitsCount += 1

        subject.onNext(register)

        assert(digitsCount > 0) { // <3>
            "At this stage, we should have more than one digit "
        }
    }
    // end::assert[]

    override fun enterOperation(op: Operation<*>) {
        tempRegister = register
        register = 0
        currentOperation = op as Operation<Long>?
        digitsCount = 0
        subject.onNext(register)
    }

    override fun factorial() {
        val input = register
        Observable.fromCallable {
            var fact: Long = 1
            if (input > 0) {
                for (i in 1..input) {
                    fact *= i
                }
            }
            fact
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { fact ->
                    register = fact!!
                    subject.onNext(register)
                }
    }

    override fun clear() {
        if (currentOperation == null) {
            register = 0
        }
        currentOperation = null
        digitsCount = 0
        tempRegister = 0
        subject.onNext(register)
    }

    override fun execute() {
        if (currentOperation == null) {
            register = 0
            clear()
            return
        }
        currentOperation?.let {
            val result = it.execute(tempRegister, register)
            if (storage != null) {
                val op = it.getString(tempRegister, register)
                storage?.addOperation(op)
            }
            register = result.toInt().toLong()
            clear()
        }
    }

    override fun backspace() {
        register /= 10
        subject.onNext(register)
    }

    override fun toString(): String = storage?.toString() ?: "Integer Calculator, no storage"
}
