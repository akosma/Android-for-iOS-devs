package training.akosma.rxcalculator

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fm = supportFragmentManager
        var fragment: Fragment? = fm.findFragmentById(R.id.activity_main)

        if (fragment == null) {
            fragment = CalculatorFragment()
            fm.beginTransaction()
                    .add(R.id.activity_main, fragment)
                    .commit()
        }

    }
}
