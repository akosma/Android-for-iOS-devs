package training.akosma.rxcalculator.engine.storage

import java.util.ArrayList

class MemoryStorage : Storage {
    private val mOperations = ArrayList<String>()

    override val operations: List<String>
        get() = mOperations

    override fun addOperation(operation: String) {
        mOperations.add(operation)
    }

    override fun toString(): String {
        return mOperations.toString()
    }
}
