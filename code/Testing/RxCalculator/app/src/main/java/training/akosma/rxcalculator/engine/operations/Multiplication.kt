package training.akosma.rxcalculator.engine.operations

class Multiplication<T : Number> : Operation<T> {
    override fun execute(op1: T, op2: T): Number {
        return op1.toDouble() * op2.toDouble()
    }

    override fun getString(op1: T, op2: T): String {
        val sb = StringBuilder()
        sb.append(op1.toDouble())
                .append(" * ")
                .append(op2.toDouble())
                .append(" = ")
                .append(op1.toDouble() * op2.toDouble())
        return sb.toString()
    }
}
