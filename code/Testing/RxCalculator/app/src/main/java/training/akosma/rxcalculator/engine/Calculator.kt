package training.akosma.rxcalculator.engine

import rx.Observable
import training.akosma.rxcalculator.engine.operations.Operation
import training.akosma.rxcalculator.engine.storage.Storage

interface Calculator {

    val observable: Observable<*>

    fun setStorage(storage: Storage)

    fun enterDigit(digit: Digit)

    fun enterOperation(op: Operation<*>)

    fun factorial()

    fun clear()

    fun execute()

    fun backspace()
}
