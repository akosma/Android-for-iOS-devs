package training.akosma.rxcalculator.engine.storage

interface Storage {
    val operations: List<String>
    fun addOperation(operation: String)
}
