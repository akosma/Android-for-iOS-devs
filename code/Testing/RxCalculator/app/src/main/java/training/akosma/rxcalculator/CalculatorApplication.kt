package training.akosma.rxcalculator

import android.app.Application
import android.os.Build
import android.os.StrictMode

import training.akosma.rxcalculator.engine.CalculatorComponent
import training.akosma.rxcalculator.engine.DaggerCalculatorComponent

class CalculatorApplication : Application() {

    lateinit var calculatorComponent: CalculatorComponent
        private set

    override fun onCreate() {
        super.onCreate()

        calculatorComponent = DaggerCalculatorComponent.builder().build()

        // tag::strict[]
        if (isDebug()) { // <1>
            StrictMode.enableDefaults();

            val threadPolicy = StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build()
            val vmPolicy = StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects() // <2>
                    .penaltyLog()
                    .penaltyDeath()
                    .build()

            StrictMode.setThreadPolicy(threadPolicy);
            StrictMode.setVmPolicy(vmPolicy);
        }
        // end::strict[]
    }

    private fun isDebug(): Boolean = "google_sdk" == Build.PRODUCT || "sdk" == Build.PRODUCT
}
