package training.akosma.rxcalculator

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.widget.FrameLayout
import android.widget.TextView

import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull

// tag::instrum[]
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    @Throws(Exception::class)
    fun onCreate() {
        val view = activityTestRule.activity.findViewById<FrameLayout>(R.id.activity_main)
        assertEquals("android.widget.FrameLayout", view.javaClass.name)

        val text = view.findViewById<TextView>(R.id.text_view_display)
        assertNotNull(text)
    }

}
// end::instrum[]
