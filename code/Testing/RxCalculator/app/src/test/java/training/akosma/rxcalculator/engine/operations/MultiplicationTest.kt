package training.akosma.rxcalculator.engine.operations

import org.junit.Test

import training.akosma.rxcalculator.engine.operations.Multiplication

import org.junit.Assert.*

class MultiplicationTest {
    @Test
    @Throws(Exception::class)
    fun execute() {
        val operation = Multiplication<Float>()
        val op1 = 2F
        val op2 = 3F
        val result = operation.execute(op1, op2)
        assertEquals(result.toFloat(), 6.0f, 0.001f)
    }

    @Test
    @Throws(Exception::class)
    fun getString() {
        val operation = Multiplication<Float>()
        val op1 = 2F
        val op2 = 3F
        val value = operation.getString(op1, op2)
        assertEquals(value, "2.0 * 3.0 = 6.0")
    }
}