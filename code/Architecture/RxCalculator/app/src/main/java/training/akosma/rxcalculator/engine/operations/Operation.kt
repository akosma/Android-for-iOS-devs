package training.akosma.rxcalculator.engine.operations

interface Operation<T : Number> {
    fun execute(op1: T, op2: T): Number
    fun getString(op1: T, op2: T): String
}
