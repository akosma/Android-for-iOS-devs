package training.akosma.rxcalculator.engine.operations

class Division<T : Number> : Operation<T> {
    override fun execute(op1: T, op2: T): Number {
        // We are cheating here…
        // Unfortunately there are no generic arithmetic operations on Generics in Java
        return op1.toDouble() / op2.toDouble()
    }

    override fun getString(op1: T, op2: T): String {
        val sb = StringBuilder()
        sb.append(op1.toDouble())
                .append(" / ")
                .append(op2.toDouble())
                .append(" = ")
                .append(op1.toDouble() / op2.toDouble())
        return sb.toString()
    }
}
