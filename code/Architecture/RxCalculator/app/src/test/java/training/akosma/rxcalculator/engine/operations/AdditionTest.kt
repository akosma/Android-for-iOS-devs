package training.akosma.rxcalculator.engine.operations

import org.junit.Test

import org.junit.Assert.assertEquals

// tag::local[]
class AdditionTest {

    @Test
    @Throws(Exception::class)
    fun execute() {
        val op = Addition<Float>()
        val op1 = 2F
        val op2 = 3F
        val result = op.execute(op1, op2)
        assertEquals(result.toFloat(), 5.0f, 0.001f)
    }

    @Test
    @Throws(Exception::class)
    fun getString() {
        val op = Addition<Float>()
        val op1 = 2F
        val op2 = 3F
        val value = op.getString(op1, op2)
        assertEquals(value, "2.0 + 3.0 = 5.0")
    }
}
// end::local[]
