package training.akosma.rxcalculator.engine.operations

import org.junit.Test

import training.akosma.rxcalculator.engine.operations.Division

import org.junit.Assert.assertEquals

class DivisionTest {
    @Test
    @Throws(Exception::class)
    fun execute() {
        val operation = Division<Float>()
        val op1 = 6F
        val op2 = 3F
        val result = operation.execute(op1, op2)
        assertEquals(result.toFloat(), 2.0f, 0.001f)
    }

    @Test
    @Throws(Exception::class)
    fun getString() {
        val operation = Division<Float>()
        val op1 = 6F
        val op2 = 3F
        val value = operation.getString(op1, op2)
        assertEquals(value, "6.0 / 3.0 = 2.0")
    }
}