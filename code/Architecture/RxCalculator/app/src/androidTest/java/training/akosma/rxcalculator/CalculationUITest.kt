package training.akosma.rxcalculator

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CalculationUITest {

    @Rule @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

    @Test
    fun calculationUITest() {
        val appCompatButton = onView(
                allOf(withId(R.id.button_1), withText("1"), isDisplayed()))
        appCompatButton.perform(click())

        val appCompatButton2 = onView(
                allOf(withId(R.id.button_2), withText("2"), isDisplayed()))
        appCompatButton2.perform(click())

        val appCompatButton3 = onView(
                allOf(withId(R.id.button_plus), withText("+"), isDisplayed()))
        appCompatButton3.perform(click())

        val appCompatButton4 = onView(
                allOf(withId(R.id.button_3), withText("3"), isDisplayed()))
        appCompatButton4.perform(click())

        val appCompatButton5 = onView(
                allOf(withId(R.id.button_6), withText("6"), isDisplayed()))
        appCompatButton5.perform(click())

        val appCompatButton6 = onView(
                allOf(withId(R.id.button_equal), withText("="), isDisplayed()))
        appCompatButton6.perform(click())

        val appCompatButton7 = onView(
                allOf(withId(R.id.button_ac), withText("AC"), isDisplayed()))
        appCompatButton7.perform(click())

        val appCompatButton8 = onView(
                allOf(withId(R.id.button_7), withText("7"), isDisplayed()))
        appCompatButton8.perform(click())

        // tag::espresso[]
        val appCompatButton9 = onView(
                allOf(withId(R.id.button_8), withText("8"), isDisplayed()))
        appCompatButton9.perform(click())

        val appCompatButton10 = onView(
                allOf(withId(R.id.button_delete), withText("DEL"), isDisplayed()))
        appCompatButton10.perform(click())

        val textView = onView(
                allOf(withId(R.id.text_view_display), withText("7"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.activity_main),
                                        0),
                                0),
                        isDisplayed()))
        textView.check(matches(withText("7")))
        // end::espresso[]

    }
}
