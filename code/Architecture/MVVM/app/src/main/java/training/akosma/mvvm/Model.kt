package training.akosma.mvvm

data class Model(var name: String, var age: Int, var active: Boolean)
