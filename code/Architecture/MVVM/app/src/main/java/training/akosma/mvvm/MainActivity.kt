package training.akosma.mvvm

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import training.akosma.mvvm.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // tag::binding[]
        viewModel = ViewModelImpl()
        val binding: ActivityMainBinding   // <1>
                = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()
        // end::binding[]

        viewModel.model = Model("Michelin", 7, false)
    }
}
