package training.akosma.mvvm

import android.databinding.ObservableField
import android.widget.SeekBar

// tag::viewmodelimpl[]
class ViewModelImpl : ViewModel {
    var backingModel: Model? = null

    override var model: Model?
        get() = backingModel
        set(value) {
            backingModel = value
            progress.set(backingModel?.age)
            text.set(backingModel?.toString())
        }

    override val text = ObservableField<String>()
    override val progress = ObservableField<Int>()

    override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
        model?.age = i
        text.set(model?.toString())
    }
}
// end::viewmodelimpl[]
