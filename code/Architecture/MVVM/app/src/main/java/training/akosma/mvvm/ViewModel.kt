package training.akosma.mvvm

import android.databinding.ObservableField
import android.widget.SeekBar

// tag::viewmodel[]
interface ViewModel {
    var model: Model?
    val text: ObservableField<String>
    val progress: ObservableField<Int>

    fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean)
}
// end::viewmodel[]
