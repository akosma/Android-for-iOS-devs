package training.akosma.notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import java.util.*

class MainActivity : AppCompatActivity() {

    // tag::receiver[]
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            receive(intent)
        }
    }

    private fun receive(intent: Intent) {
        val value = intent.getStringExtra(Constants.DATA_KEY)
        Toast.makeText(this, value, Toast.LENGTH_SHORT).show()

        val detailFragment = supportFragmentManager
                .findFragmentById(R.id.itemFragment) as DetailFragment?
        if (detailFragment == null || !detailFragment.isInLayout) {
            val showDetailIntent = Intent(this, DetailActivity::class.java)
            showDetailIntent.putExtra(DetailFragment.PARAMETER, value)
            startActivity(showDetailIntent)
        } else {
            detailFragment.update(value)
        }
    }
    // end::receiver[]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::register[]
        val filter = IntentFilter(Constants.NOTIFICATION_NAME)
        val manager = LocalBroadcastManager.getInstance(this)
        manager.registerReceiver(receiver, filter)
        // end::register[]

        val items = ArrayList<String>()
        for (i in 0..99) {
            items.add("Item " + i.toString())
        }

        val fragment = supportFragmentManager
                .findFragmentById(R.id.listFragment) as ListFragment
        fragment.setItems(items)
    }

    // tag::deregister[]
    override fun onDestroy() {
        val manager = LocalBroadcastManager.getInstance(this)
        manager.unregisterReceiver(receiver)
        super.onDestroy()
    }
    // end::deregister[]
}
