package training.akosma.notifications

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val fragment = supportFragmentManager.findFragmentById(R.id.itemFragment) as DetailFragment
        fragment.update(intent.getStringExtra(DetailFragment.PARAMETER))
    }
}
