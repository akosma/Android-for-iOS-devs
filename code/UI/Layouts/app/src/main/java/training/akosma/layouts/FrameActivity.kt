package training.akosma.layouts

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class FrameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame)
    }
}
