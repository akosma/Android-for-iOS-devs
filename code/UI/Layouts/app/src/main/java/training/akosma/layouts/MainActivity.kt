package training.akosma.layouts

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        frameButton.setOnClickListener    { showLayout(FrameActivity::class.java)    }
        gridButton.setOnClickListener     { showLayout(GridActivity::class.java)     }
        linearButton.setOnClickListener   { showLayout(LinearActivity::class.java)   }
        relativeButton.setOnClickListener { showLayout(RelativeActivity::class.java) }
    }

    private fun showLayout(klass: Class<*>) {
        val intent = Intent(this, klass)
        startActivity(intent)
    }
}
