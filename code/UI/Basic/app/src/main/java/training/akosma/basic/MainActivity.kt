package training.akosma.basic

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.text.Spanned
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

// tag::intro[]
class MainActivity : AppCompatActivity() {
// end::intro[]
    lateinit var name: String

    // tag::oncreate[]
    // <1>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // end::oncreate[]

        // tag::check[]
        // <2>
        if (savedInstanceState != null) {
            name = savedInstanceState.getString(KEY)
            greetWithHtml()
        }
        // end::check[]

        /*
        // tag::listener2[]
        greetButton.setOnClickListener {
            val name = nameEditText.text.toString()
            val greeting = "Hello, " + name
            greetingTextView.text = greeting
        }
        // end::listener2[]
        */
    }

    // tag::save[]
    // <3>
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString(KEY, name)
    }
    // end::save[]

    // tag::listener[]
    // <2>
    fun button_onClick(view: View) {
        name = nameEditText.text.toString()
        greetWithHtml()
    }

    private fun greetWithHtml() {
        val text = "Hello <span style='color: blue;'><b>$name</b></span>"
        var html: Spanned
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            html = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY) // <3>
        } else {
            @Suppress("DEPRECATION")
            html = Html.fromHtml(text)
        }
        greetingTextView.text = html
    }
    // end::listener[]


    // tag::key[]
    companion object {

        // <1>
        private val KEY = "name"
    }
    // end::key[]

// tag::last[]
}
// end::last[]
