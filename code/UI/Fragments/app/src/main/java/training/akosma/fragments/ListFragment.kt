package training.akosma.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_list.*

import java.util.ArrayList

class ListFragment : Fragment() {
    private var items: List<String> = ArrayList()
    private var listener: OnItemSelectionListener? = null

    fun setItems(items: List<String>) {
        this.items = items
        if (this.isAdded) {
            listView.adapter = Adapter(items)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            items = arguments?.getStringArrayList(ARG_LIST) ?: ArrayList()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listView.setHasFixedSize(true)
        listView.adapter = Adapter(items)
        listView.layoutManager = LinearLayoutManager(activity)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnItemSelectionListener) {
            listener = context
        } else {
            throw RuntimeException(context?.toString() + " must implement OnDetailFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnItemSelectionListener {
        fun onItemSelected(value: String?)
    }

    private inner class StringHolder internal constructor(v: View)
        : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var item: String? = null
        private val textView: TextView

        init {
            v.setOnClickListener(this)
            textView = v.findViewById(android.R.id.text1)
        }

        internal fun bind(value: String) {
            item = value
            textView.text = value
        }

        override fun onClick(view: View) {
            listener?.onItemSelected(item)
        }
    }

    private inner class Adapter internal constructor(private val list: List<String>)
        : RecyclerView.Adapter<StringHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringHolder {
            val inflater = LayoutInflater.from(activity)
            val view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
            return StringHolder(view)
        }

        override fun onBindViewHolder(holder: StringHolder, position: Int) {
            val item = list[position]
            holder.bind(item)
        }

        override fun getItemCount(): Int {
            return list.size
        }
    }

    companion object {
        val ARG_LIST = "list"
    }
}
