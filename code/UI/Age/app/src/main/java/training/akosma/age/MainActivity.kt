package training.akosma.age

import android.app.Activity
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            age = savedInstanceState.getInt("age")
        } else {
            age = DEFAULT_AGE
        }
        displayAge()

        // tag::explicit[]
        ageButton.setOnClickListener {
            val i = Intent(this, AgeActivity::class.java)
            i.putExtra("age", age)
            startActivityForResult(i, 0)
        }
        // end::explicit[]

        // tag::implicit[]
        webButton.setOnClickListener {
            val intent = Intent(ACTION_VIEW, Uri.parse("https://akosma.training"))
            startActivity(intent)
        }
        // end::implicit[]
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt("age", age)
    }

    // tag::reading[]
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        age = data.getIntExtra("age", DEFAULT_AGE)
        displayAge()
    }
    // end::reading[]

    private fun displayAge() {
        textView.text = "Your age is " + age
    }

    companion object {
        private val DEFAULT_AGE = 30
    }
}
