package training.akosma.age

import android.app.Activity
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_age.*

class AgeActivity : AppCompatActivity() {
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_age)

        if (savedInstanceState != null) {
            age = savedInstanceState.getInt("age")
        } else {
            age = intent.getIntExtra("age", 30)
        }

        displayAge()
        updateLabel()
        notifyAge()

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                age = seekBar.progress * 10
                updateLabel()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                notifyAge()
            }
        })

        doneButton.setOnClickListener { finish() }

        // tag::contact[]
        contactsButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
            startActivityForResult(intent, PICK_REQUEST)
        }
        // end::contact[]
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt("age", age)
    }

    private fun displayAge() {
        seekBar.progress = age / 10
    }

    private fun updateLabel() {
        textView.text = "Your age is " + age
    }

    // tag::returning[]
    private fun notifyAge() {
        val data = Intent()
        data.putExtra("age", age)
        setResult(Activity.RESULT_OK, data)
    }
    // end::returning[]

    // tag::result[]
    public override fun onActivityResult(request: Int, code: Int, data: Intent) {
        super.onActivityResult(request, code, data)

        if (request == PICK_REQUEST && code == Activity.RESULT_OK) {
            val contactData = data.data
            val resolver = contentResolver
            var c: Cursor? = null
            if (contactData != null) {
                c = resolver.query(contactData, null, null, null, null)
            }
            if (c != null && c.moveToFirst()) {
                val column = ContactsContract.Contacts.DISPLAY_NAME
                val index = c.getColumnIndex(column)
                val name = c.getString(index)
                textView.text = name
                c.close()
            }
        }
    }

    companion object {
        private val PICK_REQUEST = 0
    }
    // end::result[]
}
