package training.akosma.introduction

// tag::interface[]
interface Person {
    // <1>
    var name: String
        get set

    // <2>
    fun greet() = "Hello! I am $this"

    // <3>
    fun showMoreInformation()
}
// end::interface[]
