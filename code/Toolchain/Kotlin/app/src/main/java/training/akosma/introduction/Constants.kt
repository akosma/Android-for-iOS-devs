package training.akosma.introduction

// tag::object[]
object Constants {
    val PI = 3.14
    val ANSWER = 42

    fun name() = "Math contstants"
}
// end::object[]
