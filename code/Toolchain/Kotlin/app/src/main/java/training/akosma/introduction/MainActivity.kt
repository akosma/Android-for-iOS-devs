package training.akosma.introduction

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import java.net.URL
import java.util.*

class MainActivity : AppCompatActivity() {

    // tag::variables[]
    var variable = 43
    val constant = "Hello"

    var typedVariable: Float = 5.6F
    val typedConstant: Boolean = true

    var optionalVariable: Employee? = null
    val optionalConstant: URL? = URL("https://sbb.ch")

    var anyVariable: Any = "This can be anything"

    var manager = Manager.managerFactory()
    // end::variables[]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        anyVariable = 34
        anyVariable = false
        anyVariable = Employee("Rosie", 35)

        method3(typedVariable)
        method3(optionalConstant)
        method3(anyVariable)

        /*
        Equivalent Swift code:

            // tag::swift_statements[]
            let str = "Hello, Swift"

            func increase(_ value: Int) -> Int { return value + 1 }

            func statements() {
                var i = 0
                while i < 10 { i = increase(i) }
                if str == "something" { print(str) }
            }
            // end::swift_statements[]
        */

        // tag::statements[]
        val str = "Hello, Kotlin"

        fun increase(value: Int): Int = value + 1

        fun statements() {
            var i = 0
            while (i < 10) i = increase(i)
            if (str == "something") print(str)
        }
        // end::statements[]

        // tag::expr[]
        val test = if (true) "Test" else "False"
        
        val state = State.Off
        fun decide() = when(state) {
            State.Off -> "Off"
            State.On -> "On"
        }
        val decision = decide()
        // end::expr[]

        // tag::interpolation[]
        val date = Date().toString()
        val person = Employee("Johnny", 70)
        print("Today is $date and ${person.name}'s age is ${person.age}")
        // end::interpolation[]

        // tag::range[]
        for (index in 1..5) {
            println("$index times 5 is ${index * 5}")
        }
        // end::range[]

        // tag::collections[]
        val stringArray = arrayOf<String>()
        val stringList = listOf<String>()
        val stringFloatMap = mapOf<String, Float>()
        val stringSet = setOf<String>()

        // Iterating over the elements of arrays, lists, maps and sets
        for (str in stringArray) {
            println("A value in the array is '$str'")
        }

        for (str in stringList) {
            println("A value in the list is '$str'")
        }

        for ((str, num) in stringFloatMap) {
            println("Pair => $str: $num")
        }

        for (str in stringSet) {
            println("Set element: $str")
        }

        // Arrays vs Lists: which one to choose?
        // An array has a fixed size and is usually very fast
        val shoppingList = arrayOf("salt", "sugar", "milk", "cheese")
        // You cannot add or remove items! This won't work:
        // shoppingList.add("bread")
        // but you can modify an individual item if needed
        shoppingList[1] = "bottle of wine"

        // If you need to add or remove items at runtime,
        // consider using a mutable list instead:
        val countries = mutableListOf<String>("Switzerland", "France", "Germany")
        countries.add("Italy")
        countries.remove("France")

        val jobs = mutableMapOf(
                "Roger" to "CEO",
                "Martin" to "CTO"
        )
        jobs["Adrian"] = "Writer"
        // end::collections[]

        // tag::when[]
        val number = 42
        when (number) {
            in 0..7, 8, 9 -> println("1 digit")
            10 -> println("2 digits")
            in 11..99 -> println("2 digits")
            in 100..999 -> println("3 digits")
            else -> println("4 or more digits")
        }
        // end::when[]

        // tag::class[]
        val person1 = Employee("Olivia", 45) // <1>

        val person2 = Employee().apply { // <2>
            name = "Thomas"
            age = 56
        }
        println("People: ${person1.greet()} and ${person2.showMoreInformation()}")

        manager.addPerson(person1)
        manager.addPerson(person2)
        manager.state = State.On
        // end::class[]

        // tag::optionals[]
        val optionalEmployee: Employee? = Employee("Olivia", 45)

        val greeting = optionalEmployee?.greet()
        println("greeting: $greeting")

        val age = optionalEmployee?.age ?: 30 // <1>

        if (optionalEmployee != null) {
            val greetingAgain = optionalEmployee.greet()
            println("greeting: $greetingAgain")
            manager.addPerson(optionalEmployee)
        }
        // end::optionals[]

        // tag::optionallet[]
        optionalEmployee?.let { // <1>
            it.greet()
            println("Employee ${it.name} is ${it.age} years old")
        }

        optionalEmployee?.let { employee -> // <2>
            employee.greet()
            println("Employee ${employee.name} is ${employee.age} years old")
        }
        // end::optionallet[]

        // tag::extensions[]
        val temperature: Double = 32.0
        val fahrenheit = temperature.fahrenheit
        val celsius = fahrenheit.celsius
        println("$temperature degrees Celsius is $fahrenheit degrees Fahrenheit")
        // end::extensions[]

        // tag::infix[]
        manager addPerson person
        // end::infix[]

        // tag::customop[]
        val team = manager + person
        // end::customop[]
    }

    // tag::fun[]
    fun method1(input: String): Int {
        return input.length
    }

    fun method2(input: String) = input.length

    fun <T>method3(input: T) = input.toString()
    // end::fun[]
}
