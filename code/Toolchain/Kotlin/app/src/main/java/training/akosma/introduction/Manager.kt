package training.akosma.introduction

// tag::constructor[]
class Manager(private var backingName: String = "",
              private var staff: MutableList<Person> = mutableListOf<Person>(),
              var state: State = State.Off) : Person {
// end::constructor[]

    // tag::init[]
    private val isActive: Boolean

    init {
        isActive = true
    }
    // end::init[]

    override var name: String
        get() = backingName
        set(value) {
            backingName = value
        }

    override fun showMoreInformation() {
        println("This is a manager: state $state")
    }

    // tag::infix[]
    infix fun addPerson(person: Person) {
        if (staff.count() < MAXIMUM_EMPLOYEE_COUNT) {
            staff.add(person)
        }
        else {
            throw Exception("Cannot add more staff members")
        }
    }
    // end::infix[]

    // tag::customop[]
    operator fun plus(person: Person): Team {
        return Team(this, person)
    }
    // end::customop[]

    fun firePerson(person: Person) {
        if (staff.contains(person)) {
            staff.remove(person)
        }
    }

    // tag::companion[]
    companion object OptionalName {
        val MAXIMUM_EMPLOYEE_COUNT = 10

        fun managerFactory() = Manager("Maria Hill")
    }
    // end::companion[]
}
