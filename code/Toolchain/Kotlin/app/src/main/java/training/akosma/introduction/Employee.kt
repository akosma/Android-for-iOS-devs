package training.akosma.introduction

// tag::dataclass[]
data class Employee(private var backingName: String = "",
                    var age: Int = 30) : Person {
// end::dataclass[]

    override var name: String
        get() = backingName
        set(value) {
            backingName = value
        }

    override fun showMoreInformation() {
        println("This is a showMoreInformation for person: ${this.name}")
    }
}
