package training.akosma.introduction

// tag::extension[]
val Double.fahrenheit: Double get() = (this * 9 / 5) + 32
val Double.celsius: Double get() = (this - 32) * 5 / 9
// end::extension[]
