package training.akosma.stethosample

// tag::usage[]
import android.app.Application
import com.facebook.stetho.Stetho

class StethoSampleApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}
// end::usage[]
