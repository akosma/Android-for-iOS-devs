package training.akosma.nslogger

import android.content.Context

object Debug {
    val D = true        // set to false to disable debug
    var L: DroidLogger? = null

    fun assert_(condition: Boolean) {
        if (D && !condition)
            throw AssertionError("assertion failed")
    }

    fun enableDebug(ctx: Context, flushEachMessage: Boolean) {
        if (L == null) {
            L = DroidLogger(ctx)
            L!!.setMessageFlushing(flushEachMessage)
        }
    }
}
