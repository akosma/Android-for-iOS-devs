package training.akosma.nslogger

import android.content.Context
import com.NSLogger.NSLoggerClient

import java.util.Formatter

class DroidLogger(ctx: Context) : NSLoggerClient(ctx) {

    private fun taggedLog(level: Int, tag: String, message: String) {
        val st = Thread.currentThread().stackTrace
        if (st != null && st.size > 4) {
            // log with originating source code info
            val e = st[4]
            log(e.fileName, e.lineNumber, e.className + "." + e.methodName + "()", tag, level, message)
        } else {
            // couldn't obtain stack trace? log without source code info
            log(tag, level, message)
        }
    }

    fun LOG_MARK(mark: String) {
        logMark(mark)
    }

    fun LOG_EXCEPTION(exc: Exception) {
        val st = exc.stackTrace
        if (st != null && st.size != 0) {
            // a stack trace was attached to exception, report the most recent callsite in file/line/function
            // information, and append the full stack trace to the message
            val sb = StringBuilder(256)
            sb.append("Exception: ")
            sb.append(exc.toString())
            sb.append("\n\nStack trace:\n")
            for (e in st) {
                sb.append(e.fileName)
                if (e.lineNumber < 0)
                    sb.append(" (native)")
                else {
                    sb.append(":")
                    sb.append(Integer.toString(e.lineNumber))
                }
                sb.append(" ")
                sb.append(e.className)
                sb.append(".")
                sb.append(e.methodName)
                sb.append("\n")
            }
            val e = st[0]
            log(e.fileName, e.lineNumber, e.className + "." + e.methodName + "()", "exception", 0, sb.toString())
        } else {
            // no stack trace attached to exception (should not happen)
            taggedLog(0, "exception", exc.toString())
        }
    }

    fun LOG_APP(level: Int, format: String, vararg args: Any) {
        taggedLog(level, "app", Formatter().format(format, *args).toString())
    }

    fun LOG_DYNAMIC_IMAGES(level: Int, format: String, vararg args: Any) {
        taggedLog(level, "images", Formatter().format(format, *args).toString())
    }

    fun LOG_WEBVIEW(level: Int, format: String, vararg args: Any) {
        taggedLog(level, "webview", Formatter().format(format, *args).toString())
    }

    fun LOG_CACHE(level: Int, format: String, vararg args: Any) {
        taggedLog(level, "cache", Formatter().format(format, *args).toString())
    }

    fun LOG_NETWORK(level: Int, format: String, vararg args: Any) {
        taggedLog(level, "network", Formatter().format(format, *args).toString())
    }

    fun LOG_SERVICE(level: Int, format: String, vararg args: Any) {
        taggedLog(level, "service", Formatter().format(format, *args).toString())
    }

    fun LOG_UI(level: Int, format: String, vararg args: Any) {
        taggedLog(level, "ui", Formatter().format(format, *args).toString())
    }
}
