package training.akosma.nslogger

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // tag::nslogger[]
        if (Debug.D) {
            Debug.enableDebug(application, true)
            // change to your mac's IP address, set a fixed TCP port in the Prefs in desktop NSLogger
            Debug.L?.setRemoteHost("192.168.1.115", 50007, true)
            Debug.L?.LOG_MARK("MainActivity startup")
        }

        button.setOnClickListener { Debug.L?.LOG_UI(0, "Button clicked") }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                Debug.L?.LOG_APP(0, "SeekBar changed")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                Debug.L?.LOG_NETWORK(0, "SeekBar onStartTrackingTouch")
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                Debug.L?.LOG_SERVICE(0, "SeekBar onStopTrackingTouch")
            }
        })
        // end::nslogger[]
    }

}
